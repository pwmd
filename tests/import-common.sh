KEYID="0xE1E740F6"

do_import() {
    FILE="$1"

    #echo Importing with pinentry ...
    #$PWMD --homedir $WDIR --import datafile.xml -o data/datafile $@
    #echo Result is $?

    echo "# Importing ..."
    $PWMD --homedir $OUTDIR --import $WDIR/$FILE.xml --keyid $KEYID \
	-o $OUTDIR/data/$FILE --passphrase-file $WDIR/passphrase.key \
	2>/dev/null || bail_out "Could not import $FILE.xml using key $KEYID."
}
