Password Manager Daemon is a server that provides a way for applications to
securely store and retrieve data at a centralized location. The data is stored
in an XML file and clients connect and send commands to manipulate the data.
It mimics a filesystem in many ways with commands to store, copy, move and
delete elements.

There are quite a few password managers around and pwmd is similar to most.
The need for pwmd is that gnupg keys stored on a smartcard cannot be cached in
gpg-agent and therefore each access to a datafile would require the smartcard
present to decrypt it. Pwmd decrypts the datafile once and caches it in memory
and has it's own cache expiration settings rather than using the gpg-agent
cache.

You may also find pwmd useful if you have a few applications that require the
same credentials but hate having to update all those configuration files to
reflect any changes. The data file can be edited once with a pwmd client and
other clients can retrieve the updated shared info.  Similar to how a symbolic
link on a filesystem works. See the "Target Attribute" in the documentation
for details.

Some other features include:

    * Multi-threaded. More than one client may access the data at the same
      time
    * OpenPGP encrypted XML data files via GnuPG with smartcard support
    * Remote network connections via libpwmd over GnuTLS or SSH via a proxy
    * Configuration file which supports file specific settings including:
      cache expiration, passphrase file and more
    * Logging to file and/or syslog
    * Secure memory usage. Pwmd will zero out memory before freeing it and
      also has the option to lock the entire process in RAM to avoid swapping
      the data to virtual memory
    * Per file and element ACL's including connections over TLS


Requirements:
-------------
    C99 compiler - http://www.gnu.org/software/gcc
    		   Has been tested to work. Others should work fine, too.

    libassuan    - ftp://ftp.gnupg.org/gcrypt/libassuan
		   Protocol. Version 2.5.0 or later is required.

    libgpg-error - http://www.gnupg.org
	           Error handling. Version 1.29 or later is required.

    gpg-agent    - http://www.gnupg.org
		   Keypair generation, decryption and caching. Version 2.2.7
		   or later is required.

    pinentry     - http://www.gnupg.org
		   Normally required for passphrase retrieval from gpg-agent.

    libxml2      - http://xmlsoft.org
                   Required for XML parsing and data manipulation.

    libgpgme     - http://www.gnupg.org
		   For data file crypto operations. Version 1.11.0 or later is
		   required.

    libgcrypt    - http://www.gnupg.org
		   Cache encryption and hashing. Version 1.7.0 or later is
		   required.

    libgnutls    - http://www.gnu.org/software/gnutls/
    		   For encrypted connections over a TCP network. Version
		   3.3.0 or later. Optional.


Installation:
-------------
./configure && make install

For TLS support, pass --enable-gnutls to ./configure.

Typing 'pwmd' will start the server and wait for connections to
~/.pwmd/socket.  Passing --allow-preset-passphrase to gpg-agent is recommended
and gpg-agent option --allow-loopback-pinentry is required if using a keyfile
or connecting over TLS. These options can be added to the gpg-agent.conf
configuration file in the gnupg home directory (~/.pwmd/.gnupg). See the
gpg-agent(1) manual page for details about these options and the pwmd
documentation for details about the location of the GnuPG home directory.


Connecting:
-----------
Any program that can connect to a UNIX domain socket will work although using
pwmc, a client included with libpwmd, is recommended:

    echo command | pwmc filename
or interactively
    pwmc filename
or
    socat UNIX-CONNECT:$HOME/.pwmd/socket -

A much more user-friendly full featured client QPwmc is also available. It has
a nice Qt GUI and is much easier to use. See
https://gitlab.com/bjk/qpwmc/wikis for details.

Please read pwmd.info manual for protocol commands, syntax and
configuration details. A HTML version is also available in doc/pwmd.html.


GIT Repository
--------------
There is a public GIT repository available at Gitlab. Anonymous checkouts can
be done by doing:

    git clone https://gitlab.com/bjk/pwmd
    or from the mirror:
    git clone git://repo.or.cz/pwmd.git

The gitweb interface can be viewed at:

    https://gitlab.com/bjk/pwmd
    or
    http://repo.or.cz/w/pwmd.git


Mailing lists
-------------
For pwmd:
https://lists.sourceforge.net/lists/listinfo/pwmd-devel

And for libpwmd:
https://lists.sourceforge.net/lists/listinfo/libpwmd-devel


Issue tracker:
--------------
Please send any bug reports, feature requests or patches to the issue tracker
at Gitlab: https://gitlab.com/bjk/pwmd/issues.


Ben Kibbey <bjk@luxsci.net>
https://gitlab.com/bjk/pwmd/wikis
