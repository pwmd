#!/bin/sh
#
# Tests for the IMPORT protocol command.
#
MAX_TESTS=8

. $AM_SRCDIR/common.sh
. $AM_SRCDIR/import-common.sh

init_tests() {
    do_import list
    cat >$OUTDIR/config << EOF
[global]
invoking_user=$USER
log_level=9
#enable_logging=true
[import]
passphrase_file=$WDIR/passphrase.key
EOF
    mv -f data/list data/import || bail_out "Could not move data/list."
    launch_pwmd import
}

test_1() {
    run_pwmc "--inquire IMPORT import" $DEVNULL <<EOF
<element _name="import">import value</invalid>
EOF
    test_failure $test_n $? 536871001 "Invalid syntax."
}

test_2() {
    run_pwmc "--inquire IMPORT import -S" $DEVNULL <<EOF
<element _name="import">import value</element>
EOF
    e=$?
    if [ $e -eq 0 ]; then
	run_pwmc "import" >result $DEVNULL <<EOF
GET import
EOF
        e=$?
    fi
    test_result $test_n $e import "Import to root."
}

test_3() {
    run_pwmc "--inquire IMPORT import -S" $DEVNULL <<EOF
<element _name="import">new import value</element>
EOF
    e=$?
    if [ $e -eq 0 ]; then
	run_pwmc "import" >result $DEVNULL <<EOF
GET import
EOF
        e=$?
    fi
    test_result $test_n $e import "Import to root with overwrite."
}

test_4() {
    run_pwmc "--inquire 'IMPORT --root=import' import -S" $DEVNULL <<EOF
<element _name="child">child import value</element>
EOF
    e=$?
    if [ $e -eq 0 ]; then
	run_pwmc "import" >result $DEVNULL <<EOF
GET import	child
EOF
        e=$?
    fi
    test_result $test_n $e import "Import to child."
}

test_5() {
    run_pwmc "--inquire 'IMPORT --root=import' import -S" $DEVNULL <<EOF
<element _name="child">new child import value</element>
EOF
    e=$?
    if [ $e -eq 0 ]; then
	run_pwmc "import" >result $DEVNULL <<EOF
GET import	child
EOF
        e=$?
    fi
    test_result $test_n $e import "Import to child with overwrite."
}

test_6() {
    run_pwmc "--inquire 'IMPORT --root=import\troot' import -S" $DEVNULL <<EOF
<element _name="a">A value<element _name="b">A B value</element></element>
<element _name="b">B value</element>
EOF
    e=$?
    if [ $e -eq 0 ]; then
	run_pwmc "import" >result $DEVNULL <<EOF
GET import	root	a	b
EOF
        e=$?
    fi
    test_result $test_n $e import "Import siblings."
}

test_7() {
    run_pwmc "--inquire 'IMPORT --root=import\troot' import -S" $DEVNULL <<EOF
<element _name="a">A value<element _name="b"/></element>
<element _name="b">B value</element>
EOF
    e=$?
    if [ $e -eq 0 ]; then
	run_pwmc "import" >result $DEVNULL <<EOF
GET import	root	b
EOF
        e=$?
    fi
    test_result $test_n $e import "Import siblings with overwrite."
}

# FIXME what is this supposed to do?
test_8() {
    run_pwmc "import" >result $DEVNULL <<EOF
GET import	root	a	b
EOF
    test_failure $test_n $? 536870970 "Fail to import siblings."
}

run_tests $@
