# Read in commands.c and add texinfo commands. See Makefile.am.

BEGIN {
}
{
while (getline > 0)
    if ($0 ~ /!BEGIN-HELP-TEXT!/) {
	list_total = 1
	name_length = 0

	while (getline > 0)
	    if ($0 ~ /!END-HELP-TEXT!/) {
		exit
	    }
	    else if ($0 ~ /new_command\(/) {
		    desc = ""
		getline
		sub(/^"/,"")
		idx = index($0,"\\n")+7;
		chapter = substr($0,idx,length())
		sub(/ \*\//,"",chapter)
		sub(/\\n".*$/,"")
		gsub(/\\"/,"\"")
		name = sub(/^[^ ]*/,"&")

		if (length($name) > name_length)
		    name_length = length($name)

		info = "@node " $name ", !NEXT!, !PREV!, Commands\n"
		info = info "@section " chapter "\n"
		info = info "@cindex " $name " command\n"
		info = info "Syntax:\n"
		info = info "@example\n"
		info = info $0 "\n"
		info = info "@end example\n"
		list[list_total "," "name"] = $name

		while (getline > 0)
		    if ($0 ~ /^\)\);/) {
			break
		    }
		    else {
			gsub(/\\n"$/,"")
			gsub(/"/,"")
			gsub(/\n/,"")
			desc = desc $0 "\n"
		    }

		desc = desc "\n"
		list[list_total "," "chapter"] = chapter
		list[list_total "," "info"] = info
		list[list_total "," "desc"] = desc
		list_total++
	    }
    }

}

#
# Borrowed from http://awk.info/?TenLiners
#
function qsort(A, left, right,   i, last) {
        if (left >= right)
                return
        swap(A, left, left+int((right-left+1)*rand()))
        last = left
        for (i = left+1; i <= right; i++)
                if (A[i "," "name"] < A[left "," "name"])
                        swap(A, ++last, i)
        swap(A, left, last)
        qsort(A, left, last-1)
        qsort(A, last+1, right)
}

function swap(A, i, j,   hrm) {
	name = A[i "," "name"];
	chapter = A[i "," "chapter"];
	info = A[i "," "info"];
	desc = A[i "," "desc"];
	A[i "," "name"] = A[j "," "name"]
	A[i "," "chapter"] = A[j "," "chapter"]
	A[i "," "info"] = A[j "," "info"]
	A[i "," "desc"] = A[j "," "desc"]
	A[j "," "name"] = name;
	A[j "," "chapter"] = chapter;
	A[j "," "info"] = info;
	A[j "," "desc"] = desc;
}

END {
	qsort(list, 1, list_total-1)

	print "@menu" > "menu.texi"
	for (t = 1; t < list_total; t++) {
		if (t == 1) {
			sub(/!PREV!/, "", list[t "," "info"])
		}
		else {
			sub(/!PREV!/, list[t-1 "," "name"], list[t "," "info"])
		}

		if (t == list_total-1) {
			sub(/!NEXT!/, "", list[t "," "info"])
		}
		else {
			sub(/!NEXT!/, list[t+1 "," "name"], list[t "," "info"])
		}

		print list[t "," "info"] "\n" list[t "," "desc"]

		n = name_length - length(list[t "," "name"])
		sp = " "
		while (n--)
		    sp = sp " "

		print "* " list[t "," "name"] "::" sp list[t "," "chapter"] >> "menu.texi"
	}

	print "@end menu" > "menu.texi"
	close("menu.texi")
}
