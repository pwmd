/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pwmd-error.h"
#include <assuan.h>
#include "util-misc.h"
#include "util-string.h"

pthread_key_t last_error_key;

static char *
_gpg_strerror (gpg_error_t rc)
{
  char ebuf[ASSUAN_LINELENGTH], *ep;

  gpg_strerror_r (rc, ebuf, sizeof (ebuf));
  ep = pthread_getspecific (last_error_key);
  free_key (ep);
  ep = str_dup (ebuf);
  pthread_setspecific (last_error_key, ep);
  return ep;
}

const char *
pwmd_strerror (gpg_error_t e)
{
  return _gpg_strerror (e);
}
