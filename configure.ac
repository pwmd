dnl Process this file with autoconf to produce a configure script.
AC_PREREQ([2.69])
AC_INIT([pwmd],[3.3.9],[Ben Kibbey <bjk@luxsci.net>])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR(build)
AC_CANONICAL_TARGET
AM_INIT_AUTOMAKE([foreign dist-bzip2 no-dist-gzip])
AC_PROG_MAKE_SET
AC_USE_SYSTEM_EXTENSIONS()
AC_CONFIG_SRCDIR([src/pwmd.c])
AC_CONFIG_HEADERS([config.h])
AM_GNU_GETTEXT_VERSION([0.21])

AM_CONDITIONAL([IS_GIT_REPO], [test -f ".git/HEAD"])
if test -f ".git/HEAD"; then
    GIT_BRANCH="`cut -d / -f 3 .git/HEAD`"
fi
AC_SUBST([GIT_BRANCH])

REQUIRE_LIBGCRYPT_VERSION="1.7.0"
REQUIRE_LIBASSUAN_VERSION="2.5.0"
REQUIRE_LIBGPGERROR_VERSION="1.29"
REQUIRE_LIBGPGME_VERSION="1.11.0"
REQUIRE_GNUPG_VERSION="2.2.7"
REQUIRE_LIBGNUTLS_VERSION="3.3.0"

AC_DEFINE_UNQUOTED(REQUIRE_LIBASSUAN_VERSION, "$REQUIRE_LIBASSUAN_VERSION",
	  [Minimum libassuan version.])
AC_DEFINE_UNQUOTED(REQUIRE_LIBGCRYPT_VERSION, "$REQUIRE_LIBGCRYPT_VERSION",
	  [Minimum libgcrypt version.])
AC_DEFINE_UNQUOTED(REQUIRE_LIBGPGERROR_VERSION, "$REQUIRE_LIBGPGERROR_VERSION",
	  [Minimum libgpg-error version.])
AC_DEFINE_UNQUOTED(REQUIRE_LIBGPGME_VERSION, "$REQUIRE_LIBGPGME_VERSION",
	  [Minimum libgpgme version.])
AC_DEFINE_UNQUOTED(REQUIRE_GNUPG_VERSION, "$REQUIRE_GNUPG_VERSION",
	  [Minimum gnupg version.])
AC_DEFINE_UNQUOTED(REQUIRE_LIBGNUTLS_VERSION, "$REQUIRE_LIBGNUTLS_VERSION",
	  [Minimum libgnutls version.])

dnl Checks for programs.
AX_PROG_CC_FOR_BUILD
AC_PROG_CXX
AC_PROG_AWK
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_PROG_MKDIR_P
AC_PROG_RANLIB
AC_PROG_YACC

dnl Checks for library functions.
AX_PTHREAD(, AC_MSG_ERROR([POSIX threads are required.]))
CC="$PTHREAD_CC"
CFLAGS="$CFLAGS $PTHREAD_CFLAGS"
LIBS="$PTHREAD_LIBS $LIBS"

acx_pthread_testcancel_ok=no
AC_MSG_CHECKING([for pthread_testcancel()])
AC_TRY_LINK_FUNC(pthread_testcancel, acx_pthread_testcancel_ok=yes)
AC_MSG_RESULT($acx_pthread_testcancel_ok)
if test x"$acx_pthread_testcancel_ok" = "xyes"; then
    AC_DEFINE(HAVE_PTHREAD_TESTCANCEL, 1, [Define if your OS has pthread_testcancel().])
fi

acx_pthread_cancel_ok=no
AC_MSG_CHECKING([for pthread_cancel()])
AC_TRY_LINK_FUNC(pthread_cancel, acx_pthread_cancel_ok=yes)
AC_MSG_RESULT($acx_pthread_cancel_ok)
if test x"$acx_pthread_cancel_ok" = "xyes"; then
    AC_DEFINE(HAVE_PTHREAD_CANCEL, 1, [Define if your OS has pthread_cancel().])
fi

acx_pthread_atfork_ok=no
AC_MSG_CHECKING([for pthread_atfork()])
AC_TRY_LINK_FUNC(pthread_atfork, acx_pthread_atfork_ok=yes)
AC_MSG_RESULT($acx_pthread_atfork_ok)
if test x"$acx_pthread_atfork_ok" = "xyes"; then
    AC_DEFINE(HAVE_PTHREAD_ATFORK, 1, [Define if your OS has pthread_atfork().])
fi

dnl Checks for header files.
AC_FUNC_ALLOCA
AC_CHECK_HEADERS([argz.h arpa/inet.h fcntl.h getopt.h inttypes.h langinfo.h \
		  libintl.h locale.h malloc.h netdb.h netinet/in.h stdint.h \
		  stdio_ext.h strings.h sys/acl.h sys/file.h sys/ioctl.h \
		  sys/param.h sys/socket.h sys/time.h syslog.h termios.h \
		  unistd.h limits.h])

AC_CHECK_DECLS([SO_BINDTODEVICE],,, [[#include <sys/types.h>
    				      #include <sys/socket.h>]])

case "${host}" in
    *-linux-androideabi)
	PTHREAD_CFLAGS="-pthread"
	PTHREAD_LIBS="-pthread"
	;;
    *)
	;;
esac

case "$target_os" in
     linux*)
	AC_CHECK_HEADERS([linux/sockios.h sys/prctl.h])
	if test "x$ac_cv_header_linux_sockios_h" = xyes; then
		AC_CHECK_DECL([SIOCOUTQ],,, [[#include <linux/sockios.h>]])
		if test "x$ac_cv_have_decl_SIOCOUTQ" = xyes; then
			AC_DEFINE(HAVE_DECL_SIOCOUTQ, 1,
			      	[Defined if SIOCOUTQ is available (Linux specific)])
		fi
	fi

	if test "x$ac_cv_header_sys_prctl_h" = xyes; then
	   AC_CHECK_DECL([PR_SET_NAME],,, [[#include <sys/prctl.h>]])
	      if test "x$ac_cv_have_decl_PR_SET_NAME" = xyes; then
	            AC_DEFINE(HAVE_PR_SET_NAME, 1,
		       	     [Defined if PR_SET_NAME is available (Linux specific)])
	      fi

	   AC_CHECK_DECL([PR_SET_DUMPABLE],,, [[#include <sys/prctl.h>]])
	      if test "x$ac_cv_have_decl_PR_SET_DUMPABLE" = xyes; then
	            AC_DEFINE(HAVE_PR_SET_DUMPABLE, 1,
		      	     [Defined if PR_SET_DUMPABLE is available (Linux specific)])
	      fi
	fi
	;;
     openbsd*)
	;;
     *bsd*)
	AC_CHECK_HEADERS([sys/filio.h])
	if test "x$ac_cv_header_sys_filio_h" = xyes; then
		AC_CHECK_DECL([FIONWRITE],,, [[#include <sys/filio.h>]])
		if test "x$ac_cv_have_decl_FIONWRITE" = xyes; then
			AC_DEFINE(HAVE_DECL_FIONWRITE, 1,
			      	[Defined if FIONWRITE is available (*BSD specific, not OpenBSD)])
		fi
	fi
	;;
      *cygwin*)
        AC_DEFINE(HAVE_WINLIKE_SYSTEM, 1, [Define if this is a Windows-like system.])
        ;;
     *)
dnl     All others use SO_SNDLOWAT.
	;;
esac

with_proc_symlink=
AC_MSG_CHECKING([for /proc/self/ executable path symlink])
for f in exe file; do
    if test -h "/proc/$$/$f"; then
	with_proc_symlink="$f"
	break
    fi
done
if test x"$with_proc_symlink" != "x"; then
   AC_DEFINE_UNQUOTED(WITH_PROC_SYMLINK, ["$with_proc_symlink"],
		      [Define to your systems idea of an exe path in /proc/self/.])
else
    with_proc_symlink="none"
fi
AC_MSG_RESULT([$with_proc_symlink])

dnl Checks for typedefs, structures, and compiler characteristics.
AC_CHECK_HEADER_STDBOOL
AC_TYPE_UID_T
AC_C_INLINE
AC_TYPE_MODE_T
AC_TYPE_OFF_T
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_TYPE_SSIZE_T
AC_TYPE_UINT16_T
AC_TYPE_UINT32_T
AC_TYPE_UINT64_T
AC_TYPE_UINT8_T
AC_CHECK_TYPES([ptrdiff_t])
AC_SYS_LARGEFILE

dnl Checks for library functions.
AM_GNU_GETTEXT([external])
AC_FUNC_FORK
AC_FUNC_LSTAT_FOLLOWS_SLASHED_SYMLINK
AC_FUNC_MALLOC
AC_FUNC_MBRTOWC
AC_FUNC_MMAP
AC_FUNC_REALLOC
AC_CHECK_FUNCS([__argz_count __argz_next __argz_stringify backtrace \
		clock_gettime explicit_bzero flock getcwd getgrnam_r \
		getpwnam_r getopt_long isascii localeconv mblen mbrlen \
		memchr memmove mempcpy memset memset_s mkdir mlockall \
		munmap nl_langinfo putenv select setlocale setrlimit socket \
		stpcpy strcasecmp strchr strcspn strdup strerror strncasecmp \
		strrchr strstr strtol strtoul sigwaitinfo])

AM_CONDITIONAL([NEED_GETOPT_LONG], [test "$ac_cv_func_getopt_long" != "yes"])

AM_PATH_LIBASSUAN($REQUIRE_LIBASSUAN_VERSION,, AC_MSG_ERROR([libassuan >= $REQUIRE_LIBASSUAN_VERSION not found]))
AM_PATH_LIBGCRYPT([1:$REQUIRE_LIBGCRYPT_VERSION],, AC_MSG_ERROR([libgcrypt >= $REQUIRE_LIBGCRYPT_VERSION not found]))
AM_PATH_GPGME($REQUIRE_LIBGPGME_VERSION,, AC_MSG_ERROR([libgpgme >= $REQUIRE_LIBGPGME_VERSION not found]))
AM_PATH_GPG_ERROR($REQUIRE_LIBGPGERROR_VERSION,, AC_MSG_ERROR([libgpg-error >= $REQUIRE_LIBGPGERROR_VERSION not found]))
AM_PATH_XML2(,, AC_MSG_ERROR([libxml2 not found]))
AC_SEARCH_LIBS([clock_gettime], [rt])

dnl Remote TCP/TLS connections.
AC_ARG_ENABLE(gnutls, AS_HELP_STRING([--enable-gnutls],
              [Enable TCP (GnuTLS) server support.]), USE_GNUTLS=$enableval,
              USE_GNUTLS=no)
if test "x$USE_GNUTLS" = "xyes"; then
    PKG_CHECK_MODULES([GNUTLS], [gnutls >= $REQUIRE_LIBGNUTLS_VERSION])
    AC_DEFINE(WITH_GNUTLS, 1, [Define if you want TCP (GnuTLS) support.])
fi
AM_CONDITIONAL(WITH_GNUTLS, [test "x$USE_GNUTLS" = "xyes"])

dnl Libacl support (for data files).
AC_ARG_ENABLE(acl, AS_HELP_STRING([--enable-acl],
	      [Enable Access Control List support for data files]),
	      [USE_LIBACL=yes], [USE_LIBACL=no])
if test "x$USE_LIBACL" = "xyes"; then
    AC_SEARCH_LIBS([acl_get_file], [acl],,
                   AC_MSG_ERROR([Missing or incomplete libacl installation.]))
    AC_CHECK_HEADER([sys/acl.h],,
                   AC_MSG_ERROR([Missing or incomplete libacl installation.]))
    AC_DEFINE(WITH_LIBACL, 1, [Define if you want libacl support.])
fi
AM_CONDITIONAL(WITH_LIBACL, [test "x$USE_LIBACL" = "xyes"])

AC_DEFUN([AC_DEBUG],
[
    if test "$1"; then
	ac_cv_sys_debug=$1
    fi

    AC_CACHE_CHECK([if debugging is wanted], [ac_cv_sys_debug],
	[ac_cv_sys_debug=no])
    AM_CONDITIONAL([WITH_DEBUG], [test "$ac_cv_sys_debug" = "yes"])
])

AC_ARG_ENABLE(debug, AS_HELP_STRING([--enable-debug], [Enable debugging (memory de/allocations output).]),
    AC_DEBUG([$enableval]), AC_DEBUG)

AC_DEFUN([AC_MEM_DEBUG],
[
    if test "$1"; then
	ac_cv_sys_mem_debug=$1
    fi

    AC_CACHE_CHECK([if memory debugging is wanted], [ac_cv_sys_mem_debug],
	[ac_cv_sys_mem_debug=no])
    AM_CONDITIONAL([WITH_MEM_DEBUG], [test "$ac_cv_sys_mem_debug" = "yes"])
])

AC_DEFUN([AC_MUTEX_DEBUG],
[
    if test "$1"; then
	ac_cv_sys_mutex_debug=$1
    fi

    AC_CACHE_CHECK([if mutex debugging is wanted], [ac_cv_sys_mutex_debug],
	[ac_cv_sys_mutex_debug=no])
    AM_CONDITIONAL([WITH_MUTEX_DEBUG], [test "$ac_cv_sys_mutex_debug" = "yes"])
])

AC_ARG_ENABLE(mutex_debug, AS_HELP_STRING([--enable-mutex-debug], [Enable mutex debugging output.]),
    AC_MUTEX_DEBUG([$enableval]), AC_MUTEX_DEBUG)

dnl Memory debugging.
AC_ARG_ENABLE(mem_debug, AS_HELP_STRING([--enable-mem-debug], [Disable internal memory manager.]),
    AC_MEM_DEBUG([$enableval]), AC_MEM_DEBUG)
AM_CONDITIONAL(MEM_DEBUG, test x"${ac_cv_sys_mem_debug}" != xno)

# Default pwmd home directory.
AC_ARG_WITH(pwmd_homedir, AS_HELP_STRING([--with-pwmd-homedir=PATH],
	      [Default pwmd home directory (~/.pwmd)]),
	      pwmd_homedir="$withval", pwmd_homedir=)
if test x"$pwmd_homedir" != "x"; then
   AC_DEFINE_UNQUOTED(PWMD_HOMEDIR, ["$pwmd_homedir"],
   				    [Default pwmd home directory (~/.pwmd).])
fi

AC_ARG_WITH(invoking_user, AS_HELP_STRING([--with-invoking-user=nobody],
	    [Default invoking_user when not specified in the configuration.
	     Setting to blank will have the user that started pwmd be the
	     invoking_user.]), invoking_user="$withval",
	     invoking_user="nobody")
if test x"$invoking_user" != "x"; then
   AC_DEFINE_UNQUOTED(DEFAULT_INVOKER, ["$invoking_user"],
		      [Default invoking_user when not specified in the
		       configuration. Set to NULL to have the user that
		       started pwmd be the invoking_user.])
else
   AC_DEFINE(DEFAULT_INVOKER, [NULL],
		      [Default invoking_user when not specified in the
		       configuration. Set to NULL to have the user that
		       started pwmd be the invoking_user.])
fi

VERSION_MAJOR="`echo "$VERSION" | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\).*/\1/'`"
VERSION_MINOR="`echo "$VERSION" | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\).*/\2/'`"
VERSION_PATCH="`echo "$VERSION" | sed 's/\([[0-9]]*\)\.\([[0-9]]*\)\.\([[0-9]]*\).*/\3/'`"
VERSION_NUM="`printf 0x%02x%02x%02X ${VERSION_MAJOR} ${VERSION_MINOR} ${VERSION_PATCH}`"
AC_DEFINE_UNQUOTED(VERSION_HEX, ${VERSION_NUM}, [For the file header.])

AC_CONFIG_FILES([Makefile src/Makefile doc/Makefile doc/pwmd-dump.1 \
		 po/Makefile.in tests/Makefile])
AC_OUTPUT
echo
echo "Features:"
echo "    TCP (GnuTLS)....: $USE_GNUTLS"
echo "    Data file ACL's.: $USE_LIBACL"

if test x"$pwmd_homedir" != "x"; then
echo "    Home directory..: $pwmd_homedir"
else
echo "    Home directory..: ~/.pwmd"
fi

if test "x$invoking_user" != "x"; then
echo "    Invoking user...: $invoking_user"
else
echo "    Invoking user...: determined on start"
fi

echo "    Exec symlink....: $with_proc_symlink"
echo "    Debug...........: ${ac_cv_sys_debug}"
echo "    Memory debug....: ${ac_cv_sys_mem_debug}"
echo "    Mutex debug.....: ${ac_cv_sys_mutex_debug}"
