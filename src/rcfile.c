/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <pwd.h>
#include <grp.h>

#include "pwmd-error.h"
#include "mutex.h"
#include "rcfile.h"
#include "util-misc.h"
#include "common.h"
#include "util-slist.h"
#include "util-string.h"
#include "mem.h"

#ifdef HAVE_MLOCKALL
int disable_mlock;
#endif
char *logfile;
char *rcfile;
char *homedir;
int max_recursion_depth;
int disable_list_and_dump;
struct slist_s *global_config;
int log_syslog;
pthread_mutex_t rcfile_mutex;
pthread_cond_t rcfile_cond;
pthread_t rcfile_tid;
struct invoking_user_s *invoking_users;
int log_keepopen;
int log_level;

#define DEFAULT_CACHE_TIMEOUT "600"
#define DEFAULT_KEEPALIVE_INTERVAL "60"
#define DEFAULT_LOCK_TIMEOUT "50" // MUTEX_TRYLOCK in tenths of a second
#define DEFAULT_BACKLOG "128"
#define DEFAULT_CIPHER_PRIORITY "SECURE256:SECURE192:SECURE128:-VERS-SSL3.0:-VERS-TLS1.0:-VERS-TLS1.1:-AES-128-CBC:-AES-256-CBC"

#define INVALID_VALUE(file, line) do {					\
    if (file)								\
      log_write(_("%s(%i): invalid value for parameter."), file, line); \
  } while (0);

enum
{
  PARAM_INT, PARAM_CHARP, PARAM_LONG, PARAM_LONGLONG, PARAM_CHARPP,
  PARAM_BOOL, PARAM_INVALID
};

static struct config_params_s
{
  const char *name;
  int type;
  const char *value;
} config_params[] = {
  {  "backup",			PARAM_BOOL,	"true"},
  {  "enable_socket",		PARAM_BOOL,	"true"},
  {  "socket_path",		PARAM_CHARP,	NULL},
  {  "socket_perms",		PARAM_CHARP,	NULL},
  {  "backlog",			PARAM_INT,	DEFAULT_BACKLOG},
  {  "passphrase_file",		PARAM_CHARP,	NULL},
  {  "log_path",		PARAM_CHARP,	"~/.pwmd/log"},
  {  "enable_logging",		PARAM_BOOL,	"0"},
  {  "log_keepopen",		PARAM_BOOL,	"true"},
  {  "log_level",		PARAM_INT,	"0"},
  {  "disable_mlockall",	PARAM_BOOL,	"true"},
  {  "cache_timeout",		PARAM_LONG,	DEFAULT_CACHE_TIMEOUT},
  {  "cache_push",		PARAM_CHARPP,	NULL},
  {  "disable_list_and_dump",	PARAM_BOOL,	"false"},
  {  "recursion_depth",		PARAM_INT,	"100"},
  {  "syslog",			PARAM_BOOL,	"false"},
  {  "allowed",			PARAM_CHARPP,	NULL},
  {  "allowed_file",		PARAM_CHARP,	NULL},
  {  "priority",		PARAM_INT,	INVALID_PRIORITY},
  {  "keepalive_interval",	PARAM_INT,	DEFAULT_KEEPALIVE_INTERVAL},
  {  "tcp_port",		PARAM_INT,	"6466"},
  {  "enable_tcp",		PARAM_BOOL,	"false"},
  {  "tcp_require_key",		PARAM_BOOL,	"false"},
  {  "tcp_bind",		PARAM_CHARP,	"any"},
  {  "tcp_interface",		PARAM_CHARP,	NULL},
  {  "tls_timeout",		PARAM_INT,	"300"},
  {  "tls_cipher_suite",	PARAM_CHARP,	DEFAULT_CIPHER_PRIORITY},
  {  "tls_dh_params_file",	PARAM_CHARP,	NULL},
  {  "tls_use_crl",		PARAM_BOOL,	"false"},
  {  "tls_crl_file",		PARAM_CHARP,	NULL},
  {  "tls_ca_file",		PARAM_CHARP,	NULL},
  {  "tls_server_cert_file",	PARAM_CHARP,	NULL},
  {  "tls_server_key_file",	PARAM_CHARP,	NULL},
  {  "require_save_key",	PARAM_BOOL,	"true"},
  {  "invoking_user",		PARAM_CHARPP,	NULL},
  {  "invoking_file",		PARAM_CHARP,	NULL},
  {  "encrypt_to",		PARAM_BOOL,	"false"},
  {  "always_trust",		PARAM_BOOL,	"false"},
  {  "gpg_homedir",		PARAM_CHARP,	NULL},
  {  "strict_kill",		PARAM_BOOL,	"false"},
  {  "lock_timeout",		PARAM_LONG,	DEFAULT_LOCK_TIMEOUT},
  {  "kill_scd",		PARAM_BOOL,	"false"},
  {  "strict_open",		PARAM_BOOL,	"false"},
  { NULL,			PARAM_INVALID,	NULL},
};

struct config_param_s
{
  char *name;
  int type;
  union
  {
    int itype;
    char *cptype;
    char **cpptype;
    long ltype;
    long long lltype;
  } value;
};

struct config_keep_s
{
  char *section;
  char *name;
  char *value;
};

static struct config_section_s *config_find_section (struct slist_s *config,
					      const char *name);
static int new_param (struct config_section_s *section, const char *filename,
		      int lineno, const char *name, const char *value,
		      int type);
static void free_section (struct config_section_s *s);
static int set_defaults (struct slist_s **config, int reload);

static void
section_remove_param (struct config_section_s *section, const char *name)
{
  unsigned i, t = slist_length (section->params);

  for (i = 0; i < t; i++)
    {
      struct config_param_s *p = slist_nth_data (section->params, i);

      if (!p)
	continue;

      if (!strcmp (p->name, name))
	{
	  switch (p->type)
	    {
	    case PARAM_CHARP:
	      xfree (p->value.cptype);
	      break;
	    case PARAM_CHARPP:
	      strv_free (p->value.cpptype);
	      break;
	    }

	  section->params = slist_remove (section->params, p);
	  xfree (p->name);
	  xfree (p);
	  break;
	}
    }
}

static struct config_param_s *
config_has_param (struct config_section_s *s, const char *what)
{
  unsigned i, t = slist_length (s->params);

  for (i = 0; i < t; i++)
    {
      struct config_param_s *p = slist_nth_data (s->params, i);
      if (!p)
	break;

      if (!strcmp (p->name, what))
	return p;
    }

  return NULL;
}

static struct config_param_s *
config_get_param (struct slist_s *config,
		  const char *section, const char *what, int *exists)
{
  unsigned i, t = slist_length (config);

  *exists = 0;

  for (i = 0; i < t; i++)
    {
      struct config_param_s *p;
      struct config_section_s *s = slist_nth_data (config, i);

      if (!s)
	break;

      if (strcmp (s->name, section))
	continue;

      p = config_has_param (s, what);
      if (!p)
	return NULL;

      *exists = 1;
      return p;
    }

  return NULL;
}

static struct config_section_s *
new_section (struct slist_s **config, const char *name)
{
  struct slist_s *tmp;
  struct config_section_s *s = xcalloc (1, sizeof (struct config_section_s));

  if (!s)
    return NULL;

  s->name = str_dup (name);
  if (!s->name)
    {
      log_write ("%s", pwmd_strerror (ENOMEM));
      xfree (s);
      return NULL;
    }

  tmp = slist_append (*config, s);
  if (!tmp)
    {
      log_write ("%s", pwmd_strerror (ENOMEM));
      xfree (s->name);
      xfree (s);
      return NULL;
    }

  *config = tmp;
  return s;
}

int
config_set_string_param (struct slist_s **config, const char *section,
			 const char *name, const char *value)
{
  struct config_section_s *s = config_find_section (*config, section);

  if (!s)
    {
      s = new_section (config, section);
      if (!s)
	return 1;
    }

  return new_param (s, NULL, 0, name, value, PARAM_CHARP);
}

char *
config_get_string_param (struct slist_s *config, const char *section,
			 const char *what, int *exists)
{
  struct config_param_s *p = config_get_param (config, section, what, exists);
  return *exists && p->value.cptype ? str_dup (p->value.cptype) : NULL;
}

int
config_set_int_param (struct slist_s **config, const char *section,
		      const char *name, const char *value)
{
  struct config_section_s *s = config_find_section (*config, section);

  if (!s)
    {
      s = new_section (config, section);
      if (!s)
	return 1;
    }

  return new_param (s, NULL, 0, name, value, PARAM_INT);
}

int
config_get_int_param (struct slist_s *config, const char *section,
		      const char *what, int *exists)
{
  struct config_param_s *p = config_get_param (config, section, what, exists);
  return *exists ? p->value.itype : -1;
}

int
config_set_bool_param (struct slist_s **config, const char *section,
		       const char *name, const char *value)
{
  struct config_section_s *s = config_find_section (*config, section);

  if (!s)
    {
      s = new_section (config, section);
      if (!s)
	return 1;
    }

  return new_param (s, NULL, 0, name, value, PARAM_BOOL);
}

int
config_get_bool_param (struct slist_s *config, const char *section,
		       const char *what, int *exists)
{
  return config_get_int_param (config, section, what, exists);
}

int
config_set_long_param (struct slist_s **config, const char *section,
		       const char *name, const char *value)
{
  struct config_section_s *s = config_find_section (*config, section);

  if (!s)
    {
      s = new_section (config, section);
      if (!s)
	return 1;
    }

  return new_param (s, NULL, 0, name, value, PARAM_LONG);
}

long
config_get_long_param (struct slist_s *config, const char *section,
		       const char *what, int *exists)
{
  struct config_param_s *p = config_get_param (config, section, what, exists);
  return *exists ? p->value.ltype : -1;
}

int
config_set_longlong_param (struct slist_s **config, const char *section,
			   const char *name, const char *value)
{
  struct config_section_s *s = config_find_section (*config, section);

  if (!s)
    {
      s = new_section (config, section);
      if (!s)
	return 1;
    }

  return new_param (s, NULL, 0, name, value, PARAM_LONGLONG);
}

long long
config_get_longlong_param (struct slist_s *config,
			   const char *section, const char *what, int *exists)
{
  struct config_param_s *p = config_get_param (config, section, what, exists);
  return *exists ? p->value.lltype : -1;
}

int
config_set_list_param (struct slist_s **config, const char *section,
		       const char *name, const char *value)
{
  struct config_section_s *s = config_find_section (*config, section);

  if (!s)
    {
      s = new_section (config, section);
      if (!s)
	return 1;
    }

  return new_param (s, NULL, 0, name, value, PARAM_CHARPP);
}

char **
config_get_list_param (struct slist_s *config, const char *section,
		       const char *what, int *exists)
{
  struct config_param_s *p = config_get_param (config, section, what, exists);
  return *exists && p->value.cpptype ? strv_dup (p->value.cpptype) : NULL;
}

char *
config_get_string (const char *section, const char *what)
{
  char *val = NULL;
  const char *where = section ? section : "global";
  int exists = 0;

  MUTEX_LOCK (&rcfile_mutex);
  val = config_get_string_param (global_config, where, what, &exists);
  if (!exists && strcmp (section ? section : "", "global"))
    val = config_get_string_param (global_config, "global", what, &exists);

  MUTEX_UNLOCK (&rcfile_mutex);
  return val;
}

char **
config_get_list (const char *section, const char *what)
{
  char **val = NULL;
  const char *where = section ? section : "global";
  int exists = 0;

  MUTEX_LOCK (&rcfile_mutex);
  val = config_get_list_param (global_config, where, what, &exists);
  if (!exists && strcmp (section ? section : "", "global"))
    val = config_get_list_param (global_config, "global", what, &exists);

  MUTEX_UNLOCK (&rcfile_mutex);
  return val;
}

int
config_get_integer (const char *section, const char *what)
{
  int val = 0;
  const char *where = section ? section : "global";
  int exists = 0;

  MUTEX_LOCK (&rcfile_mutex);
  val = config_get_int_param (global_config, where, what, &exists);
  if (!exists && strcmp (section ? section : "", "global"))
    val = config_get_int_param (global_config, "global", what, &exists);

  MUTEX_UNLOCK (&rcfile_mutex);
  return val;
}

long long
config_get_longlong (const char *section, const char *what)
{
  long long val = 0;
  const char *where = section ? section : "global";
  int exists = 0;

  MUTEX_LOCK (&rcfile_mutex);
  val = config_get_longlong_param (global_config, where, what, &exists);
  if (!exists && strcmp (section ? section : "", "global"))
    val = config_get_longlong_param (global_config, "global", what, &exists);

  MUTEX_UNLOCK (&rcfile_mutex);
  return val;
}

long
config_get_long (const char *section, const char *what)
{
  long val = 0;
  const char *where = section ? section : "global";
  int exists = 0;

  MUTEX_LOCK (&rcfile_mutex);
  val = config_get_long_param (global_config, where, what, &exists);
  if (!exists && strcmp (section ? section : "", "global"))
    val = config_get_long_param (global_config, "global", what, &exists);

  MUTEX_UNLOCK (&rcfile_mutex);
  return val;
}

int
config_get_boolean (const char *section, const char *what)
{
  return config_get_integer (section, what);
}

char *
config_get_value (const char *section, const char *what)
{
  const char *where = section ? section : "global";
  int exists = 0;
  int i;
  int ival;
  long lval;
  long long llval;
  char *cpval;
  char **cppval;
  char *result = NULL;

  MUTEX_LOCK (&rcfile_mutex);

  for (i = 0; config_params[i].name; i++)
    {
      if (!strcmp (config_params[i].name, what))
	{
	  switch (config_params[i].type)
	    {
	    case PARAM_BOOL:
	    case PARAM_INT:
	      ival = config_get_int_param (global_config, where, what,
					   &exists);
	      if (!exists && strcmp (section ? section : "", "global"))
		ival = config_get_int_param (global_config, "global", what,
					     &exists);
	      result = str_asprintf ("%i", ival);
	      break;
	    case PARAM_CHARP:
	      cpval = config_get_string_param (global_config, where, what,
					       &exists);
	      if (!exists && strcmp (section ? section : "", "global"))
		cpval =
		  config_get_string_param (global_config, "global", what,
					   &exists);
	      result = cpval;
	      break;
	    case PARAM_LONG:
	      lval = config_get_long_param (global_config, where, what,
					    &exists);
	      if (!exists && strcmp (section ? section : "", "global"))
		lval = config_get_long_param (global_config, "global", what,
					      &exists);
	      result = str_asprintf ("%li", lval);
	      break;
	    case PARAM_LONGLONG:
	      llval = config_get_longlong_param (global_config, where, what,
						 &exists);
	      if (!exists && strcmp (section ? section : "", "global"))
		llval = config_get_longlong_param (global_config, "global",
						   what, &exists);
	      result = str_asprintf ("%lli", llval);
	      break;
	    case PARAM_CHARPP:
	      cppval = config_get_list_param (global_config, where, what,
					      &exists);
	      if (!exists && strcmp (section ? section : "", "global"))
		cppval = config_get_list_param (global_config, "global", what,
						&exists);

	      if (cppval)
		result = strv_join (",", cppval);

	      strv_free (cppval);
	      break;
	    }
	}
    }

  MUTEX_UNLOCK (&rcfile_mutex);
  return result;
}

/* 'file' is the list parameter file to load into the list parameter 'what'.
 * The parsing of the parameter is not done here. */
static gpg_error_t
parse_list_file (struct slist_s *config, const char *section,
                 const char *file, const char *what)
{
  FILE *fp;
  char buf[LINE_MAX] = {0};
  int exists;
  char **list = NULL;
  char *tmp;
  char *p = config_get_string_param (config, section, file, &exists);
  gpg_error_t rc;

  if (!p || !*p)
    {
      xfree (p);
      return 0;
    }

  tmp = expand_homedir (p);
  xfree (p);
  p = tmp;
  fp = fopen (p, "r");
  if (!fp)
    {
      rc = gpg_error_from_errno (errno);
      log_write ("%s: %s", p, pwmd_strerror (rc));
      xfree (p);
      return rc;
    }

  xfree (p);
  list = config_get_list_param (config, section, what, &exists);
  if (!list && exists)
    {
      fclose (fp);
      log_write ("%s", pwmd_strerror (ENOMEM));
      return gpg_error (ENOMEM);
    }

  while ((p = fgets (buf, sizeof (buf)-1, fp)))
    {
      char **pp = NULL;

      if (p[strlen(p)-1] == '\n')
        p[strlen(p)-1] = 0;

      while (*p && isspace (*p))
        p++;

      if (!*p || *p == ';')
        continue;

      tmp = str_dup (p);
      if (tmp)
        pp = strv_cat (list, str_dup (p));

      if (!pp || !tmp)
        {
          xfree (tmp);
          strv_free (list);
          fclose (fp);
          log_write ("%s", strerror (ENOMEM));
          return gpg_error (ENOMEM);
        }

      xfree (tmp);
      list = pp;
    }

  fclose(fp);
  if (!list)
    return 0;

  p = strv_join (",", list);
  strv_free (list);

  if (!p)
    {
      log_write ("%s", pwmd_strerror (ENOMEM));
      return gpg_error (ENOMEM);
    }

  config_set_list_param (&config, section, what, p);
  xfree (p);
  return 0;
}

static int
fixup_allowed_once (struct slist_s **config, const char *section)
{
  char **list, **pp, *p;
  int exists;
  gpg_error_t rc;

  rc = parse_list_file (*config, section, "allowed_file", "allowed");
  if (rc)
    return 1;

  list = config_get_list_param (*config, section, "allowed", &exists);
  for (pp = list; pp && *pp; pp++)
    {
      if (*(*pp) == '#')
        {
          for (p = *pp; p && *p; p++)
            *p = toupper(*p);
        }
    }

  strv_free (list);
  if (!exists)
    {
      if (!strcmp (section, "global"))
        {
          p = get_username (getuid());

          if (config_set_list_param (config, section, "allowed", p))
            {
              xfree (p);
              return 1;
            }

          xfree (p);
        }
      else
        {
          list = config_get_list_param (*config, "global", "allowed", &exists);
          if (list)
            {
              p = strv_join (",", list);
              strv_free (list);
              if (config_set_list_param (config, section, "allowed", p))
                {
                  xfree (p);
                  return 1;
                }

              xfree (p);
            }
        }
    }

  return 0;
}

static int
fixup_allowed (struct slist_s **config)
{
  int n, t = slist_length (*config);

  for (n = 0; n < t; n++)
    {
      struct config_section_s *section;

      section = slist_nth_data (*config, n);
      if (fixup_allowed_once (config, section->name))
        return 1;
    }

  return 0;
}

static int
add_invoking_user (struct invoking_user_s **users, char *id,
                   struct slist_s **config)
{
  struct passwd *pwd = NULL;
  struct group *grp = NULL;
  struct invoking_user_s *user, *p;
  int not = 0;

  if (id && (*id == '!' || *id == '-'))
    {
      not = 1;
      id++;
    }

  errno = 0;
  if (!id || !*id)
    {
      pwd = getpwuid (getuid ());
      if (!pwd)
        {
          log_write (_("could not set any invoking user: %s"),
                     pwmd_strerror (errno ? errno : GPG_ERR_INV_VALUE));
          return 1;
        }
    }
  else if (*id == '@')
    {
      grp = getgrnam (id+1);
      if (!grp)
        {
          log_write (_("could not parse group '%s': %s"), id+1,
                     pwmd_strerror (errno ? errno : GPG_ERR_INV_VALUE));
          return 1;
        }
    }
  else if (*id != '#')
    pwd = getpwnam (id);

  if (!grp && !pwd && id && *id != '#')
    {
      if (id && *id)
        log_write (_("could not set invoking user '%s': %s"), id,
                   pwmd_strerror (errno ? errno : GPG_ERR_INV_VALUE));
      else
        log_write (_("could not set any invoking user!"));

      return 1;
    }

  user = xcalloc (1, sizeof (struct invoking_user_s));
  if (!user)
    {
      log_write ("%s", pwmd_strerror (ENOMEM));
      return 1;
    }

  user->not = not;
  user->type = pwd ? INVOKING_UID : grp ? INVOKING_GID : INVOKING_TLS;
  if (pwd)
    user->uid = pwd->pw_uid;
  else if (grp)
    user->id = str_dup (id+1);
  else
    {
      char *s;

      for (s = id; s && *s; s++)
        *s = toupper(*s);

      user->id = str_dup (id+1);
    }

  /* Set the default invoking_user since it doesn't exist. */
  if (pwd && (!id || !*id))
    config_set_list_param (config, "global", "invoking_user", pwd->pw_name);

  if (!*users)
    {
      *users = user;
      return 0;
    }

  for (p = *users; p; p = p->next)
    {
      if (!p->next)
        {
          p->next = user;
          break;
        }
    }

  return 0;
}

static int
parse_invoking_users (struct slist_s **config)
{
  struct invoking_user_s *users = NULL;
  int exists;
  char **list, **l;

  if (parse_list_file (*config, "global", "invoking_file", "invoking_user"))
    return 1;

  list = config_get_list_param (*config, "global", "invoking_user", &exists);
  for (l = list; l && *l; l++)
    {
      if (add_invoking_user (&users, *l, config))
        {
          strv_free (list);
          free_invoking_users (users);
          return 1;
        }
    }

  if (!list || !*list)
    {
      config_set_list_param (config, "global", "invoking_user", DEFAULT_INVOKER);
      if (add_invoking_user (&users, (char *)DEFAULT_INVOKER, config))
        {
          strv_free (list);
          return 1;
        }
    }

  free_invoking_users (invoking_users);
  invoking_users = users;
  strv_free (list);
  return 0;
}

static int
set_defaults (struct slist_s **config, int reload)
{
  char *s = NULL;
  char **list;
  int exists;
  int i;

  for (i = 0; config_params[i].name; i++)
    {
      switch (config_params[i].type)
	{
	case PARAM_BOOL:
	  config_get_bool_param (*config, "global", config_params[i].name,
				 &exists);
	  if (!exists)
	    {
	      if (config_set_bool_param
		  (config, "global", config_params[i].name,
		   config_params[i].value))
		goto fail;
	    }
	  break;
	case PARAM_INT:
	  config_get_int_param (*config, "global", config_params[i].name,
				&exists);
	  if (!exists)
	    {
	      if (config_set_int_param
		  (config, "global", config_params[i].name,
		   config_params[i].value))
		goto fail;
	    }
	  break;
	case PARAM_CHARP:
	  s = config_get_string_param (*config, "global",
					config_params[i].name, &exists);
	  xfree (s);
	  if (!exists && config_params[i].value)
	    {
	      if (config_set_string_param (config, "global",
					   config_params[i].name,
					   config_params[i].value))
		goto fail;
	    }
	  break;
	case PARAM_CHARPP:
	  list = config_get_list_param (*config, "global",
					config_params[i].name, &exists);
	  strv_free (list);
	  if (!exists && config_params[i].value)
	    {
	      if (config_set_list_param (config, "global",
					 config_params[i].name,
					 config_params[i].value))
		goto fail;
	    }
	  break;
	case PARAM_LONG:
	  config_get_long_param (*config, "global", config_params[i].name,
				 &exists);
	  if (!exists)
	    {
	      if (config_set_long_param
		  (config, "global", config_params[i].name,
		   config_params[i].value))
		goto fail;
	    }
	  break;
	case PARAM_LONGLONG:
	  config_get_longlong_param (*config, "global", config_params[i].name,
				     &exists);
	  if (!exists)
	    {
	      if (config_set_longlong_param (config, "global",
					     config_params[i].name,
					     config_params[i].value))
		goto fail;
	    }
	  break;
	}
    }

  s = NULL;
  if (!reload && fixup_allowed (config))
    goto fail;

  if (!reload && parse_invoking_users (config))
    goto fail;

  log_level = config_get_int_param (*config, "global",
                                    "log_level", &exists);
  log_keepopen = config_get_int_param (*config, "global",
                                       "log_keepopen", &exists);
  max_recursion_depth = config_get_int_param (*config, "global",
					      "recursion_depth", &exists);
  disable_list_and_dump = config_get_bool_param (*config, "global",
						 "disable_list_and_dump",
						 &exists);
#ifdef HAVE_MLOCKALL
  disable_mlock =
    config_get_bool_param (*config, "global", "disable_mlockall", &exists);
#endif

  xfree (s);
  return 0;

fail:
  xfree (s);
  return 1;
}

static struct config_section_s *
config_find_section (struct slist_s *config, const char *name)
{
  unsigned i, t = slist_length (config);

  for (i = 0; i < t; i++)
    {
      struct config_section_s *s = slist_nth_data (config, i);

      if (!strcmp (s->name, name))
	return s;
    }

  return NULL;
}

/* Append a new parameter to the list of parameters for a file
 * section. When an existing parameter of the same name exists, its
 * value is updated.
 */
static int
new_param (struct config_section_s *section, const char *filename, int lineno,
	   const char *name, const char *value, int type)
{
  struct config_param_s *param = NULL;
  struct slist_s *tmp;
  char *e;
  unsigned i, t = slist_length (section->params);
  int dup = 0;

  for (i = 0; i < t; i++)
    {
      struct config_param_s *p = slist_nth_data (section->params, i);
      if (!p)
	break;

      if (!strcmp (name, p->name))
	{
	  param = p;
	  dup = 1;
	  break;
	}
    }

  if (!param)
    {
      param = xcalloc (1, sizeof (struct config_param_s));
      if (!param)
	{
	  log_write ("%s", pwmd_strerror (ENOMEM));
	  return 1;
	}

      param->name = str_dup (name);
      if (!param->name)
	{
	  xfree (param);
	  log_write ("%s", pwmd_strerror (ENOMEM));
	  return 1;
	}
    }

  param->type = type;

  switch (type)
    {
    case PARAM_BOOL:
      if (!strcasecmp (value, "no") || !strcasecmp (value, "0")
	  || !strcasecmp (value, "false"))
	param->value.itype = 0;
      else if (!strcasecmp (value, "yes") || !strcasecmp (value, "1")
	       || !strcasecmp (value, "true"))
	param->value.itype = 1;
      else
	{
	  INVALID_VALUE (filename, lineno);
	  goto fail;
	}
      param->type = PARAM_INT;
      break;
    case PARAM_CHARP:
      xfree (param->value.cptype);
      param->value.cptype = NULL;
      param->value.cptype = value && *value ? str_dup (value) : NULL;
      if (value && *value && !param->value.cptype)
	{
	  log_write ("%s", pwmd_strerror (ENOMEM));
	  goto fail;
	}
      break;
    case PARAM_CHARPP:
      strv_free (param->value.cpptype);
      param->value.cpptype = NULL;
      param->value.cpptype = value && *value ?
        str_split_ws (value, ",", 0) : NULL;
      if (value && *value && !param->value.cpptype)
	{
	  log_write ("%s", pwmd_strerror (ENOMEM));
	  goto fail;
	}
      break;
    case PARAM_INT:
      param->value.itype = strtol (value, &e, 10);
      if (e && *e)
	{
	  INVALID_VALUE (filename, lineno);
	  goto fail;
	}
      break;
    case PARAM_LONG:
      param->value.ltype = strtol (value, &e, 10);
      if (e && *e)
	{
	  INVALID_VALUE (filename, lineno);
	  goto fail;
	}
      break;
    case PARAM_LONGLONG:
      param->value.lltype = strtoll (value, &e, 10);
      if (e && *e)
	{
	  INVALID_VALUE (filename, lineno);
	  goto fail;
	}
      break;
    }

  if (dup)
    return 0;

  tmp = slist_append (section->params, param);
  if (!tmp)
    {
      log_write ("%s", pwmd_strerror (ENOMEM));
      goto fail;
    }

  section->params = tmp;
  return 0;

fail:
  xfree (param->name);
  xfree (param);
  return 1;
}

struct slist_s *
config_parse (const char *filename, int reload)
{
  struct slist_s *tmpconfig = NULL, *tmp;
  struct config_section_s *cur_section = NULL;
  char buf[LINE_MAX] = {0};
  char *s;
  int lineno = 1;
  int have_global = 0;
  FILE *fp = fopen (filename, "r");

  if (!fp)
    {
      log_write ("%s: %s", filename,
		 pwmd_strerror (gpg_error_from_errno (errno)));

      if (errno != ENOENT)
	return NULL;

      log_write (_("Using defaults!"));
      goto defaults;
    }

  for (; (s = fgets (buf, sizeof (buf), fp)); lineno++)
    {
      char line[LINE_MAX] = { 0 };
      size_t len = 0;
      int match = 0;

      if (*s == '#')
	continue;

      s = str_chomp (s);
      if (!*s)
        continue;

      /* New file section. */
      if (*s == '[')
        {
          struct config_section_s *section;
          char *p = strchr (++s, ']');

          if (!p)
            {
              log_write (_("%s(%i): unbalanced braces"), filename, lineno);
              goto fail;
            }

          if (*(p+1))
            {
              log_write (_("%s(%i): trailing characters"), filename, lineno);
              goto fail;
            }

          len = strlen (s) - strlen (p);
          memcpy (line, s, len);
          line[len] = 0;

          section = config_find_section (tmpconfig, line);
          if (section)
            {
              log_write (_("%s(%i): section '%s' already exists!"),
                         filename, lineno, line);
              goto fail;
            }

          if (!strcmp (line, "global"))
            have_global = 1;

          section = xcalloc (1, sizeof (struct config_section_s));
          section->name = str_dup (line);

          if (cur_section)
            {
              tmp = slist_append (tmpconfig, cur_section);
              if (!tmp)
                {
                  log_write ("%s", pwmd_strerror (ENOMEM));
                  goto fail;
                }

              tmpconfig = tmp;
            }

          cur_section = section;
          continue;
        }

      if (!cur_section)
        {
          log_write (_("%s(%i): parameter outside of section!"), filename,
                     lineno);
          goto fail;
        }

      /* Parameters for each section. */
      for (int m = 0; config_params[m].name; m++)
        {
          len = strlen (config_params[m].name);
          if (!strncmp (s, config_params[m].name, len))
            {
              char *p = s + len;

              while (*p && *p == ' ')
                p++;

              if (!*p || *p != '=')
                continue;

              p++;
              while (*p && isspace (*p))
                p++;

              s[len] = 0;
              if (new_param (cur_section, filename, lineno, s, p,
                             config_params[m].type))
                goto fail;

              match = 1;
              break;
            }
        }

      if (!match)
        {
          log_write (_("%s(%i): unknown parameter"), filename, lineno);
          goto fail;
        }
    }

  if (cur_section)
    {
      tmp = slist_append (tmpconfig, cur_section);
      if (!tmp)
	{
	  log_write ("%s", pwmd_strerror (ENOMEM));
	  goto fail;
	}

      cur_section = NULL;
      tmpconfig = tmp;
    }

  if (!have_global)
    log_write (_
	       ("WARNING: %s: could not find a [global] configuration section!"),
	       filename);

defaults:
  if (set_defaults (&tmpconfig, reload))
    goto fail;

  if (fp)
    fclose(fp);

  return tmpconfig;

fail:
  if (fp)
    fclose (fp);

  config_free (tmpconfig);
  free_section (cur_section);
  return NULL;
}

static void
free_section (struct config_section_s *s)
{
  if (!s)
    return;

  for (;;)
    {
      struct config_param_s *p = slist_nth_data (s->params, 0);

      if (!p)
	break;

      section_remove_param (s, p->name);
    }

  s->params = NULL;
  xfree (s->name);
  s->name = NULL;
}

void
config_free (struct slist_s *config)
{
  for (;;)
    {
      struct config_section_s *s = slist_nth_data (config, 0);
      if (!s)
	break;

      free_section (s);
      config = slist_remove (config, s);
      xfree (s);
    }
}

void
free_invoking_users (struct invoking_user_s *users)
{
  struct invoking_user_s *p;

  for (p = users; p;)
    {
      struct invoking_user_s *next = p->next;

      if (p->type == INVOKING_TLS || p->type == INVOKING_GID)
        xfree (p->id);

      xfree (p);
      p = next;
    }
}

static int
param_type (const char *name)
{
  int i;

  for (i = 0; config_params[i].name; i++)
    {
      if (!strcmp (config_params[i].name, name))
        return config_params[i].type;
    }

  return PARAM_INVALID;
}

static int
keep_parse (struct config_keep_s *k, const char *section, const char *key)
{
  int exists;
  int ival;
  long lval;
  long long llval;
  char *cpval;
  char **cppval;
  int type = param_type (key);
  void *value = NULL;

  switch (type)
    {
    case PARAM_BOOL:
    case PARAM_INT:
      ival = config_get_int_param (global_config, section, key, &exists);
      if (exists)
          value = str_asprintf ("%i", ival);
      break;
    case PARAM_LONG:
      lval = config_get_long_param (global_config, section, key, &exists);
      if (exists)
        value = str_asprintf ("%li", lval);
      break;
    case PARAM_LONGLONG:
      llval = config_get_longlong_param (global_config, section, key, &exists);
      if (exists)
        value = str_asprintf ("%lli", llval);
      break;
    case PARAM_CHARP:
      cpval = config_get_string_param (global_config, section, key, &exists);
      if (exists)
        value = cpval;
      break;
    case PARAM_CHARPP:
      cppval = config_get_list_param (global_config, section, key, &exists);
      if (exists)
        {
          char *s = strv_join (",", cppval);

          strv_free (cppval);
          value = s;
        }
      break;
    default:
      return 1;
    }

  if (!value)
    return 1;

  k->section = str_dup(section);
  k->name = str_dup(key);
  k->value = value;
  return 0;
}

static struct slist_s *
keep_add (struct slist_s *k, const char *s, const char *key)
{
  int n, t = slist_length (global_config);

  for (n = 0; n < t; n++)
    {
      struct config_section_s *section;
      struct config_keep_s *tmp;
      int ret;

      section = slist_nth_data (global_config, n);
      tmp = xcalloc (1, sizeof(struct config_keep_s));

      // Process all sections.
      if (!s)
        ret = keep_parse (tmp, section->name, key);
      else
        ret = keep_parse (tmp, s, key);

      if (!ret)
        k = slist_append (k, tmp);
      else
        xfree (tmp);
    }

  return k;
}

/* Keep security sensitive settings across SIGHUP. */
struct slist_s *
config_keep_save ()
{
  struct slist_s *keep = NULL;

#ifdef WITH_GNUTLS
  keep = keep_add (keep, NULL, "tcp_require_key");
#endif
  keep = keep_add (keep, NULL, "require_save_key");
  keep = keep_add (keep, NULL, "allowed");
  keep = keep_add (keep, NULL, "allowed_file");
  keep = keep_add (keep, "global", "encrypt_to");
  keep = keep_add (keep, "global", "always_trust");
  keep = keep_add (keep, "global", "invoking_user");
  keep = keep_add (keep, "global", "invoking_file");
  keep = keep_add (keep, "global", "gpg_homedir");
  return keep;
}

/* Restore parameters previously saved with config_keep_save(). This will also
 * free the 'keep'.
 */
void
config_keep_restore (struct slist_s *keep)
{
  int n, t = slist_length (keep);

  for (n = 0; n < t; n++)
    {
      struct config_keep_s *k = slist_nth_data (keep, n);
      int type = param_type (k->name);

      switch (type)
        {
        case PARAM_BOOL:
          config_set_bool_param (&global_config, k->section, k->name, k->value);
          break;
        case PARAM_INT:
          config_set_int_param (&global_config, k->section, k->name, k->value);
          break;
        case PARAM_LONG:
          config_set_long_param (&global_config, k->section, k->name, k->value);
          break;
        case PARAM_LONGLONG:
          config_set_longlong_param (&global_config, k->section, k->name,
                                     k->value);
          break;
        case PARAM_CHARP:
          config_set_string_param (&global_config, k->section, k->name,
                                   k->value);
          break;
        case PARAM_CHARPP:
          config_set_list_param (&global_config, k->section, k->name, k->value);
          break;
        default:
          break;
        }

      xfree (k->section);
      xfree (k->name);
      xfree (k->value);
      xfree (k);
    }

  slist_free (keep);
}
