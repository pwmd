/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CACHE_H
#define CACHE_H

#include <pthread.h>
#include <gpg-error.h>
#include <libxml/tree.h>
#include <time.h>

#include "crypto.h"

struct cache_data_s
{
  char **pubkey;	/* keyid's */
  char *sigkey;		/* signing keyid */
  void *doc;		/* AES-128 encrypted */
  xmlDocPtr plaintext;
  unsigned refcount;	/* for .plaintext */
  size_t size;
  unsigned char *crc;   /* Checksum of the data file. */
  char *filename;
};

typedef struct
{
  char **grip;			/* Public keygrips. */
  char *siggrip;		/* Signing keygrip. */
  char *filename;		/* Associated filename. */
  long timeout;			/* cache_timer_thread(). */
  long reset;			/* To reset .timeout. */
  int defer_clear;		/* Another thread wants to clear the cache
				   entry for this file. Prevent resetting the
				   timer until this flag is cleared. */
  pthread_mutex_t *mutex;	/* Data file mutex. */
  struct cache_data_s *data;
  unsigned refcount;
  unsigned flags;
} file_cache_t;

gpg_error_t cache_kill_scd ();
void cache_adjust_timeout ();
gpg_error_t cache_set_timeout (const char *, long timeout);
gpg_error_t cache_iscached (const char *, int *defer, int agent, int sign);
gpg_error_t cache_clear (struct client_s *, const char *, int agent, int force);
gpg_error_t cache_add_file (const char *, struct cache_data_s *, long timeout,
                            int same_file);
void cache_deinit ();
gpg_error_t cache_init ();
void cache_mutex_init ();
unsigned cache_file_count ();
gpg_error_t cache_is_shadowed (const char *filename);
struct cache_data_s *cache_get_data (const char *, int *defer);
gpg_error_t cache_set_data (const char *, struct cache_data_s *);
gpg_error_t cache_lock_mutex (void *ctx, const char *, long mutex_timeout,
                              int add, long timeout);
gpg_error_t cache_unlock_mutex (const char *, int remove);
void cache_lock ();
void cache_unlock ();
void cache_free_data_once (struct cache_data_s *data);
gpg_error_t cache_defer_clear (const char *);
gpg_error_t cache_encrypt (struct crypto_s *);
gpg_error_t cache_decrypt (struct crypto_s *);
gpg_error_t cache_clear_agent_keys (const char *, int decrypt, int sign);
gpg_error_t cache_agent_command (const char *);
gpg_error_t cache_plaintext_get (const char *filename, xmlDocPtr *plain);
gpg_error_t cache_plaintext_release (xmlDocPtr *);
gpg_error_t cache_plaintext_set (const char *filename, xmlDocPtr doc,
                                 int modified);

#endif
