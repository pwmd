/*
    Copyright (C) 2016-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef AGENT_H
#define AGENT_H

struct agent_inquire_s
{
  char *line;
  size_t len;
};

struct agent_s
{
  char *socket;
  assuan_context_t ctx;
  int restart;
  int did_restart;
  struct agent_inquire_s *inquire;
  size_t inquire_maxlen;
  membuf_t data;
};

gpg_error_t agent_init (struct agent_s **);
gpg_error_t agent_connect (struct agent_s *);
void agent_free (struct agent_s *agent);
gpg_error_t agent_command (struct agent_s *agent, char **result, size_t * len,
                           const char *fmt, ...);
gpg_error_t agent_set_option (struct agent_s *agent, const char *name,
			      const char *value);
gpg_error_t agent_kill_scd (struct agent_s *);
void agent_disconnect (struct agent_s *agent);

#endif
