/*
    Copyright (C) 2015-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <arpa/inet.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <err.h>
#include <termios.h>

#include "pwmd-error.h"
#include <gcrypt.h>

#ifdef HAVE_SYS_PRCTL_H
#include <sys/prctl.h>
#endif

#ifdef ENABLE_NLS
#ifdef HAVE_LOCALE_H
#include <locale.h>
#endif
#endif

#ifndef _
#include "gettext.h"
#define _(msgid) gettext(msgid)
#endif

#ifdef HAVE_GETOPT_LONG
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif
#else
#include "getopt_long.h"
#endif

#include "util-string.h"
#include "mem.h"
#include "version.h"

#if 0
#ifdef WITH_AGENT
#include "agent.h"
#endif
#endif

#define PWMD_CIPHER_OFFSET	(1)
#define PWMD_CIPHER(n)		(PWMD_CIPHER_OFFSET << n)
#define PWMD_CIPHER_AES128	PWMD_CIPHER (0)
#define PWMD_CIPHER_AES192	PWMD_CIPHER (1)
#define PWMD_CIPHER_AES256	PWMD_CIPHER (2)
#define PWMD_CIPHER_SERPENT128	PWMD_CIPHER (3)
#define PWMD_CIPHER_SERPENT192	PWMD_CIPHER (4)
#define PWMD_CIPHER_SERPENT256	PWMD_CIPHER (5)
#define PWMD_CIPHER_CAMELLIA128	PWMD_CIPHER (6)
#define PWMD_CIPHER_CAMELLIA192	PWMD_CIPHER (7)
#define PWMD_CIPHER_CAMELLIA256	PWMD_CIPHER (8)
#define PWMD_CIPHER_3DES	PWMD_CIPHER (9)
#define PWMD_CIPHER_CAST5	PWMD_CIPHER (10)
#define PWMD_CIPHER_BLOWFISH	PWMD_CIPHER (11)
#define PWMD_CIPHER_TWOFISH	PWMD_CIPHER (12)
#define PWMD_CIPHER_TWOFISH128	PWMD_CIPHER (13)

#define PWMD_FLAG_OFFSET	(PWMD_CIPHER_OFFSET << 15)
#define PWMD_FLAG(n)		(PWMD_FLAG_OFFSET << n)
#define PWMD_FLAG_PKI           PWMD_FLAG (1)
#define PWMD_FLAG_NO_PASSPHRASE PWMD_FLAG (2)

#define KEYSIZE	32

typedef struct
{
  uint8_t magic[5];
  uint32_t version;
  uint64_t s2k_count;
  uint64_t flags;
  uint8_t iv[16];
  uint8_t salt[8];
  uint32_t datalen;		/* of the encrypted xml */
} __attribute__ ((packed)) file_header_t;

struct save_s
{
  gcry_sexp_t pkey;		/* SAVE --keygrip */
  gcry_sexp_t sigpkey;		/* SAVE --sign-keygrip */
  file_header_t hdr;
};

struct crypto_s
{
  //assuan_context_t client_ctx;
#ifdef WITH_AGENT
  struct agent_s *agent;
#endif
  struct save_s save;
  gcry_sexp_t pkey_sexp;
  unsigned char grip[20];
  gcry_sexp_t sigpkey_sexp;
  unsigned char sign_grip[20];
  gcry_sexp_t ciphertext_sexp;
  void *ciphertext;
  size_t ciphertext_len;
  void *plaintext;
  size_t plaintext_len;
  file_header_t hdr;
  char *filename;		/* the currently opened data file */
};

#define DEFAULT_KDFS2K_ITERATIONS 5000000
#define COMPAT_KDFS2K_ITERATIONS 1000

const char *reserved_attributes[] = {
    "_name", "_mtime", "_ctime", "_acl", "_target",
    NULL
};
static unsigned char crypto_magic[5] = "\177PWMD";
static int use_agent;

static void set_header_defaults (file_header_t * hdr);
static gpg_error_t decrypt_common (struct crypto_s *crypto,
                                   const char *filename, const char *keyfile);
static gpg_error_t read_data_file (const char *filename,
                                   struct crypto_s *crypto);
static void cleanup_crypto_stage1 (struct crypto_s *cr);
static void cleanup_crypto (struct crypto_s **c);

static int
cipher_to_gcrypt (int flags)
{
  if (flags < 0)
    return flags;

  if (flags & PWMD_CIPHER_AES128)
    return GCRY_CIPHER_AES128;
  else if (flags & PWMD_CIPHER_AES192)
    return GCRY_CIPHER_AES192;
  else if (flags & PWMD_CIPHER_AES256)
    return GCRY_CIPHER_AES256;
  else if (flags & PWMD_CIPHER_SERPENT128)
    return GCRY_CIPHER_SERPENT128;
  else if (flags & PWMD_CIPHER_SERPENT192)
    return GCRY_CIPHER_SERPENT192;
  else if (flags & PWMD_CIPHER_SERPENT256)
    return GCRY_CIPHER_SERPENT256;
  else if (flags & PWMD_CIPHER_CAMELLIA128)
    return GCRY_CIPHER_CAMELLIA128;
  else if (flags & PWMD_CIPHER_CAMELLIA192)
    return GCRY_CIPHER_CAMELLIA192;
  else if (flags & PWMD_CIPHER_CAMELLIA256)
    return GCRY_CIPHER_CAMELLIA256;
  else if (flags & PWMD_CIPHER_BLOWFISH)
    return GCRY_CIPHER_BLOWFISH;
  else if (flags & PWMD_CIPHER_3DES)
    return GCRY_CIPHER_3DES;
  else if (flags & PWMD_CIPHER_CAST5)
    return GCRY_CIPHER_CAST5;
  else if (flags & PWMD_CIPHER_TWOFISH)
    return GCRY_CIPHER_TWOFISH;
  else if (flags & PWMD_CIPHER_TWOFISH128)
    return GCRY_CIPHER_TWOFISH128;

  return -1;
}

static int
cipher_string_to_cipher (const char *str)
{
  int flags = 0;

  if (!strcasecmp (str, "aes128"))
    flags = PWMD_CIPHER_AES128;
  else if (!strcasecmp (str, "aes192"))
    flags = PWMD_CIPHER_AES192;
  else if (!strcasecmp (str, "aes256"))
    flags = PWMD_CIPHER_AES256;
  else if (!strcasecmp (str, "serpent128"))
    flags = PWMD_CIPHER_SERPENT128;
  else if (!strcasecmp (str, "serpent192"))
    flags = PWMD_CIPHER_SERPENT192;
  else if (!strcasecmp (str, "serpent256"))
    flags = PWMD_CIPHER_SERPENT256;
  else if (!strcasecmp (str, "camellia128"))
    flags = PWMD_CIPHER_CAMELLIA128;
  else if (!strcasecmp (str, "camellia192"))
    flags = PWMD_CIPHER_CAMELLIA192;
  else if (!strcasecmp (str, "camellia256"))
    flags = PWMD_CIPHER_CAMELLIA256;
  else if (!strcasecmp (str, "blowfish"))
    flags = PWMD_CIPHER_BLOWFISH;
  else if (!strcasecmp (str, "cast5"))
    flags = PWMD_CIPHER_CAST5;
  else if (!strcasecmp (str, "3des"))
    flags = PWMD_CIPHER_3DES;
  else if (!strcasecmp (str, "twofish256"))
    flags = PWMD_CIPHER_TWOFISH;
  else if (!strcasecmp (str, "twofish128"))
    flags = PWMD_CIPHER_TWOFISH128;
  else
    return -1;

  return flags;
}

static void
usage (const char *pn, int rc)
{
  fprintf(rc == EXIT_SUCCESS ? stdout : stderr,
          "%s [-hvn] [--force] [-k <filename>] [--xml] <infile> <outfile>\n"
          "    --no-convert, -n don't discard data for elements with targets\n"
          "    --force          don't abort when converting\n"
          "    --xml            read a raw pwmd XML document\n"
          "    --keyfile, -k    file containing the passphrase to use for decryption\n"
          "    --help\n"
          "    --version\n\n"
          "Use - as <outfile> to write to standard output.\n",
          pn);
  exit (rc);
}

static gpg_error_t
init_client_crypto (struct crypto_s **crypto)
{
  struct crypto_s *new = xcalloc (1, sizeof (struct crypto_s));
  gpg_error_t rc;

  if (!new)
    {
      rc = GPG_ERR_ENOMEM;
      return rc;
    }

#ifdef WITH_AGENT
  if (use_agent)
    {
      rc = agent_init (&new->agent);
      if (!rc)
	{
	  rc = send_agent_common_options (new->agent);
	  if (!rc)
	    rc = agent_set_pinentry_options (new->agent);
	}

      if (rc)
	{
	  cleanup_agent (new->agent);
	  xfree (new);
	  return rc;
	}
    }
#endif

  set_header_defaults (&new->hdr);
  *crypto = new;
  return 0;
}

static gpg_error_t
parse_doc (const char *xml, size_t len, xmlDocPtr *result)
{
  xmlDocPtr doc;

  xmlResetLastError ();
  doc = xmlReadMemory (xml, len, NULL, "UTF-8", XML_PARSE_NOBLANKS);
  if (!doc && xmlGetLastError ())
    return GPG_ERR_BAD_DATA;

  *result = doc;
  return !doc ? GPG_ERR_ENOMEM : 0;
}

static xmlChar *
xml_attribute_value (xmlNodePtr n, xmlChar * attr)
{
  xmlAttrPtr a = xmlHasProp (n, attr);

  if (!a)
    return NULL;

  if (!a->children || !a->children->content)
    return NULL;

  return xmlGetProp (n, attr);
}

static int
xml_reserved_attribute (const char *name)
{
  int i;

  for (i = 0; reserved_attributes[i]; i++)
    {
      if (!strcmp (name, reserved_attributes[i]))
        return 1;
    }

  return 0;
}
static void
remove_non_reserved_attributes (xmlNodePtr n)
{
  xmlAttrPtr a;

  for (a = n->properties; a; a = a->next)
    {
      if (xml_reserved_attribute ((char *)a->name))
        continue;

      xmlRemoveProp (a);
    }
}

static gpg_error_t
strip_literals (const char *filename, xmlNodePtr n, int force)
{
  gpg_error_t rc = 0;

  for (; n; n = n->next)
    {
      xmlChar *target;

      if (n->type != XML_ELEMENT_NODE)
        continue;

      if (!xmlStrEqual (n->name, (xmlChar *) "element"))
        {
          xmlNodePtr tmp = n->next;

          xmlUnlinkNode (n);
          xmlFreeNodeList (n);
          return strip_literals (filename, tmp, force);
        }

      target = xml_attribute_value (n, (xmlChar *)"_target");
      if (target)
        {
          xmlChar lastc = 0, *p;

          remove_non_reserved_attributes (n);
again:
          for (lastc = 0, p = target; *p;)
            {
              if (*p == '!' && (p == target || lastc == '\t'))
                {
                  xmlChar *c;

                  c = p;
                  while (*p)
                    *c++ = *++p;
                  *c = 0;
                  goto again;
                }

              lastc = *p++;
            }

          if (!xmlSetProp (n, (xmlChar *) "_target", target))
            {
              xmlFree (target);
              return GPG_ERR_INV_VALUE;
            }

          xmlFree (target);

          if (n->children && !force)
            {
              fprintf(stderr, _("%s: aborting do to literal child elements (use --force)\n"), filename);
              return GPG_ERR_CANCELED;
            }
          else if (n->children)
            {
              xmlNodePtr tmp = n->children;

              xmlUnlinkNode (n->children);
              xmlFreeNodeList (tmp);
            }
        }

      rc = strip_literals (filename, n->children, force);
      if (rc)
        break;
    }

  return rc;
}

static gpg_error_t
recurse_rename_attribute (xmlNodePtr root, const char *old, const char *name)
{
  xmlNodePtr n;
  gpg_error_t rc = 0;

  for (n = root; n; n = n->next)
    {
      xmlAttrPtr attr = xmlHasProp (n, (xmlChar *)old);

      if (attr)
        {
          xmlChar *a = xml_attribute_value (n, (xmlChar *)old);

          if (a && !xmlSetProp (n, (xmlChar *) name, a))
            rc = GPG_ERR_BAD_DATA;

          xmlFree (a);

          if (!rc)
            {
              if (xmlRemoveProp (attr) == -1)
                rc = GPG_ERR_BAD_DATA;
            }
        }

      if (rc)
        break;

      rc = recurse_rename_attribute (n->children, old, name);
      if (rc)
        break;
    }

  return rc;
}

static gpg_error_t
update_to_version (xmlDocPtr doc)
{
  xmlNodePtr root = xmlDocGetRootElement (doc);
  xmlChar *v = xml_attribute_value (root,  (xmlChar *)"_version");
  gpg_error_t rc;

  // Pwmd 3.2.0 or later. No updates needed ATM.
  if (v)
    {
      xmlFree (v);
      return 0;
    }

  rc = recurse_rename_attribute (root->children, "target", "_target");
  if (!rc)
    rc = recurse_rename_attribute (root->children, "expire", "_expire");

  if (!rc)
    rc = recurse_rename_attribute (root->children, "expire_increment", "_age");

  return rc;
}

int
main(int argc, char **argv)
{
  gpg_error_t rc;
  int opt, optindex;
  struct crypto_s *crypto = NULL;
  char *outfile = NULL, *infile = NULL, *keyfile = NULL;
  void *key = NULL;
  enum {
      OPT_FORCE,
      OPT_XML
  };
  const char *optstring = "vhk:n";
  int convert = 1;
  int force = 0;
  int xml = 0;
  const struct option longopts[] = {
    {"force", no_argument, 0, 0},
    {"xml", no_argument, 0, 0},
    {"no-convert", no_argument, 0, 'n'},
    {"keyfile", required_argument, 0, 'k'},
    {"version", no_argument, 0, 'v'},
    {"help", no_argument, 0, 'h'},
    {0, 0, 0, 0}
  };

#ifndef DEBUG
#ifdef HAVE_SETRLIMIT
  struct rlimit rl;

  rl.rlim_cur = rl.rlim_max = 0;

  if (setrlimit (RLIMIT_CORE, &rl) != 0)
    err (EXIT_FAILURE, "setrlimit()");
#endif

#ifdef HAVE_PR_SET_DUMPABLE
  prctl (PR_SET_DUMPABLE, 0);
#endif
#endif

  if (!gpgrt_check_version (REQUIRE_LIBGPGERROR_VERSION))
    {
      fprintf (stderr, _("gpgrt_check_version(): Incompatible libgpg-error. "
                         "Wanted %s, got %s.\n"), REQUIRE_LIBGPGERROR_VERSION,
               gpgrt_check_version (NULL));
      exit (EXIT_FAILURE);
    }

  gpgrt_init ();
  //gpgrt_set_alloc_func (xrealloc_gpgrt);

  if (!gcry_check_version (REQUIRE_LIBGCRYPT_VERSION))
    {
      fprintf (stderr, _("gcry_check_version(): Incompatible libgcrypt. "
                         "Wanted %s, got %s.\n"), REQUIRE_LIBGCRYPT_VERSION,
               gcry_check_version (NULL));
      exit (EXIT_FAILURE);
    }

  gcry_set_allocation_handler (xmalloc, xmalloc, NULL, xrealloc, xfree);
  xmlMemSetup (xfree, xmalloc, xrealloc, str_dup);
  xmlInitMemory ();
  xmlInitGlobals ();
  xmlInitParser ();
  xmlXPathInit ();

  while ((opt = getopt_long (argc, argv, optstring, longopts, &optindex)) != -1)
    {
      switch (opt)
	{
        case 'n':
          convert = 0;
          break;
        case 'k':
          keyfile = optarg;
          break;
	case 'h':
          usage (argv[0], EXIT_SUCCESS);
	  break;
	case 'v':
          printf (_("%s\n\n"
                    "Copyright (C) 2015-2025\n"
                    "%s\n"
                    "Released under the terms of the GPL v2. Use at your own risk.\n\n"),
                    PACKAGE_STRING PWMD_GIT_HASH, PACKAGE_BUGREPORT);
          exit (EXIT_SUCCESS);
        case 0:
          switch (optindex)
            {
            case OPT_FORCE:
              force = 1;
              break;
            case OPT_XML:
              xml = 1;
              break;
            default:
	      usage (argv[0], EXIT_FAILURE);
            }
          break;
	default:
	  usage (argv[0], EXIT_FAILURE);
	  break;
        }
    }

  if (argc - optind != 2)
    usage (argv[0], EXIT_FAILURE);

  infile = argv[optind++];
  outfile = argv[optind];
  if (strcmp (outfile, "-"))
    {
      if (access (outfile, R_OK|W_OK) == 0)
        {
          fprintf(stderr, _("Please remove the existing output file '%s'.\n"),
                  outfile);
          exit (EXIT_FAILURE);
        }
    }

  rc = init_client_crypto (&crypto);
  if (rc)
    goto fail;

  if (!xml)
    {
      rc = decrypt_common (crypto, infile, keyfile);
      xfree (key);
    }
  else
    {
      struct stat st;
      int fd;

      fd = open (infile, O_RDONLY);
      if (fd == -1)
        {
          rc = gpg_error_from_syserror ();
          goto fail;
        }

      if (fstat (fd, &st) == -1)
        {
          rc = gpg_error_from_syserror ();
          close (fd);
          goto fail;
        }

      crypto->plaintext = gcry_calloc (1, sizeof (char) * st.st_size+1);
      if (!crypto->plaintext)
        {
          close (fd);
          rc = GPG_ERR_ENOMEM;
          goto fail;
        }

      crypto->plaintext_len = read (fd, crypto->plaintext, st.st_size);
      close (fd);
      if (crypto->plaintext_len != st.st_size)
        {
          rc = GPG_ERR_UNKNOWN_ERRNO;
          goto fail;
        }
    }

  if (!rc)
    {
      xmlDocPtr doc;

      rc = parse_doc ((char *) crypto->plaintext, crypto->plaintext_len, &doc);
      cleanup_crypto (&crypto);
      if (!rc)
        {
          xmlChar *buf;
          int len;
          xmlNodePtr n;

          FILE *fp = outfile[0] == '-' && outfile[1] == 0 ? stdout
            : fopen (outfile, "w");

          if (!fp)
            {
              rc = gpg_error_from_syserror ();
              xmlFreeDoc (doc);
              goto fail;
            }

          if (convert)
            {
              n = xmlDocGetRootElement (doc);
              rc = strip_literals (infile, n->children, force);
              if (rc)
                {
                  xmlFreeDoc (doc);
                  goto fail;
                }

              rc = update_to_version (doc);
              if (rc)
                {
                  xmlFreeDoc (doc);
                  goto fail;
                }

              n = xmlDocGetRootElement (doc);
              if (!xmlSetProp (n, (xmlChar *) "_version",
                               (xmlChar *)PACKAGE_VERSION))
                rc = GPG_ERR_BAD_DATA;
              if (rc)
                {
                  xmlFreeDoc (doc);
                  goto fail;
                }
            }

          xmlDocDumpMemory (doc, &buf, &len);
          xmlFreeDoc (doc);
          fprintf (fp, "%s", buf);
          xmlFree (buf);
          fclose (fp);
        }
      else
        goto fail;
    }
  else
    goto fail;

  cleanup_crypto (&crypto);
  exit (EXIT_SUCCESS);

fail:
  cleanup_crypto (&crypto);
  fprintf(stderr, "ERR %u: %s\n", rc, gpg_strerror (rc));
  exit (EXIT_FAILURE);
}

static gpg_error_t
hash_key (int algo, unsigned char *salt, size_t salt_len, const void *key,
	  size_t keylen, void **result, size_t *rlen, uint64_t iterations)
{
  gpg_error_t rc;
  void *tmp;

  /* Be sure the algorithm is available. */
  rc = gcry_cipher_algo_info (algo, GCRYCTL_GET_KEYLEN, NULL, rlen);
  if (rc)
    return rc;

  /* Always allocate enough for a 256-bit key although the algorithms
     themselves may use less. Fixes SAVE --cipher with a different
     keylen than the previously saved cipher when cached. */
  *rlen = 32;
  tmp = xmalloc (*rlen);
  if (!tmp)
    return GPG_ERR_ENOMEM;

  if (!iterations)
    iterations = DEFAULT_KDFS2K_ITERATIONS;

  rc = gcry_kdf_derive(key, keylen, GCRY_KDF_ITERSALTED_S2K, GCRY_MD_SHA1,
		       salt, salt_len, iterations, *rlen, tmp);
  if (!rc)
    *result = tmp;
  else
    xfree (tmp);

  return rc;
}

/*
 * Useful for a large amount of data. Rather than doing all of the data in one
 * iteration do it in chunks.  This lets the command be cancelable rather than
 * waiting for it to complete.
 */
#define CRYPTO_BLOCKSIZE(c) (c * 1024)
static gpg_error_t
iterate_crypto_once (gcry_cipher_hd_t h, unsigned char *inbuf,
		     size_t insize, size_t blocksize)
{
  gpg_error_t rc = 0;
  off_t len = CRYPTO_BLOCKSIZE (blocksize);
  void *p = gcry_malloc (len);
  off_t total = 0;

  if (!p)
    return gpg_error (GPG_ERR_ENOMEM);

  if (insize < CRYPTO_BLOCKSIZE (blocksize))
    len = insize;

  pthread_cleanup_push (gcry_free, p);

  for (;;)
    {
      unsigned char *inbuf2 = inbuf + total;
      unsigned char *tmp;

      if (len + total > insize)
	len = blocksize;

      rc = gcry_cipher_decrypt (h, p, len, inbuf2, len);
      if (rc)
	break;

      tmp = inbuf + total;
      memmove (tmp, p, len);
      total += len;
      if (total >= insize)
	break;

#ifdef HAVE_PTHREAD_TESTCANCEL
      pthread_testcancel ();
#endif
    }

  pthread_cleanup_pop (1);
  if (rc && gpg_err_source (rc) == GPG_ERR_SOURCE_UNKNOWN)
    rc = gpg_error (rc);

  return rc;
}

static void
cleanup_cipher (void *arg)
{
  gcry_cipher_close ((gcry_cipher_hd_t) arg);
}

/* Decrypt the XML data. For PKI data files the key is retrieved from
 * gpg-agent and the signature verified. */
static gpg_error_t
decrypt_data (struct crypto_s *crypto, unsigned char *salted_key,
              size_t skeylen)
{
  gpg_error_t rc = 0;
  unsigned char *key = salted_key;
  gcry_cipher_hd_t h = NULL;
  size_t blocksize, keysize = 0;
  int algo = cipher_to_gcrypt (crypto->hdr.flags);
  void *outbuf = NULL;
  uint64_t n;
#ifdef WITH_AGENT
  size_t keylen = skeylen;
  gcry_sexp_t sig_sexp;

  if (crypto->hdr.flags & PWMD_FLAG_PKI)
    {
      rc = agent_extract_key (crypto, &key, &keylen);
      if (rc)
	return rc;

      sig_sexp = gcry_sexp_find_token (crypto->ciphertext_sexp, "sig-val", 0);
      if (!sig_sexp)
	{
	  gcry_free (key);
	  return GPG_ERR_BAD_DATA;
	}

      rc = agent_verify (crypto->sigpkey_sexp, sig_sexp, crypto->ciphertext,
		   crypto->ciphertext_len);
      gcry_sexp_release (sig_sexp);
    }
#endif

  (void)skeylen;
  if (!rc)
    {
      rc = gcry_cipher_open (&h, algo, GCRY_CIPHER_MODE_CBC, 0);
      if (!rc)
	{
	  rc = gcry_cipher_algo_info (algo, GCRYCTL_GET_KEYLEN, NULL,
				      &keysize);
	  if (!rc)
	    {
	      rc = gcry_cipher_algo_info (algo, GCRYCTL_GET_BLKLEN, NULL,
					  &blocksize);
	      if (!rc)
		{
		  rc = gcry_cipher_setiv (h, crypto->hdr.iv, blocksize);
		  if (!rc)
		    {
		      rc = gcry_cipher_setkey (h, key, keysize);
		    }
		}
	    }
	}
    }

  pthread_cleanup_push (cleanup_cipher, rc ? NULL : h);

  if (!rc)
    {
      outbuf = gcry_malloc (crypto->hdr.datalen+1);
      if (!outbuf)
	rc = GPG_ERR_ENOMEM;
      else
        memset (outbuf, 0, crypto->hdr.datalen+1);
    }

  pthread_cleanup_push (gcry_free, outbuf);
#ifdef WITH_AGENT
  pthread_cleanup_push (gcry_free, key);
#endif

  if (!rc)
    {
      if (!key)
        rc = GPG_ERR_INV_PARAMETER;
      else
        {
          memcpy (outbuf, crypto->ciphertext, crypto->hdr.datalen);
          rc = iterate_crypto_once (h, outbuf, crypto->hdr.datalen, blocksize);
        }

      if (!rc && crypto->hdr.version <= 0x03000e)
	{
	  key[0] ^= 1;
	  rc = gcry_cipher_setkey (h, key, keysize);
	}
    }

  if (!rc && crypto->hdr.version <= 0x03000e)
    {
     for (n = 0; !rc && n < crypto->hdr.s2k_count; n++)
       {
         rc = gcry_cipher_setiv (h, crypto->hdr.iv, blocksize);
         if (rc)
           break;

         rc = iterate_crypto_once (h, outbuf, crypto->hdr.datalen, blocksize);
       }
    }

#ifdef WITH_AGENT
  pthread_cleanup_pop (0);
  if (crypto->hdr.flags & PWMD_FLAG_PKI)
    gcry_free (key);
  else if (key && crypto->hdr.version <= 0x03000e)
    key[0] ^= 1;
#else
  if (crypto->hdr.version <= 0x03000e && key)
    key[0] ^= 1;
#endif
  pthread_cleanup_pop (rc ? 1 : 0);	/* outbuf */
  pthread_cleanup_pop (1);              /* cipher */
  if (!rc)
    {
      char buf[] = "<?xml ";

      if (memcmp (outbuf, buf, sizeof(buf)-1))
	{
	  gcry_free (outbuf);
	  return gpg_error (GPG_ERR_BAD_PASSPHRASE);
	}

      crypto->plaintext = outbuf;
      crypto->plaintext_len = crypto->hdr.datalen;
    }

  if (rc && gpg_err_source (rc) == GPG_ERR_SOURCE_UNKNOWN)
    rc = gpg_error (rc);

  return rc;
}

static gpg_error_t
get_passphrase (const char *filename, const char *keyfile, void **r_key,
                size_t *r_len)
{
  char buf[255] = { 0 };
  struct termios told, tnew;

  *r_len = 0;

  if (keyfile)
    {
      ssize_t len;
      struct stat st;
      int fd;

      fd = open (keyfile, O_RDONLY);
      if (fd == -1)
        return gpg_error_from_syserror ();

      if (fstat (fd, &st) == -1)
        {
          gpg_error_t rc = gpg_error_from_syserror ();

          close (fd);
          return rc;
        }

      len = read (fd, buf, sizeof (buf));
      if (len != st.st_size)
        {
          close (fd);
          wipememory (buf, 0, sizeof (buf));
          return gpg_error_from_syserror ();
        }

      *r_len = st.st_size ? st.st_size : 1;
      *r_key = xmalloc (*r_len);
      if (*r_key)
        memcpy (*r_key, buf, *r_len);
      wipememory (buf, 0, sizeof (buf));
      close (fd);
      return *r_key ? 0 : GPG_ERR_ENOMEM;
    }

  if (!isatty (STDIN_FILENO))
    return GPG_ERR_ENOTTY;

  if (tcgetattr (STDIN_FILENO, &told) == -1)
    return gpg_error_from_syserror ();

  memcpy (&tnew, &told, sizeof (struct termios));
  tnew.c_lflag &= ~(ECHO);
  tnew.c_lflag |= ICANON | ECHONL;
  if (tcsetattr (STDIN_FILENO, TCSANOW, &tnew) == -1)
    {
      int n = errno;

      tcsetattr (STDIN_FILENO, TCSANOW, &told);
      return gpg_error_from_errno (n);
    }

  fprintf(stderr, "Passphrase for '%s': ", filename);
  if (!fgets (buf, sizeof (buf), stdin))
    {
      tcsetattr (STDIN_FILENO, TCSANOW, &told);
      return GPG_ERR_EOF;
    }

  tcsetattr (STDIN_FILENO, TCSANOW, &told);

  if (buf[strlen(buf)-1] == '\n')
    buf[strlen(buf)-1] = 0;
  *r_len = buf[0] == 0 ? 1 : strlen (buf);
  *r_key = str_dup (buf);
  wipememory (buf, 0, sizeof (buf));
  return 0;
}

/* Common to both PKI and non-PKI files. */
static gpg_error_t
decrypt_common (struct crypto_s *crypto, const char *filename,
                const char *keyfile)
{
  void *key = NULL;
  size_t keylen = 0;
  gpg_error_t rc = read_data_file (filename, crypto);
  int algo = cipher_to_gcrypt (crypto->hdr.flags);
  void *skey = NULL;
  size_t skeysize = 0;

  if (rc)
    return rc;

  rc = get_passphrase (filename, keyfile, &key, &keylen);
  if (rc)
    return rc;

  if (key)// && !IS_PKI (crypto))
    {
      rc = hash_key (algo, crypto->hdr.salt, sizeof(crypto->hdr.salt), key,
		     keylen, &skey, &skeysize,
                     crypto->hdr.version <= 0x03000e ? COMPAT_KDFS2K_ITERATIONS : crypto->hdr.s2k_count);
      if (rc)
	{
	  xfree (key);
	  return rc;
	}
    }

  xfree (key);
  xfree (crypto->filename);
  crypto->filename = str_dup (filename);
  rc = decrypt_data (crypto, skey, skeysize);
  xfree (skey);
  return rc;
}

static gpg_error_t
read_header (file_header_t *hdr, int fd)
{
  ssize_t len;
  uint32_t i;
  uint64_t n;

#ifdef WORDS_BIGENDIAN
  len = read (fd, &hdr->magic, sizeof(hdr->magic));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->version, sizeof(hdr->version));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->s2k_count, sizeof(hdr->s2k_count));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->flags, sizeof(hdr->flags));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->iv, sizeof(hdr->iv));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->salt, sizeof(hdr->salt));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->datalen, sizeof(hdr->datalen));
  if (len == -1)
    goto done;
#else
  len = read (fd, &hdr->magic, sizeof(hdr->magic));
  if (len == -1)
    goto done;

  len = read (fd, &i, sizeof(hdr->version));
  if (len == -1)
    goto done;
  hdr->version = ntohl (i);

  len = read (fd, &n, sizeof(hdr->s2k_count));
  if (len == -1)
    goto done;
  hdr->s2k_count = (((uint64_t) ntohl (n)) << 32) + ntohl (n >> 32);

  len = read (fd, &n, sizeof(hdr->flags));
  if (len == -1)
    goto done;
  hdr->flags = (((uint64_t) ntohl (n)) << 32) + ntohl (n >> 32);

  len = read (fd, &hdr->iv, sizeof(hdr->iv));
  if (len == -1)
    goto done;

  len = read (fd, &hdr->salt, sizeof(hdr->salt));
  if (len == -1)
    goto done;

  len = read (fd, &i, sizeof(hdr->datalen));
  if (len == -1)
    goto done;
  hdr->datalen = ntohl (i);
#endif

 done:
  return len == -1 ? gpg_error_from_errno (errno) : 0;
}

/* Read the header of a data file to determine cipher and other. The
 * header is stored big endian in the file and is converted to little
 * endian when needed. */
static gpg_error_t
read_data_header (const char *filename, file_header_t * rhdr,
		  struct stat *rst, int *rfd)
{
  gpg_error_t rc = 0;
  struct stat st;
  file_header_t hdr;
  int fd;

  if (rfd)
    *rfd = -1;

  fd = open (filename, O_RDONLY);
  if (fd == -1)
    return gpg_error_from_syserror ();

  if (fstat (fd, &st) == -1)
    {
      rc = gpg_error_from_syserror ();
      close (fd);
      return rc;
    }

  rc = read_header (&hdr, fd);
  if (!rc && memcmp (hdr.magic, crypto_magic, sizeof (hdr.magic)))
    rc = GPG_ERR_BAD_DATA;
  else if (!rc && hdr.version < 0x030000)
    rc = GPG_ERR_UNKNOWN_VERSION;

  if (rc)
    close (fd);
  else
    {
      if (rhdr)
	*rhdr = hdr;
      if (rst)
	*rst = st;
      if (rfd)
	*rfd = fd;
      else
	close (fd);
    }

  return gpg_error (rc);
}

static gpg_error_t
read_data_file (const char *filename, struct crypto_s * crypto)
{
  int fd;
  gpg_error_t rc = 0;
  size_t len, rlen;
  char *buf = NULL;
  struct stat st;

  if (!filename)
    return GPG_ERR_INV_PARAMETER;

  cleanup_crypto_stage1 (crypto);
  rc = read_data_header (filename, &crypto->hdr, &st, &fd);
  if (rc)
    return gpg_error (rc);

  crypto->ciphertext_len = crypto->hdr.datalen;
  crypto->ciphertext = xmalloc (crypto->hdr.datalen);
  if (!crypto->ciphertext)
    {
      rc = GPG_ERR_ENOMEM;
      goto done;
    }

  /* The keygrips for PKI files are stored after the header. They are
   * stored in the file to let file(1) magic(5) show the grips. */
  if (crypto->hdr.flags & PWMD_FLAG_PKI)
    {
      rlen = read (fd, crypto->grip, 20);
      if (rlen != 20)
	{
	  rc = rlen == -1 ? gpg_error_from_errno (errno) : GPG_ERR_BAD_DATA;
	  goto done;
	}

      rlen = read (fd, crypto->sign_grip, 20);
      if (rlen != 20)
	{
	  rc = rlen == -1 ? gpg_error_from_errno (errno) : GPG_ERR_BAD_DATA;
	  goto done;
	}
    }

  len = read (fd, crypto->ciphertext, crypto->hdr.datalen);
  if (len != crypto->hdr.datalen)
    {
      rc = len == -1 ? gpg_error_from_errno (errno) : GPG_ERR_BAD_DATA;
      goto done;
    }

  if (!(crypto->hdr.flags & PWMD_FLAG_PKI))
    goto done;

  if (!use_agent)
    {
      rc = GPG_ERR_NOT_IMPLEMENTED;
      goto done;
    }

#ifdef WITH_AGENT
  len = st.st_size - sizeof (file_header_t) - crypto->hdr.datalen - 40;
  buf = xmalloc (len);
  if (!buf)
    {
      rc = GPG_ERR_ENOMEM;
      goto done;
    }

  /* Remaining data file bytes are the encrypted key and XML. */
  rlen = read (fd, buf, len);
  if (rlen != len)
    {
      rc = rlen == -1 ? gpg_error_from_errno (errno) : GPG_ERR_BAD_DATA;
      goto done;
    }

  rc = gcry_sexp_new (&crypto->ciphertext_sexp, buf, rlen, 1);
  if (rc)
    goto done;

  if (crypto->pkey_sexp)
    gcry_sexp_release (crypto->pkey_sexp);

  if (crypto->sigpkey_sexp)
    gcry_sexp_release (crypto->sigpkey_sexp);

  crypto->pkey_sexp = crypto->sigpkey_sexp = NULL;
  rc = get_pubkey_bin (crypto, crypto->grip, &crypto->pkey_sexp);
  if (!rc)
    rc = get_pubkey_bin (crypto, crypto->sign_grip, &crypto->sigpkey_sexp);
#endif

done:
  close (fd);
  xfree (buf);
  if (rc && gpg_err_source (rc) == GPG_ERR_SOURCE_UNKNOWN)
    rc = gpg_error (rc);

  return rc;
}

static void
cleanup_save (struct save_s *save)
{
  if (!save)
    return;

#ifdef WITH_AGENT
  if (save->pkey)
    gcry_sexp_release (save->pkey);

  if (save->sigpkey)
    gcry_sexp_release (save->sigpkey);
#endif

  memset (save, 0, sizeof (struct save_s));
}

/* Keep the agent ctx to retain pinentry options which will be freed in
 * cleanup_cb(). Also keep .pubkey since it may be needed for a SAVE. */
static void
cleanup_crypto_stage1 (struct crypto_s *cr)
{
  if (!cr)
    return;

  cleanup_save (&cr->save);

#ifdef WITH_AGENT
  if (cr->ciphertext_sexp)
    gcry_sexp_release (cr->ciphertext_sexp);

  cr->ciphertext_sexp = NULL;
#endif

  gcry_free (cr->plaintext);
  xfree (cr->ciphertext);
  xfree (cr->filename);
  cr->filename = NULL;
  cr->ciphertext = NULL;
  cr->ciphertext_len = 0;
  cr->plaintext = NULL;
  cr->plaintext_len = 0;
}

static void
cleanup_crypto_stage2 (struct crypto_s *cr)
{
  if (!cr)
    return;

  cleanup_crypto_stage1 (cr);
  set_header_defaults (&cr->hdr);
}

static void
cleanup_crypto (struct crypto_s **c)
{
  struct crypto_s *cr = *c;

  if (!cr)
    return;

  cleanup_crypto_stage2 (cr);

#ifdef WITH_AGENT
  if (cr->pkey_sexp)
    gcry_sexp_release (cr->pkey_sexp);

  if (cr->sigpkey_sexp)
    gcry_sexp_release (cr->sigpkey_sexp);

  if (cr->agent)
    cleanup_agent (cr->agent);
#endif

  xfree (cr);
  *c = NULL;
}

/* Sets the default cipher values for new files. */
static void
set_header_defaults (file_header_t * hdr)
{
  const char *s = "aes256";
  int flags = cipher_string_to_cipher (s);

  memset (hdr, 0, sizeof (file_header_t));
  memcpy (hdr->magic, crypto_magic, sizeof (hdr->magic));
  if (flags == -1)
    fprintf(stderr, _("Invalid 'cipher' in configuration file. Using a default of aes256."));

  hdr->flags = flags == -1 ? PWMD_CIPHER_AES256 : flags;
  hdr->version = VERSION_HEX;

#ifdef WITH_AGENT
  if (use_agent)
    hdr->flags |= PWMD_FLAG_PKI;
#endif
}
