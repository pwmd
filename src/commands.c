/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <err.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#include <ctype.h>
#include <dirent.h>
#include <pthread.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#include <assert.h>
#include <signal.h>

#include "pwmd-error.h"
#include <gcrypt.h>

#include "mem.h"
#include "xml.h"
#include "util-misc.h"
#include "common.h"
#include "rcfile.h"
#include "cache.h"
#include "commands.h"
#include "mutex.h"
#include "crypto.h"
#include "acl.h"
#include "command-opt.h"

#define FLOCK_TYPE_NONE   0
#define FLOCK_TYPE_SH     0x0001
#define FLOCK_TYPE_EX     0x0002
#define FLOCK_TYPE_KEEP   0x0004

static char env_display[256];
static char env_gpg_tty[256];
static char env_term[256];
static struct command_table_s **command_table;

static gpg_error_t do_lock (struct client_s *client, int add);
static gpg_error_t validate_checksum (struct client_s *, const char *filename,
				      struct cache_data_s *, unsigned char **,
                                      size_t *, int);
static gpg_error_t update_checksum (struct client_s *client,
                                    unsigned char *from_crc, size_t crclen);
static gpg_error_t command_startup (assuan_context_t ctx, const char *name);
static void command_finalize (assuan_context_t ctx, gpg_error_t rc);

/* When 'status' is true the 'self' field of the status line will be false
 * because we never send the STATE status message to the same client that
 * initiated it. */
static char *
build_client_info_line (struct client_thread_s *thd, int status)
{
  char *uid, *username = NULL;
  char *line;

#ifdef WITH_GNUTLS
  if (thd->remote)
    uid = str_asprintf("#%s", thd->tls->fp);
  else
    {
      uid = str_asprintf("%u", thd->peer->uid);
      username = get_username (thd->peer->uid);
    }
#else
  uid = str_asprintf("%u", thd->peer->uid);
  username = get_username (thd->peer->uid);
#endif
  line = str_asprintf ("%p %s %s %s %u %u %u %s %s %u",
                       thd->tid,
                       thd->name ? thd->name : "-",
                       thd->cl && thd->cl->filename
                       && (thd->cl->flags & FLAG_OPEN)
                       ? thd->cl->filename : "/",
#ifdef WITH_GNUTLS
                       thd->remote ? thd->peeraddr : "-",
#else
                       "-",
#endif
                       thd->cl && thd->cl->flags & FLAG_HAS_LOCK ? 1 : 0,
                       !status &&  pthread_equal (pthread_self (), thd->tid) ? 1 : 0,
                       thd->state, uid,
#ifdef WITH_GNUTLS
                       thd->remote ? "-" : username,
#else
                       username,
#endif
                       thd->conntime
  );

  xfree (username);
  xfree (uid);
  return line;
}

void
update_client_state (struct client_s *client, unsigned s)
{
  MUTEX_LOCK (&cn_mutex);
  if (client->thd->state == s)
    {
      MUTEX_UNLOCK (&cn_mutex);
      return;
    }
  client->thd->state = s;
  MUTEX_UNLOCK (&cn_mutex);

  if (client->thd->state != CLIENT_STATE_UNKNOWN)
    {
      char *line = build_client_info_line (client->thd, 1);

      pthread_cleanup_push (xfree, line);
      if (line)
        send_status_all_not_self (STATUS_STATE, "%s", line);
      pthread_cleanup_pop (1);
    }
}

static gpg_error_t
unlock_file_mutex (struct client_s *client, int remove)
{
  gpg_error_t rc = 0;

  // OPEN: keep the lock for the same file being reopened.
  if (client->flags & FLAG_KEEP_LOCK && client->flags & FLAG_HAS_LOCK)
    return 0;

  if (!remove && !(client->flags & FLAG_HAS_LOCK))
    return GPG_ERR_NOT_LOCKED;

  rc = cache_unlock_mutex (client->filename, remove);
  if (rc)
    rc = GPG_ERR_INV_STATE;
  else
    client->flags &= ~(FLAG_HAS_LOCK | FLAG_LOCK_CMD);

  return rc;
}

static gpg_error_t
lock_file_mutex (struct client_s *client, int add)
{
  gpg_error_t rc = 0;
  long timeout = config_get_long (client->filename, "cache_timeout");

  if (client->flags & FLAG_HAS_LOCK)
    return 0;

  rc = cache_lock_mutex (client->ctx, client->filename,
			 client->lock_timeout, add, timeout);
  if (!rc)
    client->flags |= FLAG_HAS_LOCK;

  return rc;
}

static gpg_error_t
file_modified (struct client_s *client, struct command_table_s *cmd)
{
  gpg_error_t rc = 0;
  int type = !cmd->flock_type || (cmd->flock_type & FLOCK_TYPE_SH)
    ? LOCK_SH : LOCK_EX;

  if (!(client->flags & FLAG_OPEN))
    return GPG_ERR_INV_STATE;

  rc = lock_file_mutex (client, 0);
  if (rc && rc != GPG_ERR_NO_DATA)
    return rc;

  rc = lock_flock (client->ctx, client->filename, type, &client->flock_fd);
  if (!rc)
    {
      rc = validate_checksum (client, client->filename, NULL, NULL, NULL, 0);
      if (gpg_err_code (rc) == GPG_ERR_ENOENT && (client->flags & FLAG_NEW))
        rc = 0;
      else if (rc)
        log_write ("%s: %s", client->filename, pwmd_strerror (rc));
    }
  else if (gpg_err_code (rc) == GPG_ERR_ENOENT)
    rc = 0;

  /* FLAG_HAS_LOCK may have been set by the LOCK command or OPEN --lock. */
  if ((cmd->unlock && !(client->flags & FLAG_HAS_LOCK)) || rc)
    unlock_file_mutex (client, 0);

  if (rc || !(cmd->flock_type & FLOCK_TYPE_KEEP))
    unlock_flock (&client->flock_fd);

  return rc;
}

static gpg_error_t
parse_xml (assuan_context_t ctx, int new)
{
  struct client_s *client = assuan_get_pointer (ctx);
  int cached = client->doc != NULL;
  gpg_error_t rc = 0;

  if (new)
    {
      client->doc = xml_new_document ();
      if (client->doc)
	{
	  xmlChar *result;
	  int len;

	  xmlDocDumpFormatMemory (client->doc, &result, &len, 0);
	  client->crypto->plaintext = result;
	  client->crypto->plaintext_size = len;
	  if (!client->crypto->plaintext)
	    {
	      xmlFreeDoc (client->doc);
	      client->doc = NULL;
              rc = GPG_ERR_ENOMEM;
	    }
	}
      else
        rc = GPG_ERR_ENOMEM;
    }
  else if (!cached)
    rc = xml_parse_doc ((char *) client->crypto->plaintext,
                        client->crypto->plaintext_size,
                        (xmlDocPtr *)&client->doc);

  return rc;
}

static void
free_client (struct client_s *client)
{
  cache_plaintext_release (&client->doc);
  xfree (client->crc);
  xfree (client->filename);
  xfree (client->last_error);
  crypto_free (client->crypto);
  client->crypto = NULL;
}

void
reset_client (struct client_s *client)
{
  assuan_context_t ctx = client->ctx;
  struct client_thread_s *thd = client->thd;
  long lock_timeout = client->lock_timeout;
  xmlErrorPtr xml_error = client->xml_error;
  int no_pinentry = (client->flags & FLAG_NO_PINENTRY);
  int flock_fd = client->flock_fd;
  struct bulk_cmd_s *bulk_p = client->bulk_p;

  cache_lock ();
  unlock_file_mutex (client, client->flags & FLAG_NEW);
  free_client (client);
  memset (client, 0, sizeof (struct client_s));
  client->flock_fd = flock_fd;
  client->xml_error = xml_error;
  client->ctx = ctx;
  client->thd = thd;
  client->lock_timeout = lock_timeout;
  client->flags |= no_pinentry ? FLAG_NO_PINENTRY : 0;
  client->bulk_p = bulk_p;
  cache_unlock ();
}

static void
req_free (void *arg)
{
  if (!arg)
    return;

  strv_free ((char **) arg);
}

static gpg_error_t
parse_open_opt_lock (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_LOCK_ON_OPEN;
  return 0;
}

static gpg_error_t
parse_opt_inquire (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_INQUIRE;
  return 0;
}

static gpg_error_t
update_checksum (struct client_s *client, unsigned char *from_crc,
                 size_t crclen)
{
  unsigned char *crc;
  size_t len;
  struct cache_data_s *cdata;
  gpg_error_t rc;

  if (!from_crc)
    {
      rc = get_checksum (client->filename, &crc, &len);
      if (rc)
        return rc;
    }
  else
    {
      crc = from_crc;
      len = crclen;
    }

  xfree (client->crc);
  client->crc = xmalloc (len);
  memcpy (client->crc, crc, len);
  pthread_cleanup_push (xfree, crc);
  cdata = cache_get_data (client->filename, NULL);
  pthread_cleanup_pop (0);
  if (cdata)
    {
      xfree (cdata->crc);
      cdata->crc = xmalloc (len);
      memcpy (cdata->crc, crc, len);
    }

  if (!from_crc)
    xfree (crc);
  return 0;
}

static gpg_error_t
validate_checksum (struct client_s *client, const char *filename,
                   struct cache_data_s *cdata, unsigned char **r_crc,
                   size_t *r_crclen, int from_cdata)
{
  unsigned char *crc;
  size_t len;
  gpg_error_t rc;
  int n = 0;

  if (cdata && !cdata->crc)
    return GPG_ERR_CHECKSUM;

  rc = get_checksum (filename, &crc, &len);
  if (rc)
    return rc;

  if (client->crc)
    n = memcmp (client->crc, crc, len);
  else if ((client->flags & FLAG_NEW) && cache_get_data (filename, NULL))
    n = 1;

  if ((!n && cdata) || (from_cdata && cdata && crc))
    n = memcmp (cdata->crc, crc, len);

  if (!n && r_crc)
    {
      *r_crc = crc;
      *r_crclen = len;
    }
  else
    xfree (crc);

  return n ? GPG_ERR_CHECKSUM : 0;
}

static gpg_error_t
open_command (assuan_context_t ctx, char *line)
{
  gpg_error_t rc;
  struct client_s *client = assuan_get_pointer (ctx);
  int same_file = 0;
  assuan_peercred_t peer;
  struct cache_data_s *cdata = NULL;
  int cached = 0;
  unsigned char *crc = NULL;
  size_t crclen = 0;
  int plaintext = 0;
  struct argv_s *args[] = {
    &(struct argv_s) {"lock", OPTION_TYPE_NOARG, parse_open_opt_lock},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);

  rc = do_validate_peer (ctx, line, &peer, NULL);
  if (rc == GPG_ERR_FORBIDDEN)
    {
      rc = peer_is_invoker (client);
      if (rc == GPG_ERR_EACCES)
        rc = GPG_ERR_FORBIDDEN;
    }

  if (rc)
    return send_error (ctx, rc);

  if (!valid_filename (line))
    return send_error (ctx, GPG_ERR_INV_VALUE);

  /* This client may have locked a different file with ISCACHED --lock than
   * the current filename. This will remove that lock. */
  same_file = client->filename && !strcmp (line, client->filename);
  if (client->flags & FLAG_OPEN ||
      (client->flags & FLAG_HAS_LOCK && !same_file))
    {
      unsigned opts = client->opts;
      unsigned flags = client->flags;

      if (same_file)
	client->flags |= FLAG_KEEP_LOCK;

      /* Another client wrote to the same data file as currently opened. */
      cdata = cache_get_data (client->filename, NULL);
      if (cdata && cdata->crc)
        flags &= ~FLAG_NEW;
      /* Remove cache entry for the new file that hadn't been written. */
      else if (client->flags & FLAG_NEW)
        cache_clear (NULL, client->filename, 0, 0);

      cdata = NULL;
      reset_client (client);
      client->opts = opts;
      client->flags |= flags;
      client->flags &= ~(FLAG_LOCK_CMD);
      if (!same_file)
	client->flags &= ~(FLAG_HAS_LOCK | FLAG_NEW);
    }

  xfree (client->filename);
  client->filename = str_dup (line);
  if (!client->filename)
    return send_error(ctx, GPG_ERR_ENOMEM);

  /* Need to lock the mutex here because file_modified() cannot without
   * knowing the filename. */
  rc = lock_file_mutex (client, 1);
  if (rc)
    client->flags &= ~FLAG_OPEN;

  if (!rc)
    {
      rc = lock_flock (ctx, client->filename, LOCK_SH, &client->flock_fd);
      if (gpg_err_code (rc) == GPG_ERR_ENOENT)
        rc = 0;
    }

  if (!rc)
    {
      char *keyfile = config_get_string (client->filename, "passphrase_file");

      rc = crypto_init (&client->crypto, client->ctx, client->filename,
                        client->flags & FLAG_NO_PINENTRY, keyfile);
      if (rc)
        xfree (keyfile);
      else
        rc = open_check_file (client->filename, NULL, NULL, 1);
    }

  if (!rc || gpg_err_code (rc) == GPG_ERR_ENOENT)
    {
      int reload = 0;
      int defer = 0;

      if (rc) // new file
        {
          rc = 0;

          if (config_get_boolean ("global", "strict_open"))
            {
              rc = peer_is_invoker (client);
              if (rc == GPG_ERR_EACCES)
                rc = GPG_ERR_FORBIDDEN;
            }

          if (!rc)
            {
              client->flags |= FLAG_NEW;
              // data file disappeared. clear the cache entry.
              cache_clear (NULL, client->filename, 1, 1);
              cdata = NULL;
            }

          goto done;
        }

      cdata = cache_get_data (client->filename, &defer);
      if (cdata && !defer
          && !cache_plaintext_get (client->filename, &client->doc))
        {
          rc = validate_checksum (client, client->filename, cdata, &crc,
                                  &crclen, 0);
          if (rc)
            {
              log_write ("%s: %s", client->filename,
                         pwmd_strerror (rc));
              cache_plaintext_release (&client->doc);
              if (rc == GPG_ERR_CHECKSUM)
                {
                  cache_clear (NULL, client->filename, 1, 1);
                  cdata = NULL;
                  goto decrypt;
                }
            }

#ifdef WITH_GNUTLS
          if (!rc && client->thd->remote
              && config_get_boolean (client->filename, "tcp_require_key")
              && !(client->flags & FLAG_NEW))
            {
              rc = crypto_try_decrypt (client,
                                       (client->flags & FLAG_NO_PINENTRY));
            }
#endif
          if (!rc)
            {
              strv_free (client->crypto->pubkey);
              client->crypto->pubkey = NULL;
              if (cdata->pubkey)
                client->crypto->pubkey = strv_dup (cdata->pubkey);

              xfree (client->crypto->sigkey);
              client->crypto->sigkey = NULL;
              if (cdata->sigkey)
                client->crypto->sigkey = str_dup (cdata->sigkey);

              cached = 1;
              plaintext = 1;
            }
        }
      else if (cdata && cdata->doc) // cached document
        {
          if (!rc && !(client->flags & FLAG_NEW))
            {
              rc = validate_checksum (client, client->filename, cdata, &crc,
                                      &crclen, 0);
              if (rc == GPG_ERR_CHECKSUM)
                {
                  rc = 0;
                  reload = 1;
                }
            }

          if (!rc)
            {
             rc = cache_iscached (client->filename, &defer, 0, 0);
             if ((!rc || rc == GPG_ERR_ENOENT) && defer)
               {
                 rc = 0;
                 reload = 2;
               }
            }

          if (!rc && reload)
            {
              if (reload == 1)
                log_write ("%s: %s", client->filename,
                           pwmd_strerror (GPG_ERR_CHECKSUM));
              cache_clear (NULL, client->filename, 1, 1);
              cdata = NULL;
              rc = crypto_decrypt (client, client->crypto);
            }
#ifdef WITH_GNUTLS
          else if (!rc)
            {
              if (client->thd->remote
                  && config_get_boolean (client->filename, "tcp_require_key")
                  && !(client->flags & FLAG_NEW))
                rc = crypto_try_decrypt (client,
                                         (client->flags & FLAG_NO_PINENTRY));
            }
#endif

          if (!rc && cdata)
            {
              client->crypto->plaintext = cdata->doc;
              client->crypto->plaintext_size = cdata->size;
              rc = cache_decrypt (client->crypto);
              if (!rc)
                {
                  cdata->doc = NULL;
                  cdata->size = 0;

                  strv_free (client->crypto->pubkey);
                  client->crypto->pubkey = NULL;
                  if (cdata->pubkey)
                    client->crypto->pubkey = strv_dup (cdata->pubkey);

                  xfree (client->crypto->sigkey);
                  client->crypto->sigkey = NULL;
                  if (cdata->sigkey)
                    client->crypto->sigkey = str_dup (cdata->sigkey);

                  cached = 1;
                }
              else
                {
                  client->crypto->plaintext = NULL;
                  client->crypto->plaintext_size = 0;
                }
            }
        }
      else // existing file
        {
decrypt:
          cached = cdata != NULL;
          rc = crypto_decrypt (client, client->crypto);
        }
    }

done:
  if (!rc && !plaintext)
    {
      rc = parse_xml (ctx, client->flags & FLAG_NEW);
      if (!rc)
        {
          rc = cache_encrypt (client->crypto);
          if (rc)
            cache_clear (NULL, client->filename, 1, 1);
          else
            {
              long timeout = config_get_long (client->filename,
                                              "cache_timeout");

              cache_free_data_once (cdata);
              cdata = xcalloc (1, sizeof (struct cache_data_s));
              cdata->doc = client->crypto->plaintext;
              cdata->size = client->crypto->plaintext_size;
              cdata->pubkey = strv_dup(client->crypto->pubkey);
              cdata->sigkey = client->crypto->sigkey ? str_dup(client->crypto->sigkey) : NULL;
              client->crypto->plaintext = NULL;
              client->crypto->plaintext_size = 0;

              if (cached) // wont increment the refcount
                {
                  if (crclen)
                    {
                      xfree (client->crc);
                      client->crc = NULL;
                      xfree (cdata->crc);
                      cdata->crc = xmalloc (crclen);
                      memcpy (cdata->crc, crc, crclen);
                    }

                  rc = cache_set_data (client->filename, cdata);
                  /* The cache entry may have been removed and cache_set_data()
                   * already sent STATUS_CACHE. */
                  if (!cache_iscached (client->filename, NULL, 0, 0))
                    rc = send_status (ctx, STATUS_CACHE, NULL);
                }
              else
                rc = cache_add_file (client->filename, cdata, timeout, same_file);

              if (!rc)
                cache_plaintext_set (client->filename, client->doc, 0);
            }
        }
    }

  if (!rc)
    {
      xfree (client->crc);
      client->crc = NULL;
      if (crc)
        {
          client->crc = xmalloc (crclen);
          memcpy (client->crc, crc, crclen);
        }
    }

  xfree (crc);

  if (!rc && !(client->flags & FLAG_NEW) && !cached)
    rc = update_checksum (client, NULL, 0);

  if (!rc)
    {
      client->flags |= FLAG_OPEN;

      if (client->flags & FLAG_NEW)
        rc = send_status (ctx, STATUS_NEWFILE, NULL);

      if (!rc && (client->opts & OPT_LOCK_ON_OPEN))
        rc = do_lock (client, 0);

      crypto_free_non_keys (client->crypto);
    }
  else
    {
      log_write ("%s: %s", client->filename, pwmd_strerror (rc));

      if (client->flags & FLAG_NEW)
        cache_clear (NULL, client->filename, 1, 1);

      crypto_free (client->crypto);
      client->crypto = NULL;
      client->flags &= ~FLAG_OPEN;
      cache_plaintext_release (&client->doc);
    }

  return send_error (ctx, rc);
}

/* If not an invoking_user or is an existing file, check that the list of user
 * supplied key ID's are in the list of current key ID's obtained when
 * decrypting the data file.
 */
static gpg_error_t
permitted_to_save (struct client_s *client, const char **keys,
                   const char **value)
{
  gpg_error_t rc = 0;
  const char **v;

  if ((client->flags & FLAG_NEW) || (client->opts & OPT_SYMMETRIC))
    return 0;

  rc = peer_is_invoker (client);
  if (!rc)
    return 0;
  else if (rc == GPG_ERR_EACCES)
    rc = GPG_ERR_FORBIDDEN;
  else if (rc)
    return rc;

  /* Empty match. */
  if ((!value && !keys) || ((value && !*value) && (keys && !*keys)))
    return 0;

  for (v = value; v && *v; v++)
    {
      const char **k, *pv = *v;
      int match = 0;

      if (*pv == '0' && *(pv+1) == 'x')
        pv += 2;

      for (k = keys; k && *k; k++)
        {
          const char *pk = *k;

          if (*pk == '0' && *(pk+1) == 'x')
            pk += 2;

          rc = !strcmp (pv, pk) ? 0 : GPG_ERR_FORBIDDEN;
          if (rc)
            rc = !strcmp (pv, *k) ? 0 : GPG_ERR_FORBIDDEN;

          if (!rc)
            {
              match = 1;
              break;
            }
        }

      if (!match)
        return GPG_ERR_FORBIDDEN;
    }

  return rc;
}

/* Requires that the keyid be a fingerprint in 16 byte form. */
static gpg_error_t
parse_save_opt_keyid_common (struct client_s *client, const char **list,
                             const char *value, char ***dst)
{
  gpg_error_t rc = 0;
  char **keys = NULL;

  if (value && *value)
    {
      keys = str_split (value, ",", 0);
      if (!keys)
        return GPG_ERR_ENOMEM;
    }

  rc = crypto_keyid_to_16b (keys);
  if (!rc)
    rc = permitted_to_save (client, list, (const char **)keys);

  if (!rc)
    *dst = keys;
  else
    strv_free (keys);

  return rc;
}

static gpg_error_t
parse_save_opt_keyid (void *data, void *value)
{
  struct client_s *client = data;
  const char *str = value;
  char **dst = NULL;
  gpg_error_t rc;

  rc = parse_save_opt_keyid_common (client,
                                    (const char **)client->crypto->pubkey,
                                    str, &dst);
  if (rc)
    return rc;

  client->crypto->save.pubkey = dst;
  return 0;
}

static gpg_error_t
parse_save_opt_sign_keyid (void *data, void *value)
{
  struct client_s *client = data;
  const char *str = value;
  char **dst = NULL;
  gpg_error_t rc;
  char **tmp = NULL;

  if (client->crypto->sigkey)
    {
      if (!strv_printf (&tmp, "%s", client->crypto->sigkey))
        return GPG_ERR_ENOMEM;
    }

  rc = parse_save_opt_keyid_common (client, (const char **)tmp, str, &dst);
  strv_free (tmp);
  if (rc)
    return rc;

  if (!dst)
    client->opts |= OPT_NO_SIGNER;
  else
    client->crypto->save.sigkey = str_dup (*dst);

  strv_free (dst);
  return 0;
}

static gpg_error_t
parse_save_opt_inquire_keyid (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;

  if (!(client->flags & FLAG_NEW))
    {
      gpg_error_t rc = peer_is_invoker (client);

      if (rc)
        return rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc;
    }

  client->opts |= OPT_INQUIRE_KEYID;
  return 0;
}

static gpg_error_t
parse_save_opt_symmetric (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_SYMMETRIC;
  return 0;
}

static gpg_error_t
parse_genkey_opt_userid (void *data, void *value)
{
  struct client_s *client = data;

  if (!(client->flags & FLAG_NEW))
    {
      gpg_error_t rc = peer_is_invoker (client);

      if (rc)
        return rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc;
    }

  client->crypto->save.userid = str_dup (value);
  return client->crypto->save.userid ? 0 : GPG_ERR_ENOMEM;
}

static gpg_error_t
parse_genkey_opt_algo (void *data, void *value)
{
  struct client_s *client = data;

  client->crypto->save.algo = str_dup (value);
  return client->crypto->save.algo ? 0 : GPG_ERR_ENOMEM;
}

static gpg_error_t
parse_genkey_opt_no_expire (void *data, void *value)
{
  struct client_s *client = data;

  client->crypto->save.flags |= GPGME_CREATE_NOEXPIRE;
  return 0;
}

static gpg_error_t
parse_genkey_opt_expire (void *data, void *value)
{
  struct client_s *client = data;
  gpg_error_t rc = 0;
  char *p = NULL;
  unsigned long t;

  errno = 0;
  t = strtoul (value, &p, 10);

  if (!errno && p && *p)
    rc = GPG_ERR_INV_VALUE;
  else if (t == ULONG_MAX)
    rc = GPG_ERR_INV_VALUE;
  else if (errno)
    rc = gpg_error_from_syserror ();
  else
    client->crypto->save.expire = t;

  return rc;
}

static gpg_error_t
parse_genkey_opt_no_passphrase (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->crypto->save.flags |= GPGME_CREATE_NOPASSWD;
  return 0;
}

/* Tests that the keys in new_keys are also in old_keys. */
static gpg_error_t
compare_keys (char **new_keys, char **old_keys)
{
  char **o;

  if (!old_keys || !*old_keys)
    return 0;

  crypto_keyid_to_16b (new_keys);

  for (o = old_keys; *o; o++)
    {
      char **n;

      for (n = new_keys; *n; n++)
        {
          if (!strcmp (*n, *o))
            break;
        }

      if (!*n)
        return GPG_ERR_NOT_FOUND;
    }

  return 0;
}

static gpg_error_t
inquire_keyid (struct client_s *client)
{
  gpg_error_t rc;
  unsigned char *result = NULL;
  size_t len;
  const char *s = "KEYID";
  char ***orig;
  char ***save;

  orig = &client->crypto->pubkey;
  save = &client->crypto->save.pubkey;
  rc = assuan_inquire (client->ctx, s, &result, &len, 0);
  if (!rc)
    {
      char **dst = NULL;

      rc = parse_save_opt_keyid_common (client, (const char **)*orig,
                                        (char *)result, &dst);
      if (!rc)
        *save = dst;
    }

  xfree (result);
  return rc;
}


/* When a key lookup in gpg-agent's cache fails, the agent tries the last
 * successful key to be unlocked.  This prevents pwmd from successfully
 * clearing a signing key since the previous key, which more than likely
 * belongs to the same keypair as the encryption/decryption key, will have the
 * passphrase cached and therefore the signing key also cached. So we need to
 * clear both the signing and encryption keys to get the effect of requiring a
 * passphrase when generating a new keypair for an existing data file, or when
 * the "require_save_key" configuration parameter is set. This parameter is
 * mostly a failsafe of the gpg-agent option --ignore-cache-for-signing since
 * some/most/all users may fail to set it.
 */
static gpg_error_t
save_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  struct cache_data_s *cdata = NULL;
  int sym = 0;
  gpg_error_t rc = 0, cache_rc = 0;
  int defer = 0;
  struct argv_s *args[] = {
    &(struct argv_s) {"keyid", OPTION_TYPE_ARG, parse_save_opt_keyid},
    &(struct argv_s) {"inquire-keyid", OPTION_TYPE_NOARG,
        parse_save_opt_inquire_keyid },
    &(struct argv_s) {"sign-keyid", OPTION_TYPE_OPTARG,
        parse_save_opt_sign_keyid},
    &(struct argv_s) {"symmetric", OPTION_TYPE_NOARG,
        parse_save_opt_symmetric },
    NULL
  };

  crypto_free_save (&client->crypto->save);
  client->crypto->save.expire = DEFAULT_EXPIRE;

  rc = crypto_is_symmetric (client->filename);
  if (!rc || rc == GPG_ERR_BAD_DATA || rc == GPG_ERR_ENOENT)
    {
      if (!rc)
        {
          client->opts |= OPT_SYMMETRIC;
          sym = 1;
        }
      else if (rc == GPG_ERR_ENOENT && (client->flags & FLAG_NEW))
        rc = 0; // New file
      else
        rc = 0; // PKI
    }

  if (rc)
    return send_error (ctx, rc);

  rc = parse_options (&line, args, client, 0);
  if (rc)
    return send_error (ctx, rc);

  if (config_get_boolean (client->filename, "require_save_key")
      || (client->opts & OPT_SYMMETRIC))
    client->opts |= OPT_ASK;

  rc = open_check_file (client->filename, NULL, NULL, 1);
  if (rc && gpg_err_code (rc) != GPG_ERR_ENOENT)
    {
      log_write ("%s: %s", client->filename, pwmd_strerror (rc));
      return send_error (ctx, rc);
    }

  if (!rc)
    cache_rc = rc = cache_iscached (client->filename, &defer, 0, 1);

  if (gpg_err_code (rc) == GPG_ERR_ENOENT && (client->flags & FLAG_NEW))
    rc = 0;
  else if (gpg_err_code (rc) == GPG_ERR_NO_DATA)
    rc = 0;

  if (rc)
    return send_error (ctx, rc);

  /* Specifying both a recipient and symmetric encryption is an error. */
  if ((client->opts & OPT_INQUIRE_KEYID) && (client->opts & OPT_SYMMETRIC))
    return send_error (ctx, GPG_ERR_CONFLICT);
  /* Existing file with a recipient and wanting symmetric is an error. */
  else if ((client->opts & OPT_SYMMETRIC) && client->crypto->save.pubkey)
    return send_error (ctx, GPG_ERR_CONFLICT);
  else if (client->crypto->save.pubkey && (client->opts & OPT_INQUIRE_KEYID))
    return send_error (ctx, GPG_ERR_CONFLICT);
  else if (!sym && (client->opts & OPT_SYMMETRIC) && !(client->flags & FLAG_NEW))
    return send_error (ctx, GPG_ERR_CONFLICT);
  /* New file that is not symmetric without either a recipient or signer. */
  else if ((client->flags & FLAG_NEW) && !(client->opts & OPT_SYMMETRIC)
           && (!client->crypto->save.pubkey || !client->crypto->save.sigkey))
    return send_error (ctx, GPG_ERR_SYNTAX);
  else if (client->opts & OPT_INQUIRE_KEYID)
    rc = inquire_keyid (client);

  if (!rc)
    {
      client->crypto->keyfile = config_get_string (client->filename,
                                                   "passphrase_file");
      rc = crypto_init_ctx (client->crypto, (client->flags & FLAG_NO_PINENTRY),
                            client->crypto->keyfile);
    }

  if (!rc && (client->flags & FLAG_NEW))
    {
      /* Require --keyid when --sign-keyid exists to keep KEYINFO
       * synchronized. */
      if (!client->crypto->save.pubkey && client->crypto->save.sigkey
          && !(client->opts & OPT_SYMMETRIC))
        rc = GPG_ERR_NO_PUBKEY;
    }
  else if (!rc)
    {
      cdata = cache_get_data (client->filename, NULL);
      if (cdata)
        {
          if (client->crypto->save.pubkey)
            rc = compare_keys (client->crypto->save.pubkey, cdata->pubkey);

          /* Always allow a signer for symmetric data files. */
          if (!rc && client->crypto->save.sigkey
              && !(client->opts & OPT_SYMMETRIC))
            rc = !strcmp (client->crypto->save.sigkey, cdata->sigkey)
              ? 0 : GPG_ERR_NOT_FOUND;

          /* Prevent saving to a recipient who is not in the original recipient
           * list without a passphrase. */
          if (rc || (client->crypto->sigkey && (client->opts & OPT_NO_SIGNER)))
            client->opts |= OPT_ASK;

          if (rc == GPG_ERR_NOT_FOUND)
            {
              rc = peer_is_invoker (client);
              if (rc == GPG_ERR_EACCES)
                rc = GPG_ERR_FORBIDDEN;
            }

          if (!client->crypto->save.pubkey
              && !(client->opts & OPT_SYMMETRIC))
            client->crypto->save.pubkey = strv_dup (cdata->pubkey);

          if (!rc && !client->crypto->save.sigkey && cdata->sigkey
              && !(client->opts & OPT_NO_SIGNER))
            client->crypto->save.sigkey = str_dup (cdata->sigkey);
          else if (!rc && !client->crypto->save.sigkey
              && (client->opts & OPT_NO_SIGNER)
              && !(client->opts & OPT_SYMMETRIC))
            rc = GPG_ERR_NO_SIGNATURE_SCHEME;
        }
      else
        {
          client->crypto->save.pubkey = strv_dup (client->crypto->pubkey);
          client->crypto->save.sigkey = str_dup (client->crypto->sigkey);
        }
    }

  if (!rc && !(client->flags & FLAG_NEW))
    {
      /* Fixes {NEW_,SIGN_}PASSPHRASE inquire keywords. See passphrase_cb(). */
      if (client->opts & OPT_SYMMETRIC)
        client->crypto->flags |= CRYPTO_FLAG_PASSWD;

      if (client->opts & OPT_ASK)
        {
          rc = cache_clear_agent_keys (client->filename, 1, 1);
          if (!rc)
            rc = crypto_try_decrypt (client, client->flags & FLAG_NO_PINENTRY);
        }
    }

  if (!rc && client->opts & OPT_SYMMETRIC)
    client->crypto->flags |= CRYPTO_FLAG_PASSWD_NEW;

  if (!rc)
    rc = xml_update_element_mtime (client, xmlDocGetRootElement (client->doc));

  if (!rc)
    {
      xmlNodePtr root = xmlDocGetRootElement (client->doc);

      rc = xml_add_attribute (client, root, "_version", PACKAGE_VERSION);
    }

  if (!rc)
    {
      int size;

      xmlDocDumpFormatMemory (client->doc, &client->crypto->plaintext, &size,
                              0);
      if (size > 0)
        client->crypto->plaintext_size = (size_t) size;
      else
        rc = GPG_ERR_ENOMEM;

      if (!rc)
        {
          client->crypto->flags |= (client->opts & OPT_SYMMETRIC) ? CRYPTO_FLAG_SYMMETRIC : 0;
          client->crypto->flags |= (client->flags & FLAG_NEW) ? CRYPTO_FLAG_NEWFILE : 0;
          client->crypto->flags |= !(client->opts & OPT_SYMMETRIC) ? CRYPTO_FLAG_PASSWD_SIGN : 0;
          rc = crypto_encrypt (client, client->crypto);
          client->crypto->flags &= ~CRYPTO_FLAG_SYMMETRIC;
        }

      if (!rc)
        {
          unsigned char *crc = NULL;
          size_t crclen = 0;

          rc = crypto_write_file (client->crypto, &crc, &crclen);
          pthread_cleanup_push ((void *)xfree, crc);
          if (!rc)
            {
              if (!cache_rc)
                cdata = cache_get_data (client->filename, NULL);
              else
                cdata = xcalloc (1, sizeof (struct cache_data_s));
            }

          if (!rc)
            {
              rc = cache_encrypt (client->crypto);
              if (rc)
                {
                  cache_clear (NULL, client->filename, 1, 1);
                  client->flags &= ~(FLAG_NEW);

                  strv_free (client->crypto->pubkey);
                  client->crypto->pubkey = NULL;
                  if (client->crypto->save.pubkey)
                    client->crypto->pubkey = strv_dup (client->crypto->save.pubkey);

                  xfree (client->crypto->sigkey);
                  client->crypto->sigkey = NULL;
                  if (client->crypto->save.sigkey)
                    client->crypto->sigkey = str_dup (client->crypto->save.sigkey);
                }
            }

          if (!rc)
            {
              long timeout = config_get_long (client->filename, "cache_timeout");
              xmlDocPtr doc = xmlCopyDoc  (client->doc, 1);

              if (!doc)
                rc = GPG_ERR_ENOMEM;

              if (!rc)
                {
                  cache_plaintext_release (&client->doc);
                  strv_free (cdata->pubkey);
                  cdata->pubkey = client->crypto->save.pubkey;
                  client->crypto->save.pubkey = NULL;

                  xfree (cdata->sigkey);
                  cdata->sigkey = client->crypto->save.sigkey;
                  client->crypto->save.sigkey = NULL;

                  xfree (cdata->crc);
                  cdata->crc = NULL;

                  /* cache_encrypt() does in-place encryption */
                  xfree (cdata->doc);
                  cdata->doc = client->crypto->plaintext;
                  client->crypto->plaintext = NULL;
                  cdata->size = client->crypto->plaintext_size;
                  client->crypto->plaintext_size = 0;

                  client->doc = doc;
                  cache_plaintext_set (client->filename, client->doc, 0);

                  /* Update in case the cache entry expires the next SAVE may
                   * not have any known keys. */
                  strv_free (client->crypto->pubkey);
                  client->crypto->pubkey = NULL;
                  if (cdata->pubkey)
                    client->crypto->pubkey = strv_dup (cdata->pubkey);

                  xfree (client->crypto->sigkey);
                  client->crypto->sigkey = NULL;
                  if (cdata->sigkey)
                    client->crypto->sigkey = str_dup (cdata->sigkey);

                  if (!cache_rc) // wont increment refcount
                    rc = cache_set_data (client->filename, cdata);
                  else
                    rc = cache_add_file (client->filename, cdata, timeout, 0);
                }

              if (!rc && (cache_rc || (client->flags & FLAG_NEW)))
                send_status_all (STATUS_CACHE, NULL);

              if (!rc)
                {
                  rc = update_checksum (client, crc, crclen);
                  client->flags &= ~(FLAG_NEW);
                }
            }

          pthread_cleanup_pop (1);
          if (!rc)
            {
              client->did_cow = 0;
              send_status_modified (client->ctx);
            }
        }
    }

  crypto_free_non_keys (client->crypto);
  return send_error (ctx, rc);
}

static gpg_error_t
parse_genkey_opt_usage (void *data, void *v)
{
  struct client_s *client = data;
  char *value = v;

  if (!value || !*value)
    return GPG_ERR_INV_VALUE;
  else if (!strcmp (value, "sign"))
    client->crypto->save.flags |= GPGME_CREATE_SIGN;
  else if (!strcmp (value, "encrypt"))
    client->crypto->save.flags |= GPGME_CREATE_ENCR;
  else if (!strcmp (value, "default"))
    client->crypto->save.flags &= ~(GPGME_CREATE_ENCR|GPGME_CREATE_SIGN);
  else
    return GPG_ERR_INV_VALUE;

  return 0;
}

gpg_error_t
parse_genkey_opt_subkey_of (void *data, void *v)
{
  gpg_error_t rc;
  struct client_s *client = data;
  char *value = v;
  char *tmp[] = { value, NULL };
  gpgme_key_t *keys = NULL;

  rc = peer_is_invoker (client);
  if (rc)
    return rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc;

  if (!value || !*value)
    return GPG_ERR_INV_VALUE;

  rc = crypto_list_keys (client->crypto, tmp, 1, &keys);
  if (rc)
    return rc;

  if (!keys || !*keys)
    {
      crypto_free_key_list (keys);
      return GPG_ERR_NO_SECKEY;
    }

  client->crypto->save.mainkey = keys;
  return 0;
}

static gpg_error_t
genkey_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  struct crypto_s *crypto, *orig;
  gpg_error_t rc = 0;
  struct argv_s *args[] = {
    &(struct argv_s) {"userid", OPTION_TYPE_ARG,
        parse_genkey_opt_userid},
    &(struct argv_s) {"expire", OPTION_TYPE_ARG,
        parse_genkey_opt_expire},
    &(struct argv_s) {"no-expire", OPTION_TYPE_NOARG,
        parse_genkey_opt_no_expire},
    &(struct argv_s) {"algo", OPTION_TYPE_ARG,
        parse_genkey_opt_algo},
    &(struct argv_s) {"no-passphrase", OPTION_TYPE_NOARG,
        parse_genkey_opt_no_passphrase},
    &(struct argv_s) {"usage", OPTION_TYPE_ARG,
        parse_genkey_opt_usage},
    &(struct argv_s) {"subkey-of", OPTION_TYPE_ARG,
        parse_genkey_opt_subkey_of},
    NULL
  };

  if (!(client->flags & FLAG_OPEN))
    return send_error (ctx, GPG_ERR_INV_STATE);

  rc = crypto_init (&crypto, ctx, client->filename,
                    client->flags & FLAG_NO_PINENTRY, NULL);
  if (rc)
    return send_error (ctx, rc);

  pthread_cleanup_push ((void *)crypto_free, client->crypto);
  orig = client->crypto;
  client->crypto = crypto;
  rc = parse_options (&line, args, client, 0);
  if (!rc)
    {
      if (!client->crypto->save.userid && !client->crypto->save.mainkey)
	rc = GPG_ERR_SYNTAX;
      else
        {
	  client->crypto->flags |= CRYPTO_FLAG_NEWFILE;
	  rc = crypto_genkey (client, client->crypto);
	}
    }

  pthread_cleanup_pop (0);
  crypto_free (crypto);
  client->crypto = orig;
  return send_error (ctx, rc);
}

static gpg_error_t
do_delete (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  struct xml_request_s *req;
  xmlNodePtr n;
  gpg_error_t rc;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  rc = xml_new_request (client, line, XML_CMD_DELETE, &req);
  if (rc)
    return rc;

  n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  xml_free_request (req);
  if (rc)
    return rc;

  rc = xml_is_element_owner (client, n, 0);
  if (!rc)
    rc = xml_unlink_node (client, n);

  return rc;
}

/* Won't update cdata->plaintext. Other clients are working on the original or
 * copy of the same document. The first client to SAVE wins and requires others
 * to reopen the data file due to a checksum error. */
static gpg_error_t
copy_on_write (struct client_s *client)
{
  struct cache_data_s *cdata;

  if (client->did_cow)
    return 0;

  cdata = cache_get_data (client->filename, NULL);
  if (cdata)
    {
      gpg_error_t rc;
      xmlDocPtr doc = xmlCopyDoc (client->doc, 1);

      if (!doc)
        return GPG_ERR_ENOMEM;

      rc = cache_plaintext_set (client->filename, doc, 1);
      if (rc)
        {
          xmlFree (doc);
          return rc;
        }

      (void)cache_plaintext_release (&client->doc);
      client->doc = doc;
      client->did_cow = 1;
      return 0;
    }

  return GPG_ERR_NO_DATA;
}

static gpg_error_t
delete_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  rc = copy_on_write (client);
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_delete (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_delete (ctx, line);

  return send_error (ctx, rc);
}

static gpg_error_t
update_element_expirey (struct client_s *client, xmlNodePtr n)
{
  gpg_error_t rc = 0;
  xmlChar *expire, *incr;
  char *p;
  time_t e, e_orig, i;
  time_t now = time (NULL);

  expire = xml_attribute_value (n, (xmlChar *)"_expire");
  if (!expire)
    return 0;

  errno = 0;
  e = strtoul ((char *)expire, &p, 10);
  if (errno || *p || e == ULONG_MAX || *expire == '-')
    {
      xmlFree (expire);
      rc = GPG_ERR_ERANGE;
      goto fail;
    }

  xmlFree (expire);
  incr = xml_attribute_value (n, (xmlChar *)"_age");
  if (!incr)
    return 0;

  i = strtoul ((char *)incr, &p, 10);
  if (errno || *p || i == ULONG_MAX || *incr == '-')
    {
      xmlFree (incr);
      rc = GPG_ERR_ERANGE;
      goto fail;
    }

  xmlFree (incr);
  e_orig = e;
  e = now + i;
  p = str_asprintf ("%lu", e);
  if (!p)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  rc = xml_add_attribute (client, n, "_expire", p);
  xfree (p);
  if (!rc)
    rc = send_status (client->ctx, STATUS_EXPIRE, "%lu %lu", e_orig, e);

fail:
  return rc;
}

static gpg_error_t
parse_opt_store_no_acl_inherit (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_NO_INHERIT_ACL;
  return 0;
}

static gpg_error_t
store_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  size_t len;
  unsigned char *result;
  xmlNodePtr n, parent;
  int has_content;
  char *content = NULL;
  struct xml_request_s *req;
  struct argv_s *args[] = {
    &(struct argv_s) {"no-inherit-acl", OPTION_TYPE_NOARG,
        parse_opt_store_no_acl_inherit},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);

  if (!client->bulk_p && line && *line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = copy_on_write (client);
  if (rc)
    return send_error (ctx, rc);

  if (!client->bulk_p)
    {
      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
        return send_error (ctx, rc);
      rc = xml_new_request (client, (char *)result, XML_CMD_STORE, &req);
      if (!result || !*result)
        {
          xfree (result);
          return send_error (ctx, GPG_ERR_SYNTAX);
        }

      len = strv_length (req->args);
      has_content = result[strlen ((char *)result) - 1] != '\t' && len > 1;
      xfree (result);
    }
  else
    {
      rc = xml_new_request (client, line, XML_CMD_STORE, &req);
      len = strv_length (req->args);
      has_content = line && line[strlen (line) - 1] != '\t' && len > 1;
    }

  if (rc)
    return send_error (ctx, rc);

  /* Prevent passing the element content around to save some memory
   * (has_content). */
  if (*(req->args+1) && !xml_valid_element_path (req->args, has_content))
    {
      xml_free_request (req);
      return send_error (ctx, GPG_ERR_INV_VALUE);
    }

  if (has_content || !*req->args[len-1])
    {
      content = req->args[len-1];
      req->args[len-1] = NULL;
    }

  if (strv_length (req->args) > 1)
    {
      rc = xml_check_recursion (client, req);
      if (rc && rc != GPG_ERR_ELEMENT_NOT_FOUND)
        goto fail;
    }

  parent = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);

  if (!rc && len > 1)
    {
      rc = xml_is_element_owner (client, parent, 0);
      if (!rc)
        {
          n = xml_find_text_node (parent->children);
          if (n)
            xmlNodeSetContent (n, (xmlChar *) content);
          else
            xmlNodeAddContent (parent, (xmlChar *) content);

          (void)xml_update_element_mtime (client, parent);
          (void)update_element_expirey (client, parent);
        }
    }

fail:
  xfree (content);
  xml_free_request (req);
  return send_error (ctx, rc);
}

static gpg_error_t
xfer_data (assuan_context_t ctx, const char *line, int total)
{
  struct client_s *client = assuan_get_pointer (ctx);
  int to_send = total < ASSUAN_LINELENGTH ? total : ASSUAN_LINELENGTH;
  int sent = 0;
  gpg_error_t rc = 0;

  if (!(client->flags & FLAG_LOCK_CMD))
    rc = unlock_file_mutex (client, 0);
  if (rc && rc != GPG_ERR_NOT_LOCKED)
    return rc;

  if (client->bulk_p)
    {
      client->bulk_p->result = xmalloc (total+1);
      if (!client->bulk_p->result)
        return GPG_ERR_ENOMEM;
      memcpy (client->bulk_p->result, line, total);
      client->bulk_p->result[total] = 0;
      client->bulk_p->result_len = total;
      return 0;
    }

  rc = send_status (ctx, STATUS_XFER, "%li %li", sent, total);
  if (rc)
    return rc;

  do
    {
      if (sent + to_send > total)
	to_send = total - sent;

      rc = assuan_send_data (ctx, (char *) line + sent, to_send);
      if (!rc)
        sent += to_send;
    }
  while (!rc && sent < total);

  return rc;
}

static gpg_error_t
do_get (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct xml_request_s *req;
  xmlNodePtr n;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  rc = xml_new_request (client, line, XML_CMD_NONE, &req);
  if (rc)
    return rc;

  n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (!rc)
    {
      if (n && n->children)
        {
          xmlNodePtr tmp = n;

          n = xml_find_text_node (n->children);
          if (!n || !n->content || !*n->content)
            rc = GPG_ERR_NO_DATA;
          else
            {
              rc = xfer_data (ctx, (char *) n->content, xmlStrlen (n->content));
              if (!rc)
                {
                  xmlChar *expire = xml_attribute_value (tmp,
                                                         (xmlChar *)"_expire");
                  if (expire)
                    {
                      time_t now = time (NULL);
                      time_t e;
                      char *p;

                      errno = 0;
                      e = strtoul ((char *)expire, &p, 10);
                      if (errno || *p || e == ULONG_MAX || *expire == '-')
                        log_write (_("invalid expire attribute value: %s"),
                                   expire);
                      else if (now >= e)
                        {
                          pthread_cleanup_push (xmlFree, expire);
                          rc = send_status (ctx, STATUS_EXPIRE, "%lu 0", e);
                          pthread_cleanup_pop (0);
                        }

                      xmlFree (expire);
                    }
                }
            }
        }
      else
        rc = GPG_ERR_NO_DATA;
    }

  xml_free_request (req);
  return rc;
}

static gpg_error_t
get_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_get (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_get (ctx, line);

  return send_error (ctx, rc);
}

static void list_command_free1 (void *arg);
static gpg_error_t
realpath_command (assuan_context_t ctx, char *line)
{
  gpg_error_t rc;
  char **realpath = NULL;
  struct client_s *client = assuan_get_pointer (ctx);
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      (void)xml_resolve_path (client, client->doc, result, &realpath, &rc);
      pthread_cleanup_pop (1);
    }
  else
    (void)xml_resolve_path (client, client->doc, (unsigned char *)line,
                            &realpath, &rc);

  if (!rc)
    {
      char *tmp = strv_join ((char *)"\t", realpath);

      strv_free (realpath);
      if (!tmp)
        return send_error (ctx, GPG_ERR_ENOMEM);

      pthread_cleanup_push ((void *)xfree, tmp);
      rc = xfer_data (ctx, tmp, strlen (tmp));
      pthread_cleanup_pop (1);
    }

  return send_error (ctx, rc);
}

static void
list_command_free1 (void *arg)
{
  if (arg)
    string_free ((struct string_s *) arg, 1);
}

static gpg_error_t
parse_list_opt_recurse (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_LIST_RECURSE;
  return 0;
}

static gpg_error_t
parse_opt_verbose (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_VERBOSE;
  return 0;
}

static gpg_error_t
parse_opt_sexp (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_SEXP;
  return 0;
}

static gpg_error_t
list_path_once (struct client_s *client, char *line,
		struct element_list_s *elements, struct string_s *result)
{
  gpg_error_t rc;

  rc = xml_create_path_list (client, client->doc, NULL, elements, line, 0);
  if (rc == GPG_ERR_ELOOP || rc == GPG_ERR_EACCES)
    rc = 0;

  if (!rc)
    {
      int total = slist_length (elements->list);

      if (total)
	{
          int i;

          for (i = 0; i < total; i++)
            {
              char *tmp = slist_nth_data (elements->list, i);

              string_append_printf (result, "%s%s", tmp,
                                    i + 1 == total ? "" : "\n");
	    }
	}
      else
	rc = GPG_ERR_NO_DATA;
    }

  return rc;
}

#define RESUMABLE_ERROR(rc) (rc == GPG_ERR_ELOOP || rc == GPG_ERR_EACCES \
                             || rc == GPG_ERR_ELEMENT_NOT_FOUND || !rc)
static gpg_error_t
attribute_list_common (struct client_s *client, const char *path,
                       char ***result)
{
  char **attrlist = NULL;
  int i = 0;
  int target = 0;
  xmlAttrPtr a;
  xmlNodePtr n, an, r;
  gpg_error_t rc;
  struct xml_request_s *req;

  rc = xml_new_request (client, path, XML_CMD_ATTR_LIST, &req);
  if (rc)
    return rc;

  n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  xml_free_request (req);
  if (rc)
    return rc;

  gpg_error_t acl_rc = 0;
again:
  acl_rc = xml_acl_check (client, n);
  for (a = n->properties; a; a = a->next)
    {
      char **pa;
      int reserved = xml_reserved_attribute ((char *)a->name);

      if (acl_rc == GPG_ERR_EACCES && !reserved)
        continue;
      else if (acl_rc && acl_rc != GPG_ERR_EACCES)
        {
          rc = acl_rc;
          break;
        }

      if (reserved && target)
        continue;

      pa = xrealloc (attrlist, (i + 2) * sizeof (char *));
      if (!pa)
	{
	  log_write ("%s(%i): %s", __FILE__, __LINE__,
		     pwmd_strerror (GPG_ERR_ENOMEM));
	  rc = GPG_ERR_ENOMEM;
          break;
	}

      attrlist = pa;
      an = a->children;
      attrlist[i] = str_asprintf ("%s %s", (char *) a->name, an && an->content
                                  ? (char *)an->content : "");

      if (!attrlist[i])
	{
	  log_write ("%s(%i): %s", __FILE__, __LINE__,
		     pwmd_strerror (GPG_ERR_ENOMEM));
	  rc = GPG_ERR_ENOMEM;
          break;
	}

      attrlist[++i] = NULL;
    }

  if (!rc && !attrlist)
    return GPG_ERR_NO_DATA;

  if (!rc && !target)
    {
      r = xml_resolve_path (client, client->doc, (xmlChar *)path, NULL, &rc);
      if (RESUMABLE_ERROR (rc))
        {
          rc = 0;
          if (r && r != n)
            {
              target = 1;
              n = r;
              goto again;
            }
        }
    }

  if (!rc)
    *result = attrlist;
  else
    strv_free (attrlist);

  return rc;
}

static gpg_error_t
create_list_sexp (struct client_s *client, struct string_s *string,
                  struct string_s **r_string)
{
  struct string_s *str = string;
  gpg_error_t rc = 0;
  struct string_s *result = string_new ("(11:list-result");
  char **strv = str_split (str->str, "\n", 0);
  int t = strv_length (strv);
  int i;

  string_large (result);

  for (i = 0; i < t; i++)
    {
      char **tmpv = str_split (strv[i], " ", 0);
      char **attrlist = NULL;

      result = string_append_printf (result, "(4:path%i:%s", strlen (tmpv[0]),
                                     tmpv[0]);
      if (tmpv[1])
        result = string_append_printf (result, "5:flags%i:%s", strlen (tmpv[1]),
                                       tmpv[1]);
      else
        result = string_append (result, "5:flags0:");

      rc = attribute_list_common (client, tmpv[0], &attrlist);
      if (!rc)
        {
          int ai;
          int at = strv_length (attrlist);

          result = string_append (result, "(5:attrs");

          for (ai = 0; ai < at; ai++)
            {
              char **attrv = str_split (attrlist[ai], " ", 0);
              char *value = strv_join (" ", attrv+1);

              result = string_append_printf (result, "%i:%s%i:%s",
                                                   strlen (attrv[0]), attrv[0],
                                                   strlen (value), value);
              strv_free (attrv);
              xfree (value);
            }

          result = string_append (result, ")");
        }

      result = string_append (result, ")");
      strv_free (attrlist);
      strv_free (tmpv);
    }

  strv_free (strv);
  result = string_append (result, ")");
  if (!rc)
    *r_string = result;

  return rc;
}

static gpg_error_t
do_list (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = GPG_ERR_NO_DATA;
  struct element_list_s *elements = NULL;
  struct string_s *result = NULL;

  if (!line || !*line)
    {
      struct string_s *str = string_new (NULL);
      xmlNodePtr n;

      if (!str)
        return GPG_ERR_ENOMEM;

      string_large (str);
      pthread_cleanup_push ((void *)list_command_free1, str);
      n = xmlDocGetRootElement (client->doc);
      for (n = n->children; n; n = n->next)
        {
          xmlChar *name;

          if (n->type != XML_ELEMENT_NODE)
            continue;

          name = xmlGetProp (n, (xmlChar *)"_name");
          if (!name)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          elements = xcalloc (1, sizeof (struct element_list_s));
          if (!elements)
            {
              xmlFree (name);
              rc = GPG_ERR_ENOMEM;
              break;
            }

          elements->prefix = string_new (NULL);
          if (!elements->prefix)
            {
              xmlFree (name);
              xml_free_element_list (elements);
              rc = GPG_ERR_ENOMEM;
              break;
            }

          elements->recurse = client->opts & OPT_LIST_RECURSE;
          elements->root_only = !elements->recurse;
          pthread_cleanup_push ((void *)xml_free_element_list, elements);
          rc = list_path_once (client, (char *)name, elements, str);
          xmlFree (name);
          pthread_cleanup_pop (1);
          if (rc)
            break;

          if (n->next)
            string_append (str, "\n");
        }

      if (!rc)
        {
          if (client->opts & OPT_SEXP)
            {
              rc = create_list_sexp (client, str, &result);
              if (!rc)
                {
                  pthread_cleanup_push ((void *)list_command_free1, result);
                  rc = xfer_data (ctx, result->str, result->len);
                  pthread_cleanup_pop (1);
                }
            }
          else
            rc = xfer_data (ctx, str->str, str->len);
        }

      pthread_cleanup_pop (1);
      return rc;
    }

  elements = xcalloc (1, sizeof (struct element_list_s));
  if (!elements)
    return GPG_ERR_ENOMEM;

  elements->prefix = string_new (NULL);
  if (!elements->prefix)
    {
      xml_free_element_list (elements);
      return GPG_ERR_ENOMEM;
    }

  elements->recurse = client->opts & OPT_LIST_RECURSE;
  pthread_cleanup_push ((void *)xml_free_element_list, elements);
  struct string_s *str = string_new (NULL);
  string_large (str);
  pthread_cleanup_push ((void *)list_command_free1, str);
  rc = list_path_once (client, line, elements, str);
  if (!rc)
    {
      if (client->opts & OPT_SEXP)
        {
          rc = create_list_sexp (client, str, &result);
          if (!rc)
            {
              pthread_cleanup_push ((void *)list_command_free1, result);
              rc = xfer_data (ctx, result->str, result->len);
              pthread_cleanup_pop (1);
            }
        }
      else
        rc = xfer_data (ctx, str->str, str->len);
    }

  pthread_cleanup_pop (1);
  pthread_cleanup_pop (1);
  return rc;
}

static gpg_error_t
list_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"recurse", OPTION_TYPE_NOARG, parse_list_opt_recurse},
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    /* These are deprecated but kept for backward compatibility with older
     * clients. */
    &(struct argv_s) {"verbose", OPTION_TYPE_NOARG, NULL},
    &(struct argv_s) {"with-target", OPTION_TYPE_NOARG, NULL},
    &(struct argv_s) {"all", OPTION_TYPE_NOARG, parse_list_opt_recurse},
    &(struct argv_s) {"no-recurse", OPTION_TYPE_NOARG, NULL},
    &(struct argv_s) {"sexp", OPTION_TYPE_NOARG, parse_opt_sexp},
    NULL
  };

  if (disable_list_and_dump == 1)
    return send_error (ctx, GPG_ERR_NOT_IMPLEMENTED);

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_list (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_list (ctx, line);

  return send_error (ctx, rc);
}

/*
 * args[0] - element path
 */
static gpg_error_t
attribute_list (assuan_context_t ctx, char **args)
{
  struct client_s *client = assuan_get_pointer (ctx);
  char **attrlist = NULL;
  gpg_error_t rc;
  char *line;

  if (!args || !args[0] || !*args[0])
    return GPG_ERR_SYNTAX;

  rc = attribute_list_common (client, args[0], &attrlist);
  pthread_cleanup_push ((void *)req_free, attrlist);

  if (!rc)
    {
      line = strv_join ("\n", attrlist);
      if (line)
        {
          pthread_cleanup_push ((void *)xfree, line);
          rc = xfer_data (ctx, line, strlen (line));
          pthread_cleanup_pop (1);
        }
      else
        {
          log_write ("%s(%i): %s", __FILE__, __LINE__,
                     pwmd_strerror (GPG_ERR_ENOMEM));
          rc = GPG_ERR_ENOMEM;
        }
    }

  pthread_cleanup_pop (1);
  return rc;
}

/*
 * args[0] - attribute
 * args[1] - element path
 */
static gpg_error_t
attribute_delete (struct client_s *client, char **args)
{
  gpg_error_t rc;
  xmlNodePtr n;

  if (!args || !args[0] || !*args[0] || !args[1] || !*args[1])
    return GPG_ERR_SYNTAX;

  rc = xml_protected_attr (args[0]);
  if (rc || !strcmp (args[0], "_target"))
    return rc ? rc : GPG_ERR_EPERM;

  rc = copy_on_write (client);
  if (rc)
    return rc;

  if (!xml_reserved_attribute (args[0]))
    n = xml_resolve_path (client, client->doc, (xmlChar *)args[1], NULL, &rc);
  else
    {
      struct xml_request_s *req;

      rc = xml_new_request (client, args[1], XML_CMD_ATTR, &req);
      if (rc)
        return rc;

      n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
      xml_free_request (req);
    }

  if (!rc)
    rc = xml_is_element_owner (client, n, 0);

  if (!rc)
    rc = xml_delete_attribute (client, n, (xmlChar *) args[0]);

  return rc;
}

/*
 * Creates the "_target" attribute. When other commands encounter an element
 * with this attribute they follow the "_target" after appending remaining
 * elements from the original element path to the target. The exception is the
 * ATTR command that operates on the element itself (for reserved attribute
 * names) and not the target.
 *
 * If the source element path doesn't exist when using 'ATTR SET target', it is
 * created, but the destination element path must exist. This is similar to the
 * ls(1) command: ln -s src dst.
 *
 * args[0] - source element path
 * args[1] - destination element path
 */
static gpg_error_t
set_target_attribute (struct client_s *client, char **args)
{
  struct xml_request_s *req = NULL;
  char **src, **dst;
  gpg_error_t rc;
  xmlNodePtr n;

  if (!args || !args[0] || !args[1])
    return GPG_ERR_SYNTAX;

  src = str_split (args[0], "\t", 0);
  if (!src)
    return GPG_ERR_SYNTAX;

  if (!xml_valid_element_path (src, 0))
    {
      strv_free (src);
      return GPG_ERR_INV_VALUE;
    }

  dst = str_split (args[1], "\t", 0);
  if (!dst)
    {
      strv_free (src);
      return GPG_ERR_SYNTAX;
    }

  rc = copy_on_write (client);
  if (rc)
    goto fail;

  // Be sure the destination element path exists. */
  rc = xml_new_request (client, args[1], XML_CMD_NONE, &req);
  if (rc)
    goto fail;

  (void)xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc && rc != GPG_ERR_EACCES)
    goto fail;

  xml_free_request (req);
  req = NULL;

  if (!strcmp (args[0], args[1]))
    {
      rc = GPG_ERR_EEXIST;
      goto fail;
    }

  rc = xml_new_request (client, args[0], XML_CMD_ATTR_TARGET, &req);
  if (rc)
    goto fail;

  n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc && rc != GPG_ERR_EACCES)
    goto fail;

  if (!rc)
    {
      rc = xml_add_attribute (client, n, "_target", args[1]);
      if (!rc)
        {
          rc = xml_remove_user_attributes (client, n);
          if (!rc)
            rc = xml_unlink_node (client, n->children);
        }
    }

fail:
  strv_free (src);
  strv_free (dst);
  xml_free_request (req);
  return rc;
}

/*
 * args[0] - attribute
 * args[1] - element path
 */
static gpg_error_t
attribute_get (assuan_context_t ctx, char **args)
{
  struct client_s *client = assuan_get_pointer (ctx);
  xmlNodePtr n;
  xmlChar *a;
  gpg_error_t rc;
  struct xml_request_s *req;

  if (!args || !args[0] || !*args[0] || !args[1] || !*args[1])
    return GPG_ERR_SYNTAX;

  if (!xml_reserved_attribute (args[0]))
    n = xml_resolve_path (client, client->doc, (xmlChar *)args[1], NULL, &rc);
  else
    {
      rc = xml_new_request (client, args[1], XML_CMD_ATTR, &req);
      if (rc)
        return rc;

      n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
      xml_free_request (req);
    }

  if (rc)
    return rc;

  a = xmlGetProp (n, (xmlChar *) args[0]);
  if (!a)
    return GPG_ERR_NOT_FOUND;

  pthread_cleanup_push ((void *)xmlFree, a);

  if (*a)
    rc = xfer_data (ctx, (char *) a, xmlStrlen (a));
  else
    rc = GPG_ERR_NO_DATA;

  pthread_cleanup_pop (1);
  return rc;
}

/*
 * args[0] - attribute
 * args[1] - element path
 * args[2] - value
 */
static gpg_error_t
attribute_set (struct client_s *client, char **args)
{
  struct xml_request_s *req;
  gpg_error_t rc;
  xmlNodePtr n;

  if (!args || !args[0] || !args[1])
    return GPG_ERR_SYNTAX;

  /*
   * Reserved attribute names.
   */
  if ((rc = xml_protected_attr (args[0])))
    return rc;
  else if (!strcmp (args[0], "_target"))
    return set_target_attribute (client, args + 1);
  else if (!strcmp (args[0], "_acl"))
    rc = xml_valid_username (args[2]);
  else if (!xml_valid_attribute (args[0]))
    return GPG_ERR_INV_VALUE;

  if (rc)
    return rc;

  if (!xml_valid_attribute_value (args[2]))
    return GPG_ERR_INV_VALUE;

  rc = copy_on_write (client);
  if (rc)
    return rc;

  if (!xml_reserved_attribute (args[0]))
    {
    n = xml_resolve_path (client, client->doc, (xmlChar *)args[1], NULL, &rc);
    if (!rc)
      {
        rc = xml_new_request (client, args[1], XML_CMD_NONE, &req);
        if (!rc)
          {
            rc = xml_check_recursion (client, req);
            xml_free_request (req);
          }
      }
    }
  else
    {
      rc = xml_new_request (client, args[1], XML_CMD_ATTR, &req);
      if (rc)
        return rc;

      n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
      xml_free_request (req);
    }

  if (!rc)
    rc = xml_is_element_owner (client, n, 0);

  if (!rc)
    rc = xml_add_attribute (client, n, args[0], args[2]);

  return rc;
}

/*
 * req[0] - command
 * req[1] - attribute name or element path if command is LIST
 * req[2] - element path
 * req[2] - element path or value
 */

static gpg_error_t
do_attr (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;
  char **req;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  req = str_split (line, " ", 4);
  if (!req || !req[0] || !req[1])
    {
      strv_free (req);
      return GPG_ERR_SYNTAX;
    }

  pthread_cleanup_push ((void *)req_free, req);

  if (strcasecmp (req[0], "SET") == 0)
    rc = attribute_set (client, req + 1);
  else if (strcasecmp (req[0], "GET") == 0)
    rc = attribute_get (ctx, req + 1);
  else if (strcasecmp (req[0], "DELETE") == 0)
    rc = attribute_delete (client, req + 1);
  else if (strcasecmp (req[0], "LIST") == 0)
    rc = attribute_list (ctx, req + 1);
  else
    rc = GPG_ERR_SYNTAX;

  client->flags &= ~(FLAG_ACL_IGNORE|FLAG_ACL_ERROR);
  pthread_cleanup_pop (1);
  return rc;
}

static gpg_error_t
attr_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_attr (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_attr (ctx, line);

  return send_error (ctx, rc);
}

static gpg_error_t
parse_iscached_opt_lock (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_LOCK;
  return 0;
}

static gpg_error_t
parse_iscached_opt_agent (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_CACHE_AGENT;
  return 0;
}

static gpg_error_t
parse_iscached_opt_sign (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_CACHE_SIGN;
  return 0;
}

static gpg_error_t
iscached_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  int defer = 0;
  struct argv_s *args[] = {
    &(struct argv_s) {"lock", OPTION_TYPE_NOARG, parse_iscached_opt_lock},
    &(struct argv_s) {"agent", OPTION_TYPE_NOARG, parse_iscached_opt_agent},
    &(struct argv_s) {"sign", OPTION_TYPE_NOARG, parse_iscached_opt_sign},
    NULL
  };

  if (!line || !*line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);
  else if (!valid_filename (line))
    return send_error (ctx, GPG_ERR_INV_VALUE);

  if (!(client->flags & FLAG_OPEN)
      && ((client->opts & OPT_CACHE_AGENT) || (client->opts & OPT_CACHE_SIGN)))
    return send_error (ctx, GPG_ERR_INV_STATE);

  rc = cache_iscached (line, &defer, (client->opts & OPT_CACHE_AGENT),
                       (client->opts & OPT_CACHE_SIGN));
  if (!rc && defer)
    rc = GPG_ERR_NO_DATA;

  if (!rc)
    {
      struct cache_data_s *cdata = cache_get_data (line, NULL);

      if (cdata)
        {
          rc = validate_checksum (client, line, cdata, NULL, NULL, 1);
          if (rc == GPG_ERR_CHECKSUM)
            rc = GPG_ERR_NO_DATA;
        }
    }

  if (client->opts & OPT_LOCK
      && (!rc || gpg_err_code (rc) == GPG_ERR_NO_DATA
          || gpg_err_code (rc) == GPG_ERR_ENOENT))
    {
      gpg_error_t trc = rc;

      if (client->filename && strcmp (line, client->filename))
	reset_client (client);

      xfree (client->filename);
      client->filename = str_dup (line);
      rc = do_lock (client, 1);
      if (!rc)
	rc = trc;
    }

  return send_error (ctx, rc);
}

static gpg_error_t
clearcache_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0, all_rc = 0;
  int i;
  int t;
  int all = 0;
  struct client_thread_s *once = NULL;
  unsigned count;

  cache_lock ();
  pthread_cleanup_push (cache_unlock, NULL);
  count = cache_file_count ();
  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push ((void *)release_mutex_cb, &cn_mutex);

  if (!line || !*line)
    all = 1;

  t = slist_length (cn_thread_list);

  for (i = 0; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);
      assuan_peercred_t peer;

      if (!thd->cl)
	continue;

      /* Lock each connected clients' file mutex to prevent any other client
       * from accessing the cache entry (the file mutex is locked upon
       * command startup). The cache for the entry is not cleared if the
       * file mutex is locked by another client to prevent this function
       * from blocking. Rather, it returns an error.
       */
      if (all)
	{
	  if (thd->cl->filename)
            {
              rc = do_validate_peer (ctx, thd->cl->filename, &peer, NULL);
              /* The current client doesn't have permission to open the other
               * filename do to "access" configuration parameter in a filename
               * section. Since we are clearning all cache entries the error
               * will be returned later during cache_clear(). */
              if (rc)
                {
                  rc = 0;
                  continue;
                }
            }
          else // Idle client without opened file?
            continue;

	  rc = cache_lock_mutex (thd->cl->ctx, thd->cl->filename, -1, 0, -1);
	  if (gpg_err_code (rc) == GPG_ERR_LOCKED)
	    {
              /* The current client owns the lock. */
	      if (pthread_equal (pthread_self (), thd->tid))
		rc = 0;
	      else
		{
		  if (cache_iscached (thd->cl->filename, NULL, 0, 0)
                      == GPG_ERR_NO_DATA)
		    {
		      rc = 0;
		      continue;
		    }

                  /* The cache entry will be cleared when the other client
                   * disconnects and cache_timer_thread() does its thing. */
		  cache_defer_clear (thd->cl->filename);
		}
	    }
	  else if (gpg_err_code (rc) == GPG_ERR_NO_DATA)
	    {
	      rc = 0;
	      continue;
	    }

	  if (!rc)
	    {
	      rc = cache_clear (NULL, thd->cl->filename, 1, 0);
	      cache_unlock_mutex (thd->cl->filename, 0);
	    }

	  if (rc)
	    all_rc = rc;

	  rc = 0;
	}
      else
	{
          /* A single data filename was specified. Lock only this data file
           * mutex and free the cache entry. */
	  rc = do_validate_peer (ctx, line, &peer, NULL);
          if (rc == GPG_ERR_FORBIDDEN)
            rc = peer_is_invoker (client);

          if (rc == GPG_ERR_EACCES)
            rc = GPG_ERR_FORBIDDEN;

	  if (!rc && thd->cl->filename && !strcmp (thd->cl->filename , line))
	    {
	      rc = cache_lock_mutex (thd->cl->ctx, thd->cl->filename, -1, 0, -1);
	      if (gpg_err_code (rc) == GPG_ERR_LOCKED)
		{
                  /* The current client owns the lock. */
		  if (pthread_equal (pthread_self (), thd->tid))
		    rc = 0;
		}

	      if (!rc)
		{
		  once = thd;
		  rc = cache_clear (NULL, thd->cl->filename, 1, 0);
		  cache_unlock_mutex (thd->cl->filename, 0);
		}
              else
                cache_defer_clear (thd->cl->filename);

              /* Found a client with the opened file. The cache entry has been
               * either cleared (self) or deferred to be cleared. */
	      break;
	    }
	}
    }

  /* Only connected clients' cache entries have been cleared. Now clear any
   * remaining cache entries without clients but only if there wasn't an
   * error from above since this would defeat the locking check of the
   * remaining entries. */
  if (all && !all_rc && cache_file_count ())
    {
      rc = cache_clear (client, NULL, 1, 0);
      if (rc == GPG_ERR_EACCES)
        rc = GPG_ERR_FORBIDDEN;
    }

  /* No clients are using the specified file. */
  else if (!all_rc && !rc && !once)
    rc = cache_clear (NULL, line, 1, 0);

  /* Release the connection mutex. */
  pthread_cleanup_pop (1);

  if (!rc || (cache_file_count () && count != cache_file_count ()))
    send_status_all (STATUS_CACHE, NULL);

  /* Release the cache mutex. */
  pthread_cleanup_pop (1);

  /* One or more files were locked while clearing all cache entries. */
  if (all_rc)
    rc = all_rc;

  return send_error (ctx, rc);
}

static gpg_error_t
cachetimeout_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  long timeout;
  char **req = str_split (line, " ", 0);
  char *p;
  gpg_error_t rc = 0;

  if (!(client->flags & FLAG_OPEN))
    return send_error (ctx, GPG_ERR_INV_STATE);

  if (!req || !*req || strv_length (req) > 1)
    {
      strv_free (req);
      return send_error (ctx, GPG_ERR_SYNTAX);
    }

  errno = 0;
  timeout = strtol (req[0], &p, 10);
  if (errno != 0 || *p || timeout < (long)-1)
    rc = GPG_ERR_SYNTAX;

  if (!rc)
    {
      rc = cache_set_timeout (client->filename, timeout);
      if (!rc || gpg_err_code (rc) == GPG_ERR_NOT_FOUND)
	{
	  rc = 0;
	  MUTEX_LOCK (&rcfile_mutex);
	  config_set_long_param (&global_config, client->filename,
                                 "cache_timeout", req[0]);
	  MUTEX_UNLOCK (&rcfile_mutex);
	}
    }

  strv_free (req);
  return send_error (ctx, rc);
}

static gpg_error_t
dump_command (assuan_context_t ctx, char *line)
{
  xmlChar *xml;
  int len;
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;

  if (disable_list_and_dump == 1)
    return send_error (ctx, GPG_ERR_NOT_IMPLEMENTED);

  if (line && *line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = peer_is_invoker(client);
  if (rc)
    return send_error (ctx, (rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc));

  xmlDocDumpFormatMemory (client->doc, &xml, &len, 1);

  if (!xml)
    {
      log_write ("%s(%i): %s", __FILE__, __LINE__,
		 pwmd_strerror (GPG_ERR_ENOMEM));
      return send_error (ctx, GPG_ERR_ENOMEM);
    }

  pthread_cleanup_push ((void *)xmlFree, xml);
  rc = xfer_data (ctx, (char *) xml, len);
  pthread_cleanup_pop (1);
  return send_error (ctx, rc);
}

static gpg_error_t
getconfig_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;
  char filename[255] = { 0 }, param[747] = { 0 };
  char *p, *tmp = NULL, *fp = client->filename, *paramp = line;

  if (!line || !*line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  if (strchr (line, ' '))
    {
      int ret = sscanf (line, " %254[^ ] %746c", filename, param);

      if (ret <= 0)
        return send_error (ctx, gpg_error_from_syserror());
      paramp = param;
      fp = filename;
    }

  if (fp && !valid_filename (fp))
    return send_error (ctx, GPG_ERR_INV_VALUE);

  paramp = str_down (paramp);
  p = config_get_value (fp ? fp : "global", paramp);
  if (!p)
    return send_error (ctx, GPG_ERR_UNKNOWN_OPTION);

  tmp = expand_homedir (p);
  xfree (p);
  if (!tmp)
    {
      log_write ("%s(%i): %s", __FILE__, __LINE__,
		 pwmd_strerror (GPG_ERR_ENOMEM));
      return send_error (ctx, GPG_ERR_ENOMEM);
    }

  p = tmp;
  pthread_cleanup_push ((void *)xfree, p);
  rc = xfer_data (ctx, p, strlen (p));
  pthread_cleanup_pop (1);
  return send_error (ctx, rc);
}

struct xpath_s
{
  xmlXPathContextPtr xp;
  xmlXPathObjectPtr result;
  xmlBufferPtr buf;
  char **req;
};

static void
xpath_command_free (void *arg)
{
  struct xpath_s *xpath = arg;

  if (!xpath)
    return;

  req_free (xpath->req);

  if (xpath->buf)
    xmlBufferFree (xpath->buf);

  if (xpath->result)
    xmlXPathFreeObject (xpath->result);

  if (xpath->xp)
    xmlXPathFreeContext (xpath->xp);

  xfree (xpath);
}

static gpg_error_t
do_xpath (assuan_context_t ctx, char *line)
{
  gpg_error_t rc;
  struct client_s *client = assuan_get_pointer (ctx);
  struct xpath_s *xpath = NULL;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  xpath = xcalloc (1, sizeof (struct xpath_s));
  if (!xpath)
    return GPG_ERR_ENOMEM;

  if ((xpath->req = str_split (line, "\t", 2)) == NULL)
    {
      if (strv_printf (&xpath->req, "%s", line) == 0)
        {
          xpath_command_free (xpath);
          return GPG_ERR_ENOMEM;
        }
    }

  if (xpath->req[1])
    {
      rc = copy_on_write (client);
      if (rc)
        goto fail;
    }

  xpath->xp = xmlXPathNewContext (client->doc);
  if (!xpath->xp)
    {
      rc = GPG_ERR_BAD_DATA;
      goto fail;
    }

  xpath->result =
    xmlXPathEvalExpression ((xmlChar *) xpath->req[0], xpath->xp);
  if (!xpath->result)
    {
      rc = GPG_ERR_BAD_DATA;
      goto fail;
    }

  if (xmlXPathNodeSetIsEmpty (xpath->result->nodesetval))
    {
      rc = GPG_ERR_ELEMENT_NOT_FOUND;
      goto fail;
    }

  rc = xml_recurse_xpath_nodeset (client, client->doc,
                                  xpath->result->nodesetval,
                                  (xmlChar *) xpath->req[1], &xpath->buf, 0,
                                  NULL);
  if (rc)
    goto fail;
  else if (!xpath->req[1] && !xmlBufferLength (xpath->buf))
    {
      rc = GPG_ERR_NO_DATA;
      goto fail;
    }
  else if (xpath->req[1])
    {
      rc = 0;
      goto fail;
    }

  pthread_cleanup_push ((void *)xpath_command_free, xpath);
  rc = xfer_data (ctx, (char *) xmlBufferContent (xpath->buf),
		  xmlBufferLength (xpath->buf));
  pthread_cleanup_pop (0);
fail:
  xpath_command_free (xpath);
  return rc;
}

static gpg_error_t
xpath_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  if (disable_list_and_dump == 1)
    return send_error (ctx, GPG_ERR_NOT_IMPLEMENTED);

  rc = peer_is_invoker(client);
  if (rc)
    return send_error (ctx, (rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc));

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_xpath (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_xpath (ctx, line);

  return send_error (ctx, rc);
}

static gpg_error_t
do_xpathattr (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  char **req = NULL;
  int cmd = 0;			//SET
  struct xpath_s *xpath = NULL;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  if ((req = str_split (line, " ", 3)) == NULL)
    return GPG_ERR_ENOMEM;

  if (!req[0])
    {
      rc = GPG_ERR_SYNTAX;
      goto fail;
    }

  if (!strcasecmp (req[0], "SET"))
    cmd = 0;
  else if (!strcasecmp (req[0], "DELETE"))
    cmd = 1;
  else
    {
      rc = GPG_ERR_SYNTAX;
      goto fail;
    }

  if (!req[1] || !req[2])
    {
      rc = GPG_ERR_SYNTAX;
      goto fail;
    }

  xpath = xcalloc (1, sizeof (struct xpath_s));
  if (!xpath)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  if ((xpath->req = str_split (req[2], "\t", 3)) == NULL)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  if (!xpath->req[0] || (!xpath->req[1] && !cmd) || (xpath->req[1] && cmd))
    {
      rc = GPG_ERR_SYNTAX;
      goto fail;
    }

  rc = copy_on_write (client);
  if (rc)
    goto fail;

  xpath->xp = xmlXPathNewContext (client->doc);
  if (!xpath->xp)
    {
      rc = GPG_ERR_BAD_DATA;
      goto fail;
    }

  xpath->result = xmlXPathEvalExpression ((xmlChar *) xpath->req[0], xpath->xp);
  if (!xpath->result)
    {
      rc = GPG_ERR_BAD_DATA;
      goto fail;
    }

  if (xmlXPathNodeSetIsEmpty (xpath->result->nodesetval))
    {
      rc = GPG_ERR_ELEMENT_NOT_FOUND;
      goto fail;
    }

  rc = xml_recurse_xpath_nodeset (client, client->doc,
                                  xpath->result->nodesetval,
                                  (xmlChar *) xpath->req[1], &xpath->buf, cmd,
                                  (xmlChar *) req[1]);

fail:
  xpath_command_free (xpath);
  strv_free (req);
  return rc;
}

/* XPATHATTR SET|DELETE <name> <expression>[<TAB>[value]] */
static gpg_error_t
xpathattr_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  if (disable_list_and_dump == 1)
    return send_error (ctx, GPG_ERR_NOT_IMPLEMENTED);

  rc = peer_is_invoker(client);
  if (rc)
    return send_error (ctx, (rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc));

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_xpathattr (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_xpathattr (ctx, line);

  return send_error (ctx, rc);
}

static gpg_error_t
do_import (struct client_s *client, const char *root_element,
	   unsigned char *content)
{
  xmlDocPtr doc = NULL;
  xmlNodePtr n = NULL, root;
  gpg_error_t rc = 0;
  struct string_s *str = NULL, *tstr = NULL;
  struct xml_request_s *req = NULL;

  if (!content || !*content)
    return GPG_ERR_SYNTAX;

  if (root_element)
    {
      rc = xml_new_request (client, root_element, XML_CMD_STORE, &req);
      if (rc)
        {
          xfree (content);
          return rc;
        }

      if (!xml_valid_element_path (req->args, 0))
        {
          xml_free_request (req);
          xfree (content);
          return GPG_ERR_INV_VALUE;
        }
    }

  str = string_new_content ((char *)content);
  tstr = string_prepend (str, "<pwmd>");
  if (tstr)
    {
      str = tstr;
      tstr = string_append (str, "</pwmd>");
      if (!tstr)
        rc = GPG_ERR_ENOMEM;
      else
        str = tstr;
    }
  else
    rc = GPG_ERR_ENOMEM;

  if (rc)
    goto fail;

  doc = xmlReadDoc ((xmlChar *) str->str, NULL, "UTF-8", XML_PARSE_NOBLANKS);
  string_free (str, 1);
  if (!doc)
    {
      rc = GPG_ERR_BAD_DATA;
      goto fail;
    }

  root = xmlDocGetRootElement (doc);
  root = root->children;
  if (!root || root->type != XML_ELEMENT_NODE)
    {
      rc = GPG_ERR_BAD_DATA;
      goto fail;
    }

  rc = xml_validate_import (client, root);
  if (rc)
    goto fail;

  if (req)
    {
      /* Create the new specified root element path, if needed. */
      n = xml_find_elements (client, req, xmlDocGetRootElement (req->doc),
                              &rc);
      if (rc)
        goto fail;

      /* Overwrite the children of the specified root element path. */
      (void)xml_unlink_node (client, n->children);
      n->children = NULL;
    }
  else
    n = xmlDocGetRootElement (client->doc);

  if (n)
    {
      xmlNodePtr copy;
      xmlChar *name = xml_attribute_value (root, (xmlChar *)"_name");

      if (!name)
        rc = GPG_ERR_BAD_DATA;
      else
        rc = xml_is_element_owner (client, n, 0);

      if (!rc && !req)
        {
          /* No --root argument passed. Overwrite the current documents' root
           * element matching the root element of the imported data. */
          xml_free_request (req);
          req = NULL;
          rc = xml_new_request (client, (char *)name, XML_CMD_DELETE, &req);
          xmlFree (name);
          if (!rc)
            {
              xmlNodePtr tmp;

              tmp = xml_find_elements (client, req,
                                        xmlDocGetRootElement (req->doc), &rc);
              if (rc && rc != GPG_ERR_ELEMENT_NOT_FOUND)
                goto fail;

              if (!rc)
                {
                  rc = xml_is_element_owner (client, tmp, 0);
                  if (rc)
                    goto fail;
                }

              (void)xml_unlink_node (client, tmp);
              rc = 0;
            }
        }

      if (!rc)
        {
          copy = xmlCopyNodeList (root);
          if (!copy)
            rc = GPG_ERR_ENOMEM;
          else
            {
              n = xmlAddChildList (n, copy);
              if (!n)
                rc = GPG_ERR_ENOMEM;
            }
        }
    }

  if (!rc && n && n->parent)
    rc = xml_update_element_mtime (client, n->parent);

fail:
  xml_free_request (req);

  if (doc)
    xmlFreeDoc (doc);

  return rc;
}

static gpg_error_t
parse_import_opt_root (void *data, void *value)
{
  struct client_s *client = data;

  client->import_root = str_dup (value);
  return client->import_root ? 0 : GPG_ERR_ENOMEM;
}

static gpg_error_t
import_command (assuan_context_t ctx, char *line)
{
  gpg_error_t rc;
  struct client_s *client = assuan_get_pointer (ctx);
  unsigned char *result;
  size_t len;
  struct argv_s *args[] = {
      &(struct argv_s) {
          "root", OPTION_TYPE_ARG, parse_import_opt_root
      },
      NULL
  };

  xfree (client->import_root);
  client->import_root = NULL;
  rc = parse_options (&line, args, client, client->bulk_p ? 1 : 0);
  if (rc)
    return send_error (ctx, rc);

  rc = copy_on_write (client);
  if (rc)
    return send_error (ctx, rc);

  if (!client->bulk_p)
    {
      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
        {
          xfree (client->import_root);
          client->import_root = NULL;
          return send_error (ctx, rc);
        }
      rc = do_import (client, client->import_root, result);
    }
  else
    {
      char *p = str_dup (line);

      if (!p)
        rc = GPG_ERR_ENOMEM;
      else
        rc = do_import (client, client->import_root, (unsigned char *)p);
    }

  xfree (client->import_root);
  client->import_root = NULL;
  return send_error (ctx, rc);
}

static gpg_error_t
do_lock (struct client_s *client, int add)
{
  gpg_error_t rc = lock_file_mutex (client, add);

  if (!rc)
    client->flags |= FLAG_LOCK_CMD;

  return rc;
}

static gpg_error_t
lock_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;

  if (line && *line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = do_lock (client, 0);
  return send_error (ctx, rc);
}

static gpg_error_t
unlock_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;

  if (line && *line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = unlock_file_mutex (client, 0);
  return send_error (ctx, rc);
}

static void
set_env (const char *name, char *buf, size_t size, const char *value)
{
  MUTEX_LOCK (&rcfile_mutex);
  if (!value || !*value)
    {
      gpgrt_unsetenv (name);
      buf[0] = 0;
    }
  else if (!buf[0] || strcmp (buf, value))
    gpgrt_setenv (name, value, 1);

  MUTEX_UNLOCK (&rcfile_mutex);
}

static gpg_error_t
option_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;
  char namebuf[255] = { 0 };
  char *name = namebuf;
  char *value = NULL, *p, *tmp = NULL;

  p = strchr (line, '=');
  if (!p)
    {
      strncpy (namebuf, line, sizeof(namebuf));
      namebuf[sizeof(namebuf)-1] = 0;
    }
  else
    {
      tmp = str_dup (line);
      if (!tmp)
        return send_error (ctx, GPG_ERR_ENOMEM);

      tmp[strlen (line) - strlen (p)] = 0;
      strncpy (namebuf, tmp, sizeof (namebuf));
      namebuf[sizeof(namebuf)-1] = 0;
      xfree (tmp);
      tmp = NULL;
      value = p+1;
    }

  log_write2 ("OPTION name='%s' value='%s'", name, value);

  if (strcasecmp (name, (char *) "lock-timeout") == 0)
    {
      long n = 0;

      if (value)
	{
	  n = strtol (value, &tmp, 10);
	  if (tmp && *tmp)
	    return send_error (ctx, GPG_ERR_INV_VALUE);
	}

      client->lock_timeout = n;
    }
  else if (strcasecmp (name, (char *) "client-state") == 0)
    {
      long n = 0;

      MUTEX_LOCK (&client->thd->status_mutex);
      if (value)
	{
	  n = strtol (value, &tmp, 10);
	  if ((tmp && *tmp) || (n < 0 || n > 1))
            {
              MUTEX_UNLOCK (&client->thd->status_mutex);
              return send_error (ctx, GPG_ERR_INV_VALUE);
            }
          client->thd->send_state = n;
	}
      else
        client->thd->send_state = 0;
      MUTEX_UNLOCK (&client->thd->status_mutex);
    }
  else if (strcasecmp (name, (char *) "NAME") == 0)
    {
      if (value && strchr (value, ' '))
        rc = GPG_ERR_INV_VALUE;
      else
        {
          MUTEX_LOCK (&cn_mutex);
          tmp = pthread_getspecific (thread_name_key);
          pthread_setspecific (thread_name_key, NULL);
          xfree (tmp);
          xfree (client->thd->name);
          client->thd->name = NULL;
          if (value && *value)
            {
              pthread_setspecific (thread_name_key, str_dup (value));
              client->thd->name = str_dup (value);
            }
          MUTEX_UNLOCK (&cn_mutex);
        }
    }
  else if (strcasecmp (name, "disable-pinentry") == 0)
    {
      int n = 1;

      if (value && *value)
	{
	  n = (int) strtol (value, &tmp, 10);
	  if (*tmp || n < 0 || n > 1)
	    return send_error (ctx, GPG_ERR_INV_VALUE);
	}

      if (n)
	client->flags |= FLAG_NO_PINENTRY;
      else
	client->flags &= ~FLAG_NO_PINENTRY;
    }
  else if (strcasecmp (name, "ttyname") == 0)
    set_env ("GPG_TTY", env_gpg_tty, sizeof (env_gpg_tty), value);
  else if (strcasecmp (name, "ttytype") == 0)
    set_env ("TERM", env_term, sizeof (env_term), value);
  else if (strcasecmp (name, "display") == 0)
    set_env ("DISPLAY", env_display, sizeof (env_display), value);
  else if (strcasecmp (name, "lc_messages") == 0)
    {
    }
  else if (strcasecmp (name, "lc_ctype") == 0)
    {
    }
  else if (strcasecmp (name, "desc") == 0)
    {
    }
  else if (strcasecmp (name, "pinentry-timeout") == 0)
    {
    }
  else
    rc = GPG_ERR_UNKNOWN_OPTION;

  return send_error (ctx, rc);
}

static gpg_error_t
do_rename (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  char **args, *p;
  xmlNodePtr src, dst;
  struct string_s *str = NULL, *tstr;
  struct xml_request_s *req;
  gpg_error_t rc;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  args = str_split (line, " ", 0);
  if (!args || strv_length (args) != 2)
    {
      strv_free (args);
      return GPG_ERR_SYNTAX;
    }

  if (!xml_valid_element ((xmlChar *) args[1]))
    {
      strv_free (args);
      return GPG_ERR_INV_VALUE;
    }

  rc = xml_new_request (client, args[0], XML_CMD_RENAME, &req);
  if (rc)
    {
      strv_free (args);
      return rc;
    }

  src = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc)
    goto fail;

  rc = xml_is_element_owner (client, src, 0);
  if (rc)
    goto fail;

  xmlChar *a = xmlGetProp (src, (xmlChar *) "_name");
  if (!a)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  /* To prevent unwanted effects:
   *
   * <element _name="a"><element _name="b"/></element>
   *
   * RENAME a<TAB>b b
   */
  if (xmlStrEqual (a, (xmlChar *) args[1]))
    {
      xmlFree (a);
      rc = GPG_ERR_EEXIST;
      goto fail;
    }

  xmlFree (a);
  str = string_new (args[0]);
  if (!str)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  p = strrchr (str->str, '\t');
  if (p)
    tstr = string_truncate (str, strlen (str->str)-strlen (p));
  else
    tstr = string_truncate (str, 0);

  if (!tstr)
    {
      string_free (str, 1);
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  str = tstr;
  tstr = string_append_printf (str, "%s%s", str->len ? "\t" : "", args[1]);
  if (!tstr)
    {
      string_free (str, 1);
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  str = tstr;
  xml_free_request (req);
  rc = xml_new_request (client, str->str, XML_CMD_RENAME, &req);
  string_free (str, 1);
  if (rc)
    {
      req = NULL;
      goto fail;
    }

  dst = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc && rc != GPG_ERR_ELEMENT_NOT_FOUND)
    goto fail;

  rc = 0;

  /* Target may exist:
   *
   * <element _name="a"/>
   * <element _name="b" target="a"/>
   *
   * RENAME b a
   */
  if (src == dst)
    {
      rc = GPG_ERR_EEXIST;
      goto fail;
    }

  if (dst)
    {
      rc = xml_is_element_owner (client, dst, 0);
      if (rc)
        goto fail;

      rc = xml_add_attribute (client, src, "_name", args[1]);
      if (!rc)
        xml_unlink_node (client, dst);
    }
  else
    rc = xml_add_attribute (client, src, "_name", args[1]);

fail:
  xml_free_request (req);
  strv_free (args);
  return rc;
}

static gpg_error_t
rename_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  rc = copy_on_write (client);
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_rename (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_rename (ctx, line);

  return send_error (ctx, rc);
}

static gpg_error_t
do_copy (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  char **args, **targs;
  xmlNodePtr src, dst, tree = NULL;
  struct xml_request_s *req;
  gpg_error_t rc;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  args = str_split (line, " ", 0);
  if (!args || strv_length (args) != 2)
    {
      strv_free (args);
      return GPG_ERR_SYNTAX;
    }

  targs = str_split (args[1], "\t", 0);
  if (!targs)
    {
      strv_free (args);
      return GPG_ERR_SYNTAX;
    }

  if (!xml_valid_element_path (targs, 0))
    {
      strv_free (args);
      strv_free (targs);
      return GPG_ERR_INV_VALUE;
    }
  strv_free (targs);

  rc = xml_new_request (client, args[0], XML_CMD_NONE, &req);
  if (rc)
    {
      strv_free (args);
      return rc;
    }

  src = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc)
    goto fail;

  tree = xmlCopyNodeList (src);
  if (!tree)
    {
      rc = GPG_ERR_ENOMEM;
      goto fail;
    }

  xml_free_request (req);
  /* Create the destination element path if it does not exist. */
  rc = xml_new_request (client, args[1], XML_CMD_STORE, &req);
  if (rc)
    {
      req = NULL;
      goto fail;
    }

  dst = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc)
    goto fail;

  rc = xml_is_element_owner (client, dst, 0);
  if (rc)
    goto fail;

  if (!(req->flags & XML_REQUEST_FLAG_CREATED))
    {
      rc = xml_validate_target (client, req->doc, args[1], src);
      if (rc || src == dst)
        {
          rc = rc ? rc : GPG_ERR_EEXIST;
          goto fail;
        }
    }

  /* Merge any attributes from the src node to the initial dst node. */
  for (xmlAttrPtr attr = tree->properties; attr; attr = attr->next)
    {
      if (xmlStrEqual (attr->name, (xmlChar *) "_name"))
	continue;

      xmlAttrPtr a = xmlHasProp (dst, attr->name);
      if (a)
	xmlRemoveProp (a);

      xmlChar *tmp = xmlNodeGetContent (attr->children);
      xmlNewProp (dst, attr->name, tmp);
      xmlFree (tmp);
      /* Create the default attributes. */
      rc = xml_add_attribute (client, dst, NULL, NULL);
    }

  xmlNodePtr n = dst->children;
  (void)xml_unlink_node (client, n);
  dst->children = NULL;

  if (tree->children)
    {
      n = xmlCopyNodeList (tree->children);
      if (!n)
	{
	  rc = GPG_ERR_ENOMEM;
	  goto fail;
	}

      n = xmlAddChildList (dst, n);
      if (!n)
	{
	  rc = GPG_ERR_ENOMEM;
	  goto fail;
	}

      rc = xml_update_element_mtime (client, xmlDocGetRootElement (client->doc)
                                     == dst->parent ? dst : dst->parent);
    }

fail:
  if (tree)
    (void)xml_unlink_node (client, tree);

  xml_free_request (req);
  strv_free (args);
  return rc;
}

static gpg_error_t
copy_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  rc = copy_on_write (client);
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_copy (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_copy (ctx, line);

  return send_error (ctx, rc);
}

static gpg_error_t
do_move (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  char **args;
  xmlNodePtr src, dst;
  struct xml_request_s *req;
  gpg_error_t rc;

  if (!line || !*line)
    return GPG_ERR_SYNTAX;

  args = str_split (line, " ", 0);
  if (!args || strv_length (args) != 2)
    {
      strv_free (args);
      return GPG_ERR_SYNTAX;
    }

  if (*args[1])
    {
      char **targs = str_split (args[1], "\t", 0);
      if (!targs)
        {
          strv_free (args);
          return GPG_ERR_ENOMEM;
        }

      if (!xml_valid_element_path (targs, 0))
        {
          strv_free (targs);
          strv_free (args);
          return GPG_ERR_INV_VALUE;
        }

      strv_free (targs);
    }

  rc = xml_new_request (client, args[0], XML_CMD_MOVE, &req);
  if (rc)
    {
      strv_free (args);
      return rc;
    }

  src = xml_find_elements (client, req, xmlDocGetRootElement (req->doc), &rc);
  if (rc)
    goto fail;

  rc = xml_is_element_owner (client, src, 0);
  if (rc)
    goto fail;

  xml_free_request (req);
  req = NULL;
  if (*args[1])
    {
      rc = xml_new_request (client, args[1], XML_CMD_NONE, &req);
      if (rc)
        goto fail;

      dst = xml_find_elements (client, req, xmlDocGetRootElement (req->doc),
                                &rc);
    }
  else
    dst = xmlDocGetRootElement (client->doc);

  if (rc && rc != GPG_ERR_ELEMENT_NOT_FOUND)
    goto fail;

  rc = 0;

  for (xmlNodePtr n = dst; n; n = n->parent)
    {
      if (n == src)
	{
	  rc = GPG_ERR_EEXIST;
	  goto fail;
	}
    }

  if (dst)
    {
      xmlChar *a = xml_attribute_value (src, (xmlChar *) "_name");
      xmlNodePtr dup = xml_find_element (client, dst->children, (char *) a, &rc);

      xmlFree (a);
      if (rc && rc != GPG_ERR_ELEMENT_NOT_FOUND)
        goto fail;

      rc = 0;

      if (dup)
	{
	  if (dup == src)
            {
              rc = GPG_ERR_EEXIST;
              goto fail;
            }

	  if (dst == xmlDocGetRootElement (client->doc))
	    {
	      xmlNodePtr n = src;
	      int match = 0;

	      while (n->parent && n->parent != dst)
		n = n->parent;

	      a = xml_attribute_value (n, (xmlChar *) "_name");
	      xmlChar *b = xml_attribute_value (src, (xmlChar *) "_name");

	      if (xmlStrEqual (a, b))
		{
		  match = 1;
		  xmlUnlinkNode (src);
                  (void)xml_unlink_node (client, n);
		}

	      xmlFree (a);
	      xmlFree (b);

	      if (!match)
                (void)xml_unlink_node (client, dup);
	    }
	  else
	    xmlUnlinkNode (dup);
	}
    }

  if (!dst && req && req->args && *req->args)
    {
      struct xml_request_s *nreq = NULL;
      xmlChar *name = xml_attribute_value (src, (xmlChar *) "_name");

      if (src->parent == xmlDocGetRootElement (client->doc)
	  && !strcmp ((char *) name, *req->args))
	{
	  xmlFree (name);
	  rc = GPG_ERR_EEXIST;
	  goto fail;
	}

      xmlFree (name);
      rc = xml_new_request (client, args[1], XML_CMD_STORE, &nreq);
      if (!rc)
        {
          dst = xml_find_elements (client, nreq,
                                    xmlDocGetRootElement (nreq->doc), &rc);
          xml_free_request (nreq);
        }

      if (rc)
        goto fail;
    }

  xml_update_element_mtime (client, src->parent);
  xmlUnlinkNode (src);

  dst = xmlAddChildList (dst, src);
  if (!dst)
    rc = GPG_ERR_ENOMEM;
  else
    xml_update_element_mtime (client, dst->parent);

fail:
  xml_free_request (req);
  strv_free (args);
  return rc;
}

static gpg_error_t
move_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (!rc && (client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  if (rc)
    return send_error (ctx, rc);

  rc = copy_on_write (client);
  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      unsigned char *result;
      size_t len;

      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
	return send_error (ctx, rc);

      pthread_cleanup_push ((void *)xfree, result);
      rc = do_move (ctx, (char *)result);
      pthread_cleanup_pop (1);
    }
  else
    rc = do_move (ctx, line);

  return send_error (ctx, rc);
}

static int
sort_files (const void *arg1, const void *arg2)
{
  char *const *a = arg1;
  char *const *b = arg2;

  return strcmp (*a, *b);
}

static gpg_error_t
ls_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  char *tmp;
  char *dir;
  DIR *d;
  char *list = NULL;
  char **v = NULL;
  struct dirent *cur = NULL;
  struct argv_s *args[] = {
    &(struct argv_s) {"verbose", OPTION_TYPE_NOARG, parse_opt_verbose},
    NULL
  };

  rc = parse_options (&line, args, client, 0);
  if (rc)
    return send_error (ctx, rc);

  tmp = str_asprintf ("%s/data", homedir);
  dir = expand_homedir (tmp);
  xfree (tmp);
  if (!dir)
    return send_error (ctx, GPG_ERR_ENOMEM);

  d = opendir (dir);
  rc = gpg_error_from_errno (errno);

  if (!d)
    {
      xfree (dir);
      return send_error (ctx, rc);
    }

  pthread_cleanup_push ((void *)closedir, d);
  xfree (dir);
  rc = 0;

  while ((cur = readdir (d)))
    {
      char **vtmp;
      struct stat st;

      rc = open_check_file (cur->d_name, NULL, &st, 1);
      if (rc)
        continue;

      if (client->opts & OPT_VERBOSE)
        tmp = str_asprintf ("%s %lu.%lu %lu.%lu %lu.%lu", cur->d_name,
                            st.st_atim.tv_sec, st.st_atim.tv_nsec,
                            st.st_mtim.tv_sec, st.st_mtim.tv_nsec,
                            st.st_ctim.tv_sec, st.st_ctim.tv_nsec);
      else
        tmp = str_dup (cur->d_name);

      if (!tmp)
        {
          rc = GPG_ERR_ENOMEM;
          break;
        }

      vtmp = strv_cat (v, tmp);
      if (!vtmp)
        {
          xfree (tmp);
          rc = GPG_ERR_ENOMEM;
          break;
        }

      v = vtmp;
    }

  pthread_cleanup_pop (1); // closedir (d)

  if (rc && rc != GPG_ERR_ENODEV)
    {
      strv_free (v);
      return send_error (ctx, rc);
    }

  rc = 0;
  if (v && *v)
    {
      unsigned n, t = strv_length (v);

      qsort (v, t, sizeof (char **), sort_files);

      for (n = 0; n < t; n++)
        {
          tmp = str_asprintf ("%s%s\n", list ? list : "", v[n]);
          if (!tmp)
            {
              xfree (list);
              list = NULL;
              rc = GPG_ERR_ENOMEM;
              break;
            }

          xfree (list);
          list = tmp;
        }
    }

  strv_free (v);
  if (rc || !list)
    return send_error (ctx, rc ? rc : GPG_ERR_NO_DATA);

  list[strlen (list) - 1] = 0;
  pthread_cleanup_push (xfree, list);
  rc = xfer_data (ctx, list, strlen (list));
  pthread_cleanup_pop (1);
  return send_error (ctx, rc);
}

/* Run all commands in the command list 'head'. The return code isn't
 * considered at all for a command. It is stored in the list and later sent to
 * the client. */
static gpg_error_t
dispatch_bulk_commands (struct client_s *client, struct sexp_s **list)
{
  unsigned i;
  unsigned id = 0;
  gpg_error_t rc = 0;

  for (i = 0; list && list[i];)
    {
      struct bulk_cmd_s *cur = list[i]->user;

      if (!strcmp (list[i]->tag, "command"))
        {
          i++;
          continue;
        }
      else if (!strcmp (list[i]->tag, "id"))
        {
          id = i++;
          continue;
        }

      if ((client->flags & FLAG_NO_PINENTRY) && cur->cmd->inquire)
        {
          rc = send_status (client->ctx, STATUS_BULK, "BEGIN %s",
                            list[id]->data);
          if (rc)
            break;
        }

      if (!cur)
        continue;

      client->bulk_p = cur;
      cur->ran = 1;

      cur->rc = command_startup (client->ctx, cur->cmd->name);
      if (!cur->rc)
        cur->rc = cur->cmd->handler (client->ctx, list[i]->data);

      if ((client->flags & FLAG_NO_PINENTRY) && cur->cmd->inquire)
        {
          (void)send_status (client->ctx, STATUS_BULK, "END %s",
                             list[id]->data);
        }
      command_finalize (client->ctx, cur->rc);
      cur->rc = cur->rc ? cur->rc : client->last_rc;

      do {
          if (list[i+1] && !strcmp (list[i+1]->tag, "rc"))
            {
              gpg_error_t erc = 0;
              int match = 0;

              if (list[i+1]->data)
                {
                  char **rcs = str_split (list[i+1]->data, "|", 0);
                  char **p;

                  for (p = rcs; p && *p; p++)
                    {
                      erc = strtoul (*p, NULL, 10);
                      if (erc == cur->rc)
                        {
                          match = 1;
                          break;
                        }
                    }

                  strv_free (rcs);
                }

              if (!match)
                {
                  i++;
                  continue;
                }

              rc = dispatch_bulk_commands (client, list[++i]->next);
            }
          else
            {
              i++;
              break;
            }
      } while (1);
    }

  return rc;
}

static gpg_error_t
bulk_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;
  unsigned char *result, *p;
  size_t len;
  struct sexp_s **sexp = NULL;
  struct argv_s *args[] = {
    &(struct argv_s) {"inquire", OPTION_TYPE_NOARG, parse_opt_inquire},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);

  if ((client->opts & OPT_INQUIRE) && line && *line)
    rc = GPG_ERR_SYNTAX;
  else if (!(client->opts & OPT_INQUIRE) && (!line || !*line))
    rc = GPG_ERR_SYNTAX;

  if (rc)
    return send_error (ctx, rc);

  if (client->opts & OPT_INQUIRE)
    {
      rc = assuan_inquire (ctx, "DATA", &result, &len, 0);
      if (rc)
        return send_error (ctx, rc);
      p = result;
    }
  else
    {
      p = (unsigned char *)line;
      len = line && *line ? strlen (line) : 0;
    }

  if (!len)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = bulk_parse_commands (&sexp, (const char *)p, command_table);
  if (client->opts & OPT_INQUIRE)
    xfree (result);

  if (rc)
    return send_error (ctx, rc);

  pthread_cleanup_push ((void *)bulk_free_list, sexp);
  rc = dispatch_bulk_commands (client, sexp);
  pthread_cleanup_pop (0);
  if (!rc)
    rc = bulk_build_result (sexp, (char **)&result);

  bulk_free_list (sexp);

  if (!rc)
    {
      pthread_cleanup_push ((void *)xfree, result);
      rc = xfer_data (ctx, (char *)result, strlen ((char *)result));
      pthread_cleanup_pop (1);
    }

  return send_error (ctx, rc);
}

static gpg_error_t
nop_command (assuan_context_t ctx, char *line)
{
  return send_error (ctx, 0);
}

static gpg_error_t
bye_notify (assuan_context_t ctx, char *line)
{
  struct client_s *cl = assuan_get_pointer (ctx);
  gpg_error_t ret = 0;

  (void)line;
  update_client_state (cl, CLIENT_STATE_DISCON);

#ifdef WITH_GNUTLS
  cl->disco = 1;
  if (cl->thd->remote)
    {
      int rc;

      do
	{
	  struct timeval tv = { 0, 50000 };

	  rc = gnutls_bye (cl->thd->tls->ses, GNUTLS_SHUT_WR);
	  if (rc == GNUTLS_E_AGAIN)
	    select (0, NULL, NULL, NULL, &tv);
	}
      while (rc == GNUTLS_E_AGAIN);
    }
#endif

  /* This will let assuan_process_next() return. */
  if (fcntl (cl->thd->fd, F_SETFL, O_NONBLOCK) == -1)
    {
      cl->last_rc = gpg_error_from_errno (errno);
      ret = cl->last_rc;
    }

  cl->last_rc = 0;		// BYE command result
  return ret;
}

static gpg_error_t
reset_notify (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);

  (void)line;
  if (client)
    reset_client (client);

  return 0;
}

/*
 * This is called before every Assuan command.
 */
static gpg_error_t
command_startup (assuan_context_t ctx, const char *name)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;
  struct command_table_s *cmd = NULL;

  log_write2 ("command='%s'", name);
  client->last_rc = client->opts = 0;

  for (int i = 0; command_table[i]; i++)
    {
      if (!strcasecmp (name, command_table[i]->name))
	{
	  if (command_table[i]->ignore_startup)
            goto send_state;

	  cmd = command_table[i];
	  break;
	}
    }

  if (!cmd)
    return GPG_ERR_UNKNOWN_COMMAND;

  client->last_rc = rc = gpg_error (file_modified (client, cmd));

send_state:
  if (!client->bulk_p && !rc)
    update_client_state (client, CLIENT_STATE_COMMAND);

  return rc;
}

/*
 * This is called after every Assuan command.
 */
static void
command_finalize (assuan_context_t ctx, gpg_error_t rc)
{
  struct client_s *client = assuan_get_pointer (ctx);

  if (!(client->flags & FLAG_LOCK_CMD))
    unlock_file_mutex (client, 0);

  unlock_flock (&client->flock_fd);
  log_write2 (_("command completed: rc=%u"),  rc ? rc : client->last_rc);

#ifdef WITH_GNUTLS
  client->thd->buffer_timeout = client->thd->last_buffer_size = 0;
#endif

  if (!client->bulk_p && client->thd->state != CLIENT_STATE_DISCON)
    update_client_state (client, CLIENT_STATE_IDLE);

  client->bulk_p = NULL;
}

static gpg_error_t
parse_help_opt_html (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_HTML;
  return 0;
}

static gpg_error_t
help_command (assuan_context_t ctx, char *line)
{
  gpg_error_t rc;
  int i;
  struct client_s *client = assuan_get_pointer (ctx);
  struct argv_s *args[] = {
    &(struct argv_s) {"html", OPTION_TYPE_NOARG, parse_help_opt_html},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);

  if (!line || !*line)
    {
      char *tmp;
      char *help = str_dup (_("Usage: HELP [--html] [<COMMAND>]\n"
			      "For commands that take an element path as an argument, each element is "
			      "separated with an ASCII @key{TAB} character (ASCII 0x09). Not all help text is available here. For complete documentation refer to the texinfo or HTML manual."
			      "@*@*COMMANDS:"));

      for (i = 0; command_table[i]; i++)
	{
	  if (!command_table[i]->help)
	    continue;

          /* @npxref{} won't put a "See " or "see " in front of the command.
           * It's not a texinfo command but needed for --html. */
	  tmp = str_asprintf ("%s %s%s%s", help,
                              client->opts & OPT_HTML ? "@npxref{" : "",
                              command_table[i]->name,
                              client->opts & OPT_HTML ? "}" : "");
	  xfree (help);
	  help = tmp;
	}

      tmp = strip_texi_and_wrap (help, client->opts & OPT_HTML);
      xfree (help);
      pthread_cleanup_push ((void *)xfree, tmp);
      rc = xfer_data (ctx, tmp, strlen (tmp));
      pthread_cleanup_pop (1);
      return send_error (ctx, rc);
    }

  for (i = 0; command_table[i]; i++)
    {
      if (!strcasecmp (line, command_table[i]->name))
	{
	  char *help, *tmp;

	  if (!command_table[i]->help)
	    break;

	  help = strip_texi_and_wrap (command_table[i]->help,
                                      client->opts & OPT_HTML);
	  tmp = str_asprintf ("%s%s",
                              (client->opts & OPT_HTML) ? "" : _("Usage: "),
                              help);
	  xfree (help);
          pthread_cleanup_push ((void *)xfree, tmp);
	  rc = xfer_data (ctx, tmp, strlen (tmp));
          pthread_cleanup_pop (1);
	  return send_error (ctx, rc);
	}
    }

  return send_error (ctx, GPG_ERR_INV_NAME);
}

static void
new_command (const char *name, int inquire, int ignore, int unlock,
             int flock_type, gpg_error_t (*handler) (assuan_context_t, char *),
	     const char *help)
{
  int i = 0;
  struct command_table_s **tmp;

  if (command_table)
    for (i = 0; command_table[i]; i++);

  tmp = xrealloc (command_table, (i + 2) * sizeof (struct command_table_s *));
  assert (tmp);
  command_table = tmp;
  command_table[i] = xcalloc (1, sizeof (struct command_table_s));
  command_table[i]->name = name;
  command_table[i]->inquire = inquire;
  command_table[i]->handler = handler;
  command_table[i]->ignore_startup = ignore;
  command_table[i]->unlock = unlock;
  command_table[i]->flock_type = flock_type;
  command_table[i++]->help = help;
  command_table[i] = NULL;
}

void
deinit_commands ()
{
  int i;

  for (i = 0; command_table[i]; i++)
    xfree (command_table[i]);

  xfree (command_table);
}

static int
sort_commands (const void *arg1, const void *arg2)
{
  struct command_table_s *const *a = arg1;
  struct command_table_s *const *b = arg2;

  if (!*a || !*b)
    return 0;
  else if (*a && !*b)
    return 1;
  else if (!*a && *b)
    return -1;

  return strcmp ((*a)->name, (*b)->name);
}

static gpg_error_t
passwd_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;

  (void)line;
  rc = peer_is_invoker (client);
  if (rc)
    return send_error (ctx, (rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc));

  if (client->flags & FLAG_NEW)
    return send_error (ctx, GPG_ERR_INV_STATE);

  client->crypto->keyfile = config_get_string (client->filename,
                                               "passphrase_file");
  rc = crypto_init_ctx (client->crypto, client->flags & FLAG_NO_PINENTRY,
                        client->crypto->keyfile);
  if (!rc)
    rc = crypto_passwd (client, client->crypto);

  crypto_free_non_keys (client->crypto);
  return send_error (ctx, rc);
}

static gpg_error_t
parse_opt_data (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_DATA;
  return 0;
}

static gpg_error_t
send_client_list (assuan_context_t ctx)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;

  if (client->opts & OPT_VERBOSE)
    {
      unsigned i, t;
      char **list = NULL;
      char *line;

      MUTEX_LOCK (&cn_mutex);
      pthread_cleanup_push ((void *)release_mutex_cb, &cn_mutex);
      t = slist_length (cn_thread_list);

      for (i = 0; i < t; i++)
        {
          struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);
          char *tmp;

          if (thd->state == CLIENT_STATE_UNKNOWN)
            continue;

          tmp = build_client_info_line (thd, 0);
          if (tmp)
            {
              char **l = strv_cat (list, tmp);
              if (!l)
                rc = GPG_ERR_ENOMEM;
              else
                list = l;
            }
          else
            rc = GPG_ERR_ENOMEM;

          if (rc)
            {
              strv_free (list);
              break;
            }
        }

      pthread_cleanup_pop (1);
      if (rc)
        return rc;

      line = strv_join ("\n", list);
      strv_free (list);
      pthread_cleanup_push ((void *)xfree, line);
      rc = xfer_data (ctx, line, strlen (line));
      pthread_cleanup_pop (1);
      return rc;
    }

  if (client->opts & OPT_DATA)
    {
      char buf[ASSUAN_LINELENGTH];

      MUTEX_LOCK (&cn_mutex);
      snprintf (buf, sizeof (buf), "%u", slist_length (cn_thread_list));
      MUTEX_UNLOCK (&cn_mutex);
      rc = xfer_data (ctx, buf, strlen (buf));
    }
  else
    rc = send_status (ctx, STATUS_CLIENTS, NULL);

  return rc;
}

static gpg_error_t
getinfo_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  char buf[ASSUAN_LINELENGTH];
  struct argv_s *args[] = {
    &(struct argv_s) {"data", OPTION_TYPE_NOARG, parse_opt_data},
    &(struct argv_s) {"verbose", OPTION_TYPE_NOARG, parse_opt_verbose},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);

  if (!line || !*line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  if (!strcasecmp (line, "clients"))
    {
      rc = send_client_list (ctx);
    }
  else if (!strcasecmp (line, "cache"))
    {
      if (client->opts & OPT_DATA)
	{
	  snprintf (buf, sizeof (buf), "%u", cache_file_count ());
	  rc = xfer_data (ctx, buf, strlen (buf));
	}
      else
	rc = send_status (ctx, STATUS_CACHE, NULL);
    }
  else if (!strcasecmp (line, "pid"))
    {
      long pid = getpid ();

      snprintf (buf, sizeof (buf), "%li", pid);
      rc = xfer_data (ctx, buf, strlen (buf));
    }
  else if (!strcasecmp (line, "version"))
    {
      char *tmp = str_asprintf ("0x%06x %s", VERSION_HEX,
#ifdef WITH_GNUTLS
				"GNUTLS "
#endif
#ifdef WITH_LIBACL
				"ACL "
#endif
				"");
      pthread_cleanup_push (xfree, tmp);
      rc = xfer_data (ctx, tmp, strlen (tmp));
      pthread_cleanup_pop (1);
    }
  else if (!strcasecmp (line, "last_error"))
    {
      if (client->last_error)
	rc = xfer_data (ctx, client->last_error, strlen (client->last_error));
      else
	rc = GPG_ERR_NO_DATA;
    }
  else if (!strcasecmp (line, "user"))
    {
      char *user = NULL;

#ifdef WITH_GNUTLS
      if (client->thd->remote)
        user = str_asprintf ("#%s", client->thd->tls->fp);
      else
        user = get_username (client->thd->peer->uid);
#else
      user = get_username (client->thd->peer->uid);
#endif
      if (user)
        {
          pthread_cleanup_push ((void *)xfree, user);
          rc = xfer_data (ctx, user, strlen (user));
          pthread_cleanup_pop (1);
        }
      else
        rc = GPG_ERR_NO_DATA;
    }
  else
    rc = gpg_error (GPG_ERR_SYNTAX);

  return send_error (ctx, rc);
}

static gpg_error_t
parse_opt_secret (void *data, void *value)
{
  struct client_s *client = data;

  (void) value;
  client->opts |= OPT_SECRET;
  return 0;
}

static gpg_error_t
parse_keyinfo_opt_learn (void *data, void *value)
{
  struct client_s *client = data;

  (void)value;
  client->opts |= OPT_KEYINFO_LEARN;
  return 0;
}

static gpg_error_t
keyinfo_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc = 0;
  char **keys = NULL, **p = NULL;
  int sym = 0;
  struct argv_s *args[] = {
    &(struct argv_s) {"learn", OPTION_TYPE_NOARG, parse_keyinfo_opt_learn},
    NULL
  };

  rc = parse_options (&line, args, client, 0);
  if (rc)
    return send_error (ctx, rc);

  if (line && *line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  if (!(client->flags & FLAG_OPEN))
    return send_error (ctx, GPG_ERR_INV_STATE);

  if (client->opts & OPT_KEYINFO_LEARN)
    {
      rc = cache_agent_command ("LEARN");
      return send_error (ctx, rc);
    }

  if (client->flags & FLAG_NEW)
    return send_error (ctx, GPG_ERR_NO_DATA);

  rc = lock_flock (ctx, client->filename, FLOCK_TYPE_SH, &client->flock_fd);
  if (rc)
    return send_error (ctx, rc);

  rc = crypto_is_symmetric (client->filename);
  unlock_flock (&client->flock_fd);
  if (!rc)
    sym = 1;
  else if (rc != GPG_ERR_BAD_DATA)
    return send_error (ctx, rc);

  rc = 0;
  if (!sym)
    {
      p = strv_catv (keys, client->crypto->pubkey);
      if (!p)
        rc = GPG_ERR_ENOMEM;
      else
        keys = p;
    }

  if (!rc && client->crypto->sigkey)
    {
      if (!strv_printf (&keys, "S%s", client->crypto->sigkey))
        {
          strv_free (keys);
          return send_error (ctx, GPG_ERR_ENOMEM);
        }
    }

  if (!rc)
    {
      if (keys)
        {
          line = strv_join ("\n", keys);
          strv_free (keys);
          pthread_cleanup_push ((void *)xfree, line);
          if (line)
            rc = xfer_data (ctx, line, strlen (line));
          else
            rc = GPG_ERR_ENOMEM;

          pthread_cleanup_pop (1);
        }
      else
        rc = GPG_ERR_NO_DATA;
    }
  else
    strv_free (keys);

  return send_error (ctx, rc);
}

static gpg_error_t
deletekey_command (assuan_context_t ctx, char *line)
{
  gpg_error_t rc;
  struct client_s *client = assuan_get_pointer (ctx);
  struct crypto_s *crypto = NULL;
  char *keys[] = { NULL, NULL };
  gpgme_key_t *result;

  rc = peer_is_invoker (client);
  if (rc)
    return send_error (ctx, (rc == GPG_ERR_EACCES ? GPG_ERR_FORBIDDEN : rc));

  if (!*line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  rc = crypto_keyid_to_16b_once (line);
  if (rc)
    return send_error (ctx, rc);

  rc = crypto_init (&crypto, client->ctx, client->filename,
                    client->flags & FLAG_NO_PINENTRY, NULL);
  if (rc)
    return send_error (ctx, rc);

  pthread_cleanup_push ((void *)crypto_free, crypto);
  keys[0] = line;
  rc = crypto_list_keys (crypto, keys, 1, &result);
  if (!rc)
    {
      pthread_cleanup_push ((void *)crypto_free_key_list, result);
      rc = crypto_delete_key (client, crypto, *result, 1);
      pthread_cleanup_pop (1);
    }

  pthread_cleanup_pop (1);
  return send_error (ctx, rc);
}

static gpg_error_t
kill_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  gpg_error_t rc;
  unsigned i, t;

  if (!line || !*line)
    return send_error (ctx, GPG_ERR_SYNTAX);

  MUTEX_LOCK (&cn_mutex);
  pthread_cleanup_push ((void *)release_mutex_cb, &cn_mutex);
  t = slist_length (cn_thread_list);
  rc = GPG_ERR_ESRCH;

  for (i = 0; i < t; i++)
    {
      struct client_thread_s *thd = slist_nth_data (cn_thread_list, i);
      char *tmp = str_asprintf ("%p", thd->tid);

      if (strcmp (line, tmp))
        {
          xfree (tmp);
          continue;
        }

      xfree (tmp);
      rc = peer_is_invoker (client);
      if (!rc)
        {
#ifdef HAVE_PTHREAD_CANCEL
          pthread_cancel (thd->tid);
#else
          pthread_kill (thd->tid, SIGUSR2);
#endif
          break;
        }
      else if (rc == GPG_ERR_EACCES)
        rc = GPG_ERR_FORBIDDEN;
      else if (rc)
        break;

      if (config_get_boolean ("global", "strict_kill"))
        break;

#ifdef WITH_GNUTLS
      if (client->thd->remote && thd->remote)
        {
          if (!thd->tls || !thd->tls->fp)
            {
              rc = GPG_ERR_INV_STATE;
              break;
            }
          if (!strcmp (client->thd->tls->fp, thd->tls->fp))
            {
#ifdef HAVE_PTHREAD_CANCEL
              pthread_cancel (thd->tid);
#else
              pthread_kill (thd->tid, SIGUSR2);
#endif
              rc = 0;
              break;
            }
        }
      else if (!client->thd->remote && !thd->remote)
#endif
        {
          if (client->thd->peer->uid == thd->peer->uid)
            {
#ifdef HAVE_PTHREAD_CANCEL
              pthread_cancel (thd->tid);
#else
              pthread_kill (thd->tid, SIGUSR2);
#endif
              rc = 0;
            }
        }
      break;
    }

  pthread_cleanup_pop (1);
  return send_error (ctx, rc);
}

static gpg_error_t
listkeys_command (assuan_context_t ctx, char *line)
{
  struct client_s *client = assuan_get_pointer (ctx);
  struct crypto_s *crypto = NULL;
  char **pattern = NULL;
  gpgme_key_t *keys = NULL;
  char **result = NULL;
  gpg_error_t rc;
  struct argv_s *args[] = {
    &(struct argv_s) {"secret-only", OPTION_TYPE_NOARG, parse_opt_secret},
    NULL
  };

  rc = parse_options (&line, args, client, 1);
  if (rc)
    return send_error (ctx, rc);

  rc = crypto_init (&crypto, client->ctx, client->filename,
                    client->flags & FLAG_NO_PINENTRY, NULL);
  if (rc)
    return send_error (ctx, rc);

  pthread_cleanup_push ((void *)crypto_free, crypto);
  if (line)
    pattern = str_split (line, ",", 0);
  pthread_cleanup_push ((void *)(void *)strv_free, pattern);
  rc = crypto_list_keys (crypto, pattern, client->opts & OPT_SECRET,
                         &keys);
  pthread_cleanup_pop (1);
  pthread_cleanup_pop (1);
  if (!rc)
    {
      int i;

      for (i = 0; keys[i]; i++)
        {
          char *p = crypto_key_info (keys[i]);
          char **r;

          if (!p)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          r = strv_cat (result, p);
          if (!r)
            {
              rc = GPG_ERR_ENOMEM;
              break;
            }

          result = r;
        }

      if (!i)
        rc = GPG_ERR_NO_DATA;

      crypto_free_key_list (keys);
    }

  if (!rc)
    {
      line = strv_join ("\n", result);
      strv_free (result);
      pthread_cleanup_push ((void *)xfree, line);
      if (!line)
        rc = GPG_ERR_ENOMEM;
      else
        rc = xfer_data (ctx, line, strlen (line));

      pthread_cleanup_pop (1);
    }
  else
    strv_free (result);

  if (gpg_err_code (rc) == GPG_ERR_NO_PUBKEY
      || gpg_err_code (rc) == GPG_ERR_NO_SECKEY)
    rc = GPG_ERR_NO_DATA;

  return send_error (ctx, rc);
}

void
init_commands ()
{
  /* !BEGIN-HELP-TEXT!
   *
   * This comment is used as a marker to generate the offline documentation
   * found in doc/pwmd.info. The indentation needs to be kept for the awk
   * script to determine where commands begin and end.
   */
    new_command("HELP", 0, 1, 1, 0, help_command, _(
"HELP [--html] [<COMMAND>]\n" /* Showing available commands. */
"Show available commands or command specific help text."
"@*@*"
"The @option{--html} option will output the help text in HTML format."
));

    new_command("DELETEKEY", 0, 1, 1, FLOCK_TYPE_SH|FLOCK_TYPE_KEEP, deletekey_command, _(
"DELETEKEY <keyid>\n" /* Deleting a key from the key ring. */
"Deletes the public and secret key associated with key @var{keyid} from the "
"keyring. The @var{keyid} must be one associated with the currently opened "
"data file. "
"Note that no confirmation occurs. Also note that when the key is deleted, "
"the current or other data files using this key will no longer be able to be "
"opened."
));

    new_command("KILL", 0, 1, 0, 0, kill_command, _(
"KILL <thread_id>\n" /* Terminating another client. */
"Terminates the client identified by @var{thread_id} and releases any file "
"lock or other resources it has held. See @code{GETINFO} (@pxref{GETINFO}) "
"for details about listing connected clients. An @code{invoking_user} "
"(@pxref{Configuration}) may kill any client while others may only kill "
"clients of the same @code{UID} or @abbr{TLS} fingerprint."
));

    new_command("LISTKEYS", 0, 1, 0, 0, listkeys_command, _(
"LISTKEYS [--secret-only] [pattern[,<pattern>]]\n" /* Listing keys in the key ring. */
"Returns a new line separated list of key information matching a comma "
"separated list of @var{pattern}'s. When option @option{--secret-only} is "
"specified, only keys matching @var{pattern} that also have a secret key "
"available will be returned."
));

    new_command("KEYINFO", 0, 1, 0, 0, keyinfo_command, _(
"KEYINFO [--learn]\n" /* Showing keys used for the current data file. */
"Returns a new line separated list of key ID's that the currently opened "
"data file has recipients and signers for. If the key is a signing key it "
"will be prefixed with an @code{S}. If the file is a new one, or has no "
"signers in the case of being symmetrically encrypted, the error code "
"@code{GPG_ERR_NO_DATA} is returned."
"@*@*"
"When the @option{--learn} option is passed, keys on a smartcard will be "
"imported."
));

    new_command("GETINFO", 0, 1, 1, 0, getinfo_command, _(
"GETINFO [--data] [--verbose] CACHE | CLIENTS | PID | USER | LAST_ERROR | VERSION\n" /* Obtaining server and client information. */
"Get server and other information. The information is returned via a status "
"message (@pxref{Status Messages}) unless otherwise noted or @option{--data} "
"is specified."
"@*@*"
"@var{CACHE} returns the number of cached documents."
"@*@*"
"@var{CLIENTS} returns the number of "
"connected clients via a status message or a list of connected clients when "
"the @option{--verbose} parameter is used (implies @option{--data}). A "
"verbose line of a client list contains "
"space delimited "
"fields: the thread ID, client name, opened file (@code{/} if none opened), "
"IP address if remote, file lock status, whether the current client is self "
"or not, client state (see below), "
"user ID or TLS fingerprint of the connected client, username if the "
"client is a local one else @code{-}, and finally the time stamp of when the "
"client connected."
"@*@*"
"Client state @code{0} is an unknown client state, state @code{1} indicates "
"the client has connected but hasn't completed initializing, state @code{2} "
"indicates that the client is idle, state @code{3} means the "
"client is in a command and state @code{4} means the client is disconnecting. "
"@*@*"
"@var{PID} returns the process ID number of the server via a data response."
"@*@*"
"@var{VERSION} returns the server version number and compile-time features "
"via a data response with each being space delimited."
"@*@*"
"@var{LAST_ERROR} returns a detailed description of the last failed command "
"via a data response, when available."
"@*@*"
"@var{USER} returns the username or @abbr{TLS} hash of the connected client "
"via a data response."
));

    new_command("PASSWD", 1, 0, 0, FLOCK_TYPE_EX|FLOCK_TYPE_KEEP, passwd_command, _(
"PASSWD\n" /* Changing the passphrase for a key. */
"Changes the passphrase of the secret key required to open the current "
"data file. If the data file is symmetrically encrypted the error "
"@code{GPG_ERR_NOT_SUPPORTED} is returned. When symmetrically encrypted "
"the @code{SAVE} command (@pxref{SAVE}) should be used instead to prevent "
"this command saving any unwanted changes to the @abbr{XML} document."
"@*@*"
"Note that when the current data file has been either encrypted or signed "
"with a key stored on a smartcard this command will return an error. In this "
"case you should instead use @command{gpg --card-edit} to change the "
"pin of the smartcard or @command{gpg --edit-key} to change the passphrase "
"of the key used to sign or encrypt the data file."
"@*@*"
"This command is not available to non-invoking clients "
"(@pxref{Access Control})."
));

    new_command("OPEN", 1, 1, 1, FLOCK_TYPE_SH|FLOCK_TYPE_KEEP, open_command, _(
"OPEN [--lock] <filename>\n" /* Opening a data file. */
"Opens @var{filename}. When the @var{filename} is not found on the "
"file-system then a new in-memory document will be created. If the file is "
"found, it is looked for in the file cache and when found no passphrase will "
"be required to open it. When not cached, @cite{pinentry(1)} will be used to "
"retrieve the passphrase for decryption unless @option{disable-pinentry} "
"(@pxref{OPTION}) was specified in which case @command{pwmd} will "
"@emph{INQUIRE} the client for the passphrase. Note than when configuration "
"option @option{strict_open} is enabled and the client is not an "
"@option{invoking_user}, an error will be returned when the data file does "
"not exist."
"@*@*"
"When the @option{--lock} option is passed then the file mutex will be "
"locked as if the @code{LOCK} command (@pxref{LOCK}) had been sent after the "
"file had been opened."
));

    new_command("GENKEY", 1, 1, 1, 0, genkey_command, _(
"GENKEY --subkey-of=fpr | --userid=\"str\" [--no-expire | --expire=N] [--algo=\"str\"] [--no-passphrase] [--usage=\"default|sign|encrypt\"]\n" /* Generating a new key. */
"Generates a new key based on option arguments. One of "
"@option{--subkey-of} or @option{--userid} is "
"required. The @option{--subkey-of} option will generate a subkey for the key "
"of the specified fingerprint."
"@*@*"
"Note that this may take a long time to complete especially if you do not "
"have a hardware RNG. There are software RNG's that will speed this up "
"but are also not as secure."
));

    new_command("SAVE", 1, 0, 0, FLOCK_TYPE_EX|FLOCK_TYPE_KEEP, save_command, _(
"SAVE [--sign-keyid=[<fpr>]] [--symmetric | --keyid=<fpr>[,..] | --inquire-keyid]\n" /* Saving document changes to disk. */
"Writes the in-memory @abbr{XML} document to disk. The file written to is the "
"file that was opened when using the @code{OPEN} command (@pxref{OPEN})."
"@*@*"
"If the file is a new one one of @option{--symmetric}, @option{--keyid} or"
"@option{--inquire-keyid} is required. When not @option{--symmetric} the "
"option @option{--sign-keyid} is also required but optional otherwise."
"@*@*"
"You can encrypt the data file to a recipient other than the one that it "
"was originally encrypted with by passing the @option{--keyid} or "
"@option{--inquire-keyid} option with a comma separated list of "
"public encryption key fingerprints as its argument. Use the "
"@command{LISTKEYS} command (@pxref{LISTKEYS}) to show key information by "
"pattern. The @option{--sign-keyid} option may also be used to sign the data "
"file with an alternate key by specifying the fingerprint of a signing key. "
"Only one signing key is supported unlike the @option{--keyid} option. "
"A passphrase to decrypt the data file "
"will be required when one or more of the original encryption keys or signing "
"key are not found in either of these two options' arguments or when the data "
"file is symmetrically encrypted regardless of the @code{require_save_key} "
"configuration parameter. The original encryption keys and signing key will be "
"used when neither of these options are specified."
"@*@*"
"The @option{--keyid} and @option{--sign-keyid} options are not available "
"to non-invoking clients "
"(@pxref{Access Control}) when the recipients or signer do not match those "
"that were used when the file was @code{OPEN}'ed."
"@*@*"
"The @option{--symmetric} option specifies that a new data file be "
"conventionally encrypted. These types of data files do not use a recipient "
"public key but may optionally be signed by using the @option{--sign-keyid} "
"option. To remove the signing key from a symmtrically encrypted data file, "
"leave the option value empty."
"@*@*"
"Note that you cannot change encryption schemes once a data file has been "
"saved."
));

    new_command("ISCACHED", 0, 1, 0, 0, iscached_command, _(
"ISCACHED [--lock] [--agent [--sign]] <filename>\n" /* Testing cache status. */
"Determines the file cache status of the specified @var{filename}. "
"The default is to test whether the filename is cached in memory. Passing "
"option @option{--agent} will test the @command{gpg-agent} cache for at most "
"one cached key used for opening the data file (@pxref{OPEN}). To test if "
"a signing key is cached, pass @option{--sign} along with @option{--agent}. "
"Both the @option{--agent} and @option{--sign} options require an opened data "
"file."
"@*@*"
"An @emph{OK} response is returned if the specified @var{filename} is found "
"in the cache. If not found in the cache but exists on the filesystem "
"then @code{GPG_ERR_NO_DATA} is returned. Otherwise a filesystem error is "
"returned."
"@*@*"
"The @option{--lock} option will lock the file mutex of @var{filename} when "
"the file exists; it does not need to be opened nor cached. The lock will be "
"released when the client exits or sends the @code{UNLOCK} command "
"(@pxref{UNLOCK}). When this option is passed the current data file is closed."
));

    new_command("CLEARCACHE", 0, 1, 1, 0, clearcache_command, _(
"CLEARCACHE [<filename>]\n" /* Removing a cache entry. */
"Clears a file cache entry for all or the specified @var{filename}. Note that "
"this will also clear any @command{gpg-agent} cached keys which may cause "
"problems if another data file shares the same keys as @var{filename}."
"@*@*"
"When clearing all cache entries a permissions test is done against the "
"current client based on the @var{allowed} configuration parameter in a "
"@var{filename} section. Both a cache entry may be cleared and an error "
"returned depending on cached data files and client permissions."
));

    new_command("CACHETIMEOUT", 0, 1, 1, 0, cachetimeout_command, _(
"CACHETIMEOUT <seconds>\n" /* Setting the cache timeout. */
"The time in @var{seconds} until the currently opened data file will be "
"removed from the cache. @code{-1} will keep the cache entry forever, "
"@code{0} will require the passphrase for each @code{OPEN} command "
"(@pxref{OPEN}) or @code{SAVE} (@pxref{SAVE}) command. @xref{Configuration}, "
"and the @code{cache_timeout} parameter."
));

    new_command("LIST", 0, 0, 1, 0, list_command, _(
"LIST [--inquire] [--recurse] [--sexp] [element[<TAB>child[..]]]\n" /* Showing document elements. */
"If no element path is given then a newline separated list of root elements "
"is returned with a data response. If given, then children of the specified "
"element path are returned."
"@*@*"
"Each element path "
"returned will have zero or more flags appened to it. These flags are "
"delimited from the element path by a single space character. A flag itself "
"is a single character. Flag @code{P} indicates that access to the element "
"is denied. Flag @code{+} indicates that there are child nodes of "
"the current element path. Flag @code{E} indicates that an element of the "
"element path contained in a @var{target} attribute could not be found. Flag "
"@code{O} indicates that a @var{target} attribute recursion limit was reached "
"(@pxref{Configuration}). Flag @code{T}, followed by a single space character, "
"then an element path, is the element path of the @var{target} attribute "
"contained in the current element."
"@*@*"
"When a specified element path contains an error either from the final "
"element in the path or any previous element, the path is still shown but "
"will contain the error flag for the element with the error. Determining "
"the actual element which contains the error is up to the client. This can be "
"done by traversing the final element up to parent elements that contain the "
"same error flag."
"@*@*"
"The option @option{--recurse} may be used to list the entire element tree "
"for a specified element path or the entire tree for all root elements."
"@*@*"
"The option @option{--sexp} outputs the list in an s-expression and also "
"appends an elements' attributes and attribute values. The syntax is:\n"
"\n"
"@example\n"
"(11:list-result\n"
"  (4:path N:<path> 5:flags N:<flags>\n"
"    (5:attrs N:<name> N:<value> ...)\n"
"  )\n"
"  ...\n"
")\n"
"@end example\n"
"\n"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("REALPATH", 0, 0, 1, 0, realpath_command, _(
"REALPATH [--inquire] element[<TAB>child[..]]\n" /* Resolving an element. */
"Resolves all @code{target} attributes of the specified element path and "
"returns the result with a data response. @xref{Target Attribute}, for details."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("STORE", 0, 0, 1, 0, store_command, _(
"STORE [--no-inherit-acl] element[<TAB>child[..]]<TAB>[content]\n" /* Modifying the content of an element. */
"This command uses a server @emph{INQUIRE} to retrieve data from the client."
"@*@*"
"Creates a new element path or modifies the @var{content} of an existing "
"element. If only a single element is specified then a new root element is "
"created. Otherwise, elements are @key{TAB} delimited and the content will be "
"set to the final @key{TAB} delimited element. If no @var{content} is "
"specified after the final @key{TAB}, then the content of the existing "
"element will be removed; or will be empty if creating a new element."
"@*@*"
"The option @option{--no-inherit-acl} prevents a newly created element from "
"inheriting the value of the parent element @code{_acl} attribute. In either "
"case, the current user is made the owner of the newly created element(s)."
"@*@*"
"The only restriction of an element name is that it not contain whitespace "
"characters. There is no other whitespace between the @key{TAB} delimited "
"elements. It is recommended that the content of an element be base64 encoded "
"when it contains control or @key{TAB} characters to prevent @abbr{XML} "
"parsing and @command{pwmd} syntax errors."
));

    new_command("RENAME", 0, 0, 1, 0, rename_command, _(
"RENAME [--inquire] element[<TAB>child[..]] <value>\n" /* Renaming an element. */
"Renames the specified @var{element} to the new @var{value}. If an element of "
"the same name as the @var{value} already exists it will be overwritten."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("COPY", 0, 0, 1, 0, copy_command, _(
"COPY [--inquire] source[<TAB>child[..]] dest[<TAB>child[..]]\n" /* Copying an element. */
"Copies the entire element tree starting from the child node of the source "
"element, to the destination element path. If the destination element path "
"does not exist then it will be created; otherwise it is overwritten."
"@*@*"
"Note that attributes from the source element are merged into the "
"destination element when the destination element path exists. When an "
"attribute of the same name exists in both the source and destination "
"elements then the destination attribute will be updated to the source "
"attribute value."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("MOVE", 0, 0, 1, 0, move_command, _(
"MOVE [--inquire] source[<TAB>child[..]] [dest[<TAB>child[..]]]\n" /* Moving an element. */
"Moves the source element path to the destination element path. If the "
"destination is not specified then it will be moved to the root node of the "
"document. If the destination is specified and exists then it will be "
"overwritten; otherwise non-existing elements of the destination element "
"path will be created."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("DELETE", 0, 0, 1, 0, delete_command, _(
"DELETE [--inquire] element[<TAB>child[..]]\n" /* Deleting an element. */
"Removes the specified element path and all of its children. This may break "
"an element with a @code{target} attribute (@pxref{Target Attribute}) that "
"refers to this element or any of its children."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("GET", 0, 0, 1, 0, get_command, _(
"GET [--inquire] element[<TAB>child[..]]\n" /* Getting the content of an element. */
"Retrieves the content of the specified element. The content is returned "
"with a data response."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("ATTR", 0, 0, 1, 0, attr_command, _(
"ATTR [--inquire] SET|GET|DELETE|LIST [<attribute>] element[<TAB>child[..]] ..\n" /* Modifying element attributes. */
"@table @asis\n"
"@item ATTR SET attribute element[<TAB>child[..]] [value]\n"
"    Stores or updates an @var{attribute} name and optional @var{value} of an "
"element. When no @var{value} is specified any existing value will be removed."
"@*@*"
"@item ATTR DELETE attribute element[<TAB>child[..]]\n"
"    Removes an attribute from an element. If @var{attribute} is @code{_name} "
"or @code{target} an error is returned. Use the @command{DELETE} command "
"(@pxref{DELETE}) instead."
"@*@*"
"@item ATTR LIST element[<TAB>child[..]]\n"
"    Retrieves a newline separated list of attributes names and values "
"from the specified element. Each attribute name and value is space delimited."
"@*@*"
"@item ATTR GET attribute element[<TAB>child[..]]\n"
"    Retrieves the value of an @var{attribute} from an element."
"@end table\n"
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
"@*@*"
"@xref{Target Attribute}, for details about this special attribute and also "
"@pxref{Other Attributes} for other attributes that are handled specially "
"by @command{pwmd}."
));

    new_command("XPATH", 0, 0, 1, 0, xpath_command, _(
"XPATH [--inquire] <expression>[<TAB>[value]]\n" /* Modifying more than one element. */
"Evaluates an XPath @var{expression}. If no @var{value} argument is "
"specified it is assumed the expression is a request to return a result. "
"Otherwise, the result is set to the @var{value} argument and the document is "
"updated. If there is no @var{value} after the @key{TAB} character, the value "
"is assumed to be empty and the document is updated. For example:"
"@sp 1\n"
"@example\n"
"XPATH //element[@@_name='password']@key{TAB}\n"
"@end example\n"
"@sp 1\n"
"would clear the content of all @var{password} elements in the data file "
"while leaving off the trailing @key{TAB} would return all @var{password} "
"elements in @abbr{XML} format."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
"@*@*"
"See @url{https://www.w3schools.com/xml/xpath_intro.asp} for @abbr{XPATH} "
"expression syntax."
));

    new_command("XPATHATTR", 0, 0, 1, 0, xpathattr_command, _(
"XPATHATTR [--inquire] SET|DELETE <name> <expression>[<TAB>[<value>]]\n" /* Modifying more than one element's attributes. */
"Like the @code{XPATH} command (@pxref{XPATH}) but operates on element "
"attributes and does not return a result. For the @var{SET} operation the "
"@var{value} is optional but the field is required. If not specified then "
"the attribute value will be empty. For example:"
"@sp 1\n"
"@example\n"
"XPATHATTR SET password //element[@@_name='password']@key{TAB}\n"
"@end example\n"
"@sp 1\n"
"would create a @var{password} attribute for each @var{password} element "
"found in the document. The attribute value will be empty but still exist."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
"@*@*"
"See @url{https://www.w3schools.com/xml/xpath_intro.asp} for @abbr{XPATH} "
"expression syntax."
));

    new_command("IMPORT", 0, 0, 1, 0, import_command, _(
"IMPORT [--root=element[<TAB>child[..]]]\n" /* Creating elements from XML. */
"This command uses a server @emph{INQUIRE} to retrieve data from the client."
"@*@*"
"Like the @code{STORE} command (@pxref{STORE}), but the @var{content} "
"argument is raw @abbr{XML} data. The content is created as a child of "
"the element path specified with the @option{--root} option or at the "
"document root when not specified. Existing elements of the same name will "
"be overwritten."
"@*@*"
"The content must begin with an @abbr{XML} element node. @xref{Introduction}, "
"for details."
));

    new_command("DUMP", 0, 1, 1, 0, dump_command, _(
"DUMP\n" /* Showing the XML document. */
"Shows the in memory @abbr{XML} document with indenting. @xref{XPATH}, for "
"dumping a specific node."
));

    new_command("LOCK", 0, 0, 0, 0, lock_command, _(
"LOCK\n" /* Locking the current data file. */
"Locks the mutex associated with the opened file. This prevents other clients "
"from sending commands to the same opened file until the client "
"that sent this command either disconnects or sends the @code{UNLOCK} "
"command. @xref{UNLOCK}."
));

    new_command("UNLOCK", 0, 1, 0, 0, unlock_command, _(
"UNLOCK\n" /* Removing a data file lock. */
"Unlocks the file mutex which was locked with the @code{LOCK} command or "
"a commands' @option{--lock} option (@pxref{LOCK}, @pxref{OPEN}, "
"@pxref{ISCACHED})."
));

    new_command("GETCONFIG", 0, 1, 1, 0, getconfig_command, _(
"GETCONFIG [filename] <parameter>\n" /* Obtaining a configuration value. */
"Returns the value of a @command{pwmd} configuration @var{parameter} with a "
"data response. If no file has been opened then the value for @var{filename} "
"or the default from the @var{global} section will be returned. If a file "
"has been opened and no @var{filename} is specified, the value previously "
"set with the @code{OPTION} command (@pxref{OPTION}) will be returned."
));

    new_command("OPTION", 0, 1, 1, 0, option_command, _(
"OPTION <NAME>=[<VALUE>]\n" /* Setting various client parameters. */
"Sets a client option @var{name} to @var{value}. The value for an option is "
"kept for the duration of the connection with the exception of the "
"@command{pinentry} options which are defaults for all future connections "
"(@pxref{Pinentry}). When @var{value} is empty the option is unset."
"@*@*"
"@table @asis\n"
"@item DISABLE-PINENTRY\n"
"Disable use of @command{pinentry} for passphrase retrieval. When @code{1}, a "
"server inquire is sent to the client to obtain the passphrase. This option "
"may be set as needed before the @code{OPEN} (@pxref{OPEN}), @code{PASSWD} "
"(@pxref{PASSWD}) and @code{SAVE} (@pxref{SAVE}) commands. Set to @code{0} "
"to use a @command{pinentry}."
"@*@*"
"@item DISPLAY\n"
"Set or unset the X11 display to use when prompting for a passphrase."
"@*@*"
"@item TTYNAME\n"
"Set the terminal device path to use when prompting for a passphrase."
"@*@*"
"@item TTYTYPE\n"
"Set the terminal type for use with @option{TTYNAME}."
"@*@*"
"@item NAME\n"
"Associates the thread ID of the connection with the specified textual "
"representation. Useful for debugging log messages. May not contain whitespace."
"@*@*"
"@item LOCK-TIMEOUT\n"
"When not @code{0}, the duration in tenths of a second to wait for the file "
"mutex which has been locked by another thread to be released before returning "
"an error. When @code{-1} the error will be returned immediately."
"@*@*"
"@item CLIENT-STATE\n"
"When set to @code{1} then client state status messages for other clients are "
"sent to the current client. The default is @code{0}."
"@end table\n"
));

    new_command("LS", 0, 1, 1, 0, ls_command, _(
"LS [--verbose]\n" /* Showing available data files. */
"Returns a newline separated list of data files stored in the data directory "
"@file{HOMEDIR/data} (@pxref{Invoking}) with a data response. When the "
"@var{--verbose} option is passed, the space-separated filesystem inode "
"access, modification and change times are appended to the line."
));

    new_command("BULK", 0, 1, 1, 0, bulk_command, _(
"BULK [--inquire]\n" /* Run a series of commands in sequence. */
"Parses a semi-canonical s-expression representing a series of protocol "
"commands to be run in sequence (@pxref{Bulk Commands}). Returns a canonical "
"s-expression containing each commands id, return value and result data "
"(if any)."
"@*@*"
"When the @option{--inquire} option is passed then all remaining non-option "
"arguments are retrieved via a server @emph{INQUIRE}."
));

    new_command("RESET", 0, 1, 1, 0, NULL, _(
"RESET\n" /* Resetting the client state. */
"Closes the currently opened file but keeps any previously set client options "
"(@pxref{OPTION})."
));

    new_command("NOP", 0, 1, 1, 0, nop_command, _(
"NOP\n" /* Testing the connection. */
"This command does nothing. It is useful for testing the connection for a "
"timeout condition."
));

  /* !END-HELP-TEXT! */
  new_command ("CANCEL", 0, 1, 1, 0, NULL, NULL);
  new_command ("END", 0, 1, 1, 0, NULL, NULL);
  new_command ("BYE", 0, 1, 1, 0, NULL, NULL);

  int i;
  for (i = 0; command_table[i]; i++);
  qsort (command_table, i - 1, sizeof (struct command_table_s *),
	 sort_commands);
}

gpg_error_t
register_commands (assuan_context_t ctx)
{
  int i = 0, rc;

  for (; command_table[i]; i++)
    {
      if (!command_table[i]->handler)
	continue;

      rc = assuan_register_command (ctx, command_table[i]->name,
				    command_table[i]->handler,
				    command_table[i]->help);
      if (rc)
	return rc;
    }

  rc = assuan_register_bye_notify (ctx, bye_notify);
  if (rc)
    return rc;

  rc = assuan_register_reset_notify (ctx, reset_notify);
  if (rc)
    return rc;

  rc = assuan_register_pre_cmd_notify (ctx, command_startup);
  if (rc)
    return rc;

  return assuan_register_post_cmd_notify (ctx, command_finalize);
}
