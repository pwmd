/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COMMON_H
#define COMMON_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include "pwmd-error.h"
#include <assuan.h>
#include <libxml/tree.h>
#include <libxml/xmlerror.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#include <pwd.h>
#include <time.h>

#ifdef ENABLE_NLS
#ifdef HAVE_LOCALE_H
#include <locale.h>
#endif
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>
#ifndef LINE_MAX
#ifdef _POSIX2_LINE_MAX
#define LINE_MAX _POSIX2_LINE_MAX
#else
#define LINE_MAX 2048
#endif
#endif
#endif

#ifndef _
#include "gettext.h"
#define _(msgid) gettext(msgid)
#endif

#ifdef WITH_GNUTLS
#include "tls.h"
#endif
#include "status.h"
#include "bulk.h"

#define CLIENT_STATE_UNKNOWN	0
#define CLIENT_STATE_INIT	1
#define CLIENT_STATE_IDLE	2
#define CLIENT_STATE_COMMAND	3
#define CLIENT_STATE_DISCON	4

struct client_thread_s
{
  pthread_t tid;
  int fd;
  char *cmdname;
  pthread_mutex_t status_mutex;
  struct status_msg_s *msg_queue;
  int status_msg_pipe[2];
  int wrote_status;
  struct client_s *cl;
  assuan_peercred_t peer;
  unsigned state;
  int eof;
  time_t conntime;
  char *name;
  int send_state;		/* OPTION to receive client state msgs. */
#ifdef WITH_GNUTLS
  int timeout;
  int buffer_timeout;
  int last_buffer_size;
  int remote;
  struct tls_s *tls;
  char *peeraddr;
#endif
};

struct client_s
{
  assuan_context_t ctx;
  int flock_fd;
  xmlDocPtr doc;
  xmlErrorPtr xml_error;
  char *filename;
  struct client_thread_s *thd;
  struct crypto_s *crypto;
  unsigned opts;
  unsigned flags;
  char *import_root;
  long lock_timeout;		/* In tenths of a second. */
  gpg_error_t last_rc;
  char *last_error;		/* ELOOP element path. */
  unsigned char *crc;		/* Of the data file. */
  int did_cow;			/* Have already done a CoW operation. */
  struct bulk_cmd_s *bulk_p;	/* For the command result set in xfer_data(). */
#ifdef WITH_GNUTLS
  int disco;			/* Rather than thread->state to avoid lock. */
#endif
};

extern pthread_key_t thread_name_key;
extern pthread_mutex_t cn_mutex;
extern struct slist_s *cn_thread_list;

void log_write (const char *fmt, ...);
int assuan_log_cb (assuan_context_t ctx, void *data, unsigned cat,
                   const char *msg);
gpg_error_t send_error (assuan_context_t ctx, gpg_error_t e);
void update_client_state (struct client_s *client, unsigned s);
gpg_error_t lock_flock (assuan_context_t ctx, const char *filename, int type,
                        int *fd);
void unlock_flock (int *fd);

#endif
