/*
    Copyright (C) 2006-2025 Ben Kibbey <bjk@luxsci.net>

    This file is part of pwmd.

    Pwmd is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    Pwmd is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Pwmd.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef RCFILE_H
#define RCFILE_H

#include <pthread.h>
#include <sys/types.h>
#include "util-slist.h"

#define INVALID_PRIORITY                "-99"

struct config_section_s
{
  char *name;
  struct slist_s *params;
};

#define INVOKING_UID	0
#define INVOKING_GID	1
#define INVOKING_TLS	2

struct invoking_user_s
{
  int type;
  int not;
  union {
      uid_t uid;
      char *id;
  };
  struct invoking_user_s *next;
};

#ifdef HAVE_MLOCKALL
extern int disable_mlock;
#endif
extern char *logfile;
extern char *rcfile;
extern char *homedir;
extern int max_recursion_depth;
extern int disable_list_and_dump;
extern struct slist_s *global_config;
extern int log_syslog;
extern pthread_mutex_t rcfile_mutex;
extern pthread_cond_t rcfile_cond;
extern pthread_t rcfile_tid;
extern struct invoking_user_s *invoking_users;
extern int log_keepopen;
extern int log_level;

void free_invoking_users (struct invoking_user_s *);
struct slist_s *config_parse (const char *filename, int reload);
void config_free (struct slist_s *config);

char *config_get_value (const char *section, const char *what);
long config_get_long (const char *section, const char *what);
int config_get_boolean (const char *section, const char *what);
int config_get_integer (const char *section, const char *what);
char *config_get_string (const char *section, const char *what);
char **config_get_list (const char *section, const char *what);
long long config_get_longlong (const char *section, const char *what);

int config_set_list_param (struct slist_s **config, const char *section,
			   const char *name, const char *value);
char **config_get_list_param (struct slist_s *config, const char *section,
			      const char *what, int *exists);
int config_set_int_param (struct slist_s **config, const char *section,
			  const char *name, const char *value);
int config_get_int_param (struct slist_s *config, const char *section,
			  const char *name, int *exists);
int config_get_bool_param (struct slist_s *config, const char *section,
			   const char *name, int *exists);
int config_set_bool_param (struct slist_s **config, const char *section,
			   const char *name, const char *value);
int config_set_long_param (struct slist_s **config, const char *section,
                           const char *name, const char *value);
struct slist_s *config_keep_save ();
void config_keep_restore (struct slist_s *);

#endif
